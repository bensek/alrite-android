package thundercats.com.aritediagnosisapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Collections;

/**
 * The actual searchable list activity base class. Generates the list layout with search bar at the top.
 */

public class SearchableListViewHelper extends AppCompatActivity {
    protected GlobalClass globalClass;
    protected DatabaseHelper dbHelper;
    protected SearchableListAdapter adapter;
    private int toolbarTextID;
    private ListView list;
    private Toolbar myToolbar;
    private EditText editsearch;
    private ArrayList<ListItem> arrayList = new ArrayList<>();
    private AdapterView.OnItemClickListener itemClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        }
    };

    /**
     * Set layout of the page and link search bar functions
     */
    @Override
    public void onStart() {
        super.onStart();
        globalClass = (GlobalClass) getApplication();
        dbHelper = globalClass.databaseHelper;

        setContentView(R.layout.searchable_listview_2);

        setupToolbar();

        list = findViewById(R.id.ListView);

        editsearch = findViewById(R.id.SearchBar);
        editsearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                adapter.filter(charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });


        updateListView();
        updateListListener();
    }

    public AdapterView.OnItemClickListener getItemClickListener() {
        return itemClickListener;
    }

    public void setItemClickListener(AdapterView.OnItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    public ArrayList<ListItem> getArrayList() {
        return arrayList;
    }

    public void setArrayList(ArrayList<ListItem> arrayList) {
        this.arrayList = arrayList;
    }

    public void sortArrayList() {
        Collections.sort(arrayList);
    }

    public void updateListView() {
        sortArrayList();
        adapter = new SearchableListAdapter(this, arrayList);
        list.setAdapter(adapter);
    }

    public void updateListListener() {
        list.setOnItemClickListener(itemClickListener);
    }

    public ListView getList() {
        return list;
    }

    public void setList(ListView list) {
        this.list = list;
    }

    /**
     * Back arrow in toolbar does the same as back button
     *
     * @return
     */
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public void setupToolbar() {
        myToolbar = findViewById(R.id.my_toolbar);
        if (toolbarTextID > 0) {
            myToolbar.setTitle(toolbarTextID);
        }
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    public int getToolbarTextID() {
        return toolbarTextID;
    }

    public void setToolbarTextID(int toolbarTextID) {
        this.toolbarTextID = toolbarTextID;
    }
}
