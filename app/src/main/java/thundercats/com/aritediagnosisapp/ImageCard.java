package thundercats.com.aritediagnosisapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;

/**
 * Class to represent image card displayable in glossary
 */

public class ImageCard extends BaseDynamicCard {
    private ImageView imageView;
    private int imgRes;

    /**
     * Constructor takes image resource ID
     *
     * @param imgRes Image resource ID
     */
    public ImageCard(int imgRes) {
        this.imgRes = imgRes;
        layoutRes = R.layout.image_card_layout;
    }

    /**
     * Method to inflate image card, puts image inside card layout and renders it.
     * @param layoutInflater current activity layout inflater
     * @param context activity context
     * @return view representing image card
     */
    @Override
    public View inflateCard(LayoutInflater layoutInflater, Context context) {
        View view = super.inflateCard(layoutInflater, context);
        View innerView = layoutInflater.inflate(layoutRes, null);
        imageView = innerView.findViewById(R.id.ImageView);
        imageView.setImageResource(imgRes);
        container.addView(innerView);
        return view;
    }
}
