package thundercats.com.aritediagnosisapp.activities;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.aquery.AQuery;
import com.kolastudios.KSUtils;
import com.pixplicity.easyprefs.library.Prefs;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import thundercats.com.aritediagnosisapp.GlobalClass;
import thundercats.com.aritediagnosisapp.PatientProfileActivity2;
import thundercats.com.aritediagnosisapp.R;
import thundercats.com.aritediagnosisapp.adapter.PatientsAdapter;
import thundercats.com.aritediagnosisapp.models.Assessment;
import thundercats.com.aritediagnosisapp.models.Patient;
import thundercats.com.aritediagnosisapp.utils.APIInterface;
import thundercats.com.aritediagnosisapp.utils.AccountManager;
import thundercats.com.aritediagnosisapp.utils.RetrofitClient;
import thundercats.com.aritediagnosisapp.utils.Utils;

/**
 * Activity for creating a new patient
 */

public class PatientsListActivity extends AppCompatActivity {
    private RecyclerView recyclerView;
    private AQuery aq;
    private PatientsAdapter adapter;
    private GlobalClass globalClass;
    private EditText searchView;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patients_list);
        Toolbar myToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Patients");
        myToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        aq = new AQuery(this);
        recyclerView = findViewById(R.id.patients_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        searchView = findViewById(R.id.search_patients);

        searchView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(adapter != null){
                    adapter.getFilter().filter(charSequence);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(Prefs.getString(AccountManager.TOKEN, "").equals("") || !Utils.isNetworkConnected(this)){
            loadPatients();
        }

        if(!Prefs.getString(AccountManager.TOKEN, "").equals("") && Utils.isNetworkConnected(this)){
            getPatients(false);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.patients_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_add_new:
                aq.open(NewPatientActivity.class);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void getPatients(boolean hideList){
        aq.id(R.id.no_patients_found).hide();
        aq.id(R.id.patients_list).hide();
        aq.id(R.id.progress_layout).show();

        loadPatients();

        Call<List<Patient>> call = RetrofitClient.getInstance().create(APIInterface.class).getMyPatients();
        call.enqueue(new Callback<List<Patient>>() {
            @Override
            public void onResponse(Call<List<Patient>> call, Response<List<Patient>> response) {
                if(response.isSuccessful()){
                    Patient.deleteAll(Patient.class);
                    Assessment.deleteAll(Assessment.class);
                    for(Patient patient : response.body()){
                        patient.save();
                        if(patient.assessments.size() > 0) {
                            for (Assessment a : patient.assessments) {
                                a.save();
                            }
                        }
                    }
                    loadPatients();
                }else{
                    aq.id(R.id.progress_layout).hide();
                    Utils.errorDialog(PatientsListActivity.this, "An error occurred. Please try again later.");
                    try {
                        KSUtils.logE("Get Patients OnError -> "+ response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<List<Patient>> call, Throwable t) {
                aq.id(R.id.progress_layout).hide();
//                if(t instanceof IOException){
//                    Utils.errorDialog(getActivity(), "Please check your internet settings to ensure the device has network connection.");
//
//                }else{
//                    Utils.errorDialog(getActivity(), "An error occurred. Please try again later.");
//
//                }

                KSUtils.logE("Get Patients OnFailure -> "+ t.getLocalizedMessage());
            }
        });
    }

    private void loadPatients() {
        List<Patient> list = Patient.listAll(Patient.class);
        if(list.size() == 0){
            aq.id(R.id.no_patients_found).show();
            aq.id(R.id.progress_layout).hide();
            aq.id(R.id.patients_list).hide();
            return;
        }

        aq.id(R.id.no_patients_found).hide();
        aq.id(R.id.progress_layout).hide();
        aq.id(R.id.patients_list).show();

        adapter = new PatientsAdapter(PatientsListActivity.this, list, patient ->{
            Intent intent = new Intent(PatientsListActivity.this, PatientProfileActivity2.class);
            intent.putExtra("PatientId", patient.getId());
            intent.putExtra("Patient" , patient);
            startActivity(intent);
        });
        recyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }
}
