package thundercats.com.aritediagnosisapp.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.aquery.AQuery;

import thundercats.com.aritediagnosisapp.GlobalClass;
import thundercats.com.aritediagnosisapp.PatientSection;
import thundercats.com.aritediagnosisapp.R;

/**
 * Activity for creating a new patient
 */

public class NewPatientActivity extends AppCompatActivity {
    private Button submitButton;
    private AQuery aq;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_patient);
        Toolbar myToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("New Patient - Step 1");
        myToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        aq = new AQuery(this);

        submitButton = findViewById(R.id.btn_new_patient1);
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               continueToNext();
            }
        });
    }
    
    private void continueToNext(){
        String surname = aq.id(R.id.input_surname).text();
        String givenname = aq.id(R.id.input_given_name).text();
        String nickname = aq.id(R.id.input_nickname).text();

        if(surname.isEmpty()){
            aq.toast("Enter a surname");
            return;
        }
        if(givenname.isEmpty()){
            aq.toast("Enter a given name");
            return;
        }

        Intent intent = new Intent(NewPatientActivity.this, NewPatient2Activity.class);
        intent.putExtra("Surname", surname);
        intent.putExtra("GivenName", givenname);
        intent.putExtra("Nickname", nickname);
        startActivity(intent);
    }
}
