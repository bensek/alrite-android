package thundercats.com.aritediagnosisapp.activities;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.aquery.AQuery;
import com.google.android.material.navigation.NavigationView;
import com.pixplicity.easyprefs.library.Prefs;

import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import thundercats.com.aritediagnosisapp.R;
import thundercats.com.aritediagnosisapp.utils.AccountManager;

public class MainActivity extends AppCompatActivity {

    private AppBarConfiguration mAppBarConfiguration;
    private static NavigationView navigationView;
    private static Activity activity;
    static PackageManager packageManager;
    static PackageInfo packageInfo;
    TextView versionView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        packageManager = getPackageManager();
        try {
            packageInfo = packageManager.getPackageInfo(
                    getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        versionView = findViewById(R.id.nav_footer_textview);
        versionView.setText("Version " + packageInfo.versionName);
        navigationView = findViewById(R.id.nav_view);
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_home, R.id.nav_login, R.id.nav_account, R.id.nav_logout)
                .setDrawerLayout(drawer)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);

        setNavHeader();

        if(getIntent().getExtras() != null){
            Intent intent = getIntent();
            if(intent.getStringExtra("FRAGMENT") != null) {
                if (intent.getStringExtra("FRAGMENT").equals("PATIENTS")) {
                    navController.navigate(R.id.nav_patients);
                }
            }
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    public static void setNavHeader(){
        View header = navigationView.getHeaderView(0);
//        ImageView imageView = findViewById(R.id.user_image);
        AQuery aa = new AQuery(header);
        if(!Prefs.getString(AccountManager.TOKEN, "").isEmpty()){
            aa.id(R.id.user_names).text(Prefs.getString(AccountManager.NAME, ""));
            aa.id(R.id.user_phone).text(Prefs.getString(AccountManager.PHONE, ""));
            aa.id(R.id.user_image).click(v -> {

            });
//            Drawable drawable = TextDrawable.builder()
//                    .buildRound(Prefs.getString(AccountManager.INITIALS, ""), activity.getResources().getColor(R.color.primaryButtonColor));
//            imageView.setImageDrawable(drawable);
//            aa.id(R.id.user_image).image(drawable);
        }else{
            aa.id(R.id.user_names).text("Health Worker");
            aa.id(R.id.user_phone).text("07xxxxxxxx");
        }

//        aa.id(R.id.nav_footer_textview).text(packageInfo.versionName);

        if(Prefs.getString(AccountManager.TOKEN, "").equals("")){
            loggedOutMenu();
        }else{
            loggedInMenu();
        }
    }

    public static void loggedInMenu()
    {
        Menu nav_Menu = navigationView.getMenu();
        nav_Menu.findItem(R.id.nav_login).setVisible(false);
        nav_Menu.findItem(R.id.nav_logout).setVisible(true);
    }

    public static void loggedOutMenu()
    {
        Menu nav_Menu = navigationView.getMenu();
        nav_Menu.findItem(R.id.nav_logout).setVisible(false);
        nav_Menu.findItem(R.id.nav_login).setVisible(true);
    }
}