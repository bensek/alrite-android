package thundercats.com.aritediagnosisapp.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.aquery.AQuery;
import com.kolastudios.KSUtils;
import com.pixplicity.easyprefs.library.Prefs;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import thundercats.com.aritediagnosisapp.GlobalClass;
import thundercats.com.aritediagnosisapp.PatientSection;
import thundercats.com.aritediagnosisapp.PneumoniaSection;
import thundercats.com.aritediagnosisapp.R;
import thundercats.com.aritediagnosisapp.models.Patient;
import thundercats.com.aritediagnosisapp.utils.APIInterface;
import thundercats.com.aritediagnosisapp.utils.AccountManager;
import thundercats.com.aritediagnosisapp.utils.RetrofitClient;
import thundercats.com.aritediagnosisapp.utils.Utils;

/**
 * Activity for creating a new patient
 */

public class NewPatient2Activity extends AppCompatActivity {
    private Button createNew;
    private String surname, givenname, nickname;
    private RadioGroup sexRadioGroup;
    private AQuery aq;
    public PneumoniaSection pneumoniaSection;
    protected GlobalClass globalClass;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_patient2);
        Toolbar myToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("New Patient - Step 2");
        myToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        globalClass = (GlobalClass) getApplication();

        aq = new AQuery(this);
        sexRadioGroup = findViewById(R.id.sex_radio_group);

        if(getIntent().getExtras() != null){
            Intent intent = getIntent();
            surname = intent.getStringExtra("Surname");
            givenname = intent.getStringExtra("GivenName");
            nickname = intent.getStringExtra("Nickname");
        }

        aq.id(R.id.btn_new_patient2).click(k->{
            if(Prefs.getString(AccountManager.TOKEN, "").equals("") || !Utils.isNetworkConnected(this)){
                createPatientOffline();
            }

            if(!Prefs.getString(AccountManager.TOKEN, "").equals("") && Utils.isNetworkConnected(this)){
                createPatient();
            }
        });

    }
    private void createPatientOffline(){
        String sex = "";
        String age = aq.id(R.id.input_age).text();
        String weight = aq.id(R.id.input_weight).text();

        if(sexRadioGroup.getCheckedRadioButtonId() == -1){
            aq.toast("Select the sex of the child");
            return;
        }

        if(sexRadioGroup.getCheckedRadioButtonId() == R.id.radio_male){
            sex = "Male";
        }
        if(sexRadioGroup.getCheckedRadioButtonId() == R.id.radio_female){
            sex = "Female";
        }

        if(age.isEmpty()){
            aq.toast("Enter the age");
            return;
        }
        if(weight.isEmpty()){
            aq.toast("Enter the weight");
            return;
        }

        Patient patient = new Patient();
        patient.surname = surname;
        patient.given_name = givenname;
        patient.nickname = nickname;
        patient.sex = sex;
        patient.age_range = age;
        patient.weight = Integer.parseInt(weight);
        patient.save();

        startAssessment(patient);
    }

    private void createPatient(){
        String sex = "";
        String age = aq.id(R.id.input_age).text();
        String weight = aq.id(R.id.input_weight).text();

        if(sexRadioGroup.getCheckedRadioButtonId() == -1){
            aq.toast("Select the sex of the child");
            return;
        }

        if(sexRadioGroup.getCheckedRadioButtonId() == R.id.radio_male){
            sex = "Male";
        }
        if(sexRadioGroup.getCheckedRadioButtonId() == R.id.radio_female){
            sex = "Female";
        }

        if(age.isEmpty()){
            aq.toast("Enter the age");
            return;
        }
        if(weight.isEmpty()){
            aq.toast("Enter the weight");
            return;
        }

        ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait...");
        progressDialog.setTitle("Saving Patient Data");
        progressDialog.setCancelable(true);

        progressDialog.show();

        Call<Patient> call = RetrofitClient.getInstance().create(APIInterface.class).createPatient(surname, givenname, nickname,
                sex, age, weight);

        call.enqueue(new Callback<Patient>() {
            @Override
            public void onResponse(Call<Patient> call, Response<Patient> response) {
                if(response.isSuccessful()){
                    aq.toast("Patient saved");
                    progressDialog.hide();
                    Prefs.putInt(AccountManager.CURRENT_PATIENT, response.body().getId().intValue());
                    response.body().save();
                    startAssessment(response.body());
                }else{
                    progressDialog.dismiss();
                    Utils.errorDialog(NewPatient2Activity.this, "An error occurred. Please try again later.");
                    try {
                        KSUtils.logE("Create Patient OnError -> "+ response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

            }

            @Override
            public void onFailure(Call<Patient> call, Throwable t) {
                progressDialog.dismiss();
                if(t instanceof IOException){
                    createPatientOffline();
                    Utils.errorDialog(NewPatient2Activity.this, "Please check your internet settings to ensure the device has network connection.");

                }else{
                    Utils.errorDialog(NewPatient2Activity.this, "An error occurred. Please try again later.");

                }

                KSUtils.logE("Create Patient OnFailure -> "+ t.getLocalizedMessage());
            }
        });

    }

    private void startAssessment(Patient patient){
        int gender = 0;
        if(patient.sex.equals("Male")){
            gender = 0;
        }
        if(patient.sex.equals("Female")){
            gender = 1;
        }

        PatientSection patientSection = new PatientSection(globalClass);
        patientSection.setSurname(patient.surname);
        patientSection.setGivenname(patient.given_name);
        patientSection.setNickname(patient.nickname);
        patientSection.setGender(gender);
        patientSection.setWeight(patient.weight);
        patientSection.setId(String.valueOf(patient.getId()));

        globalClass.patientsMap.put(String.valueOf(patient.getId()), patientSection);
        globalClass.currentPatient = patientSection;
        patientSection.pneumoniaSection.startSection(NewPatient2Activity.this);
    }
}
