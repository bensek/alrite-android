package thundercats.com.aritediagnosisapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.aquery.AQuery;

import java.util.List;

import thundercats.com.aritediagnosisapp.R;
import thundercats.com.aritediagnosisapp.models.Assessment;
import thundercats.com.aritediagnosisapp.models.Patient;
import thundercats.com.aritediagnosisapp.utils.Utils;

public class AssessmentsAdapter extends RecyclerView.Adapter<AssessmentsAdapter.ViewHolder> {
    private List<Assessment> assessments;
    private Context context;
    private final LayoutInflater inflater;

    private final AssessmentsAdapter.OnItemClickListener listener;
    private AQuery aa;

    public interface OnItemClickListener {
        void onItemClick(Assessment item);
    }


    public AssessmentsAdapter(Context ctx, List<Assessment> list, AssessmentsAdapter.OnItemClickListener listener) {
        inflater = LayoutInflater.from(ctx);
        this.assessments = list;
        this.listener = listener;
        aa = new AQuery(ctx);
        this.context = ctx;
    }

    @Override
    public AssessmentsAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        final View view;
        view = inflater.inflate(R.layout.assessment_list_item, viewGroup, false);
        aa = new AQuery(view);
        return new AssessmentsAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AssessmentsAdapter.ViewHolder holder, int position) {
        Assessment a = assessments.get(position);
        holder.bind(a, listener);
    }

    @Override
    public int getItemCount() {
        return assessments.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView name, date;


        public ViewHolder(View view) {
            super(view);
            //name = view.findViewById(R.id.cat_name);
            name = view.findViewById(R.id.assessment_title);
            date = view.findViewById(R.id.assessment_date);
        }

        public void bind(final Assessment a, final AssessmentsAdapter.OnItemClickListener listener) {
            name.setText(a.title);
            if(a.created_at == null){
                date.setText(a.local_date);
            }else{
                date.setText(Utils.formatDate(a.created_at));
            }

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onItemClick(a);
                }
            });
        }
    }
}
