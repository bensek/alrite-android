package thundercats.com.aritediagnosisapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.aquery.AQuery;
import com.pixplicity.easyprefs.library.Prefs;

import java.util.ArrayList;
import java.util.List;

import thundercats.com.aritediagnosisapp.R;
import thundercats.com.aritediagnosisapp.models.Patient;

public class PatientsAdapter extends RecyclerView.Adapter<PatientsAdapter.ViewHolder> {
    private List<Patient> patientList = new ArrayList<>();
    private List<Patient> patientListFiltered = new ArrayList<>();
    private Context context;
    private final LayoutInflater inflater;

    private final PatientsAdapter.OnItemClickListener listener;
    private AQuery aa;

    public interface OnItemClickListener {
        void onItemClick(Patient item);
    }


    public PatientsAdapter(Context ctx, List<Patient> list, PatientsAdapter.OnItemClickListener listener) {
        inflater = LayoutInflater.from(ctx);
        this.patientList = list;
        this.patientListFiltered = list;
        this.listener = listener;
        aa = new AQuery(ctx);
        this.context = ctx;
    }

    @Override
    public PatientsAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        final View view;
        view = inflater.inflate(R.layout.patient_list_item, viewGroup, false);
        aa = new AQuery(view);
        return new PatientsAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(PatientsAdapter.ViewHolder holder, int position) {
        Patient p = patientListFiltered.get(position);
        holder.bind(p, position, listener);
    }

    @Override
    public int getItemCount() {
        return patientListFiltered.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView name, number;
        private ImageView icon;
        private TextDrawable drawable;

        public ViewHolder(View view) {
            super(view);
            //name = view.findViewById(R.id.cat_name);
            name = view.findViewById(R.id.patient_name);
            icon = view.findViewById(R.id.patient_icon);
            number = view.findViewById(R.id.patient_number);
        }

        public void bind(final Patient patient, int position, final PatientsAdapter.OnItemClickListener listener) {
            name.setText(patient.getName());
            number.setText(patient.number);
            drawable = TextDrawable.builder()
                    .buildRound(patient.getInitials(), context.getResources().getColor(R.color.primaryButtonColor));
            icon.setImageDrawable(drawable);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onItemClick(patient);
                }
            });
        }
    }


    //@Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    patientListFiltered = patientList;
                } else {
                    List<Patient> filteredList = new ArrayList<>();
                    for (Patient row : patientList) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getName().toLowerCase().contains(charString.toLowerCase()) || row.number.contains(charSequence)) {
                            filteredList.add(row);
                        }
                    }

                    patientListFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = patientListFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                patientListFiltered = (ArrayList<Patient>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    public interface PatientsAdapterListener {
        void onContactSelected(Patient patient);
    }
}
