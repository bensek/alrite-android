package thundercats.com.aritediagnosisapp;

import android.text.TextUtils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * Class to represent various data tables in the database.
 */

public class DataTable {
    private String name;
    private String[] cols;
    private String[] colTypes;

    public DataTable() {
    }

    /**
     * Constructor
     *
     * @param name     data table name
     * @param cols     column names
     * @param colTypes SQL column types
     */
    public DataTable(String name, String[] cols, String[] colTypes) {
        this.name = name;
        this.cols = cols;
        this.colTypes = colTypes;
    }

    public String getName() {
        return name;
    }

    public String[] getCols() {
        return cols;
    }

    public String[] getColTypes() {
        return colTypes;
    }

    /**
     * Gets integer index of column by name
     * @param name column name
     * @return column index
     */
    public int getColIndexByName(String name){
        return java.util.Arrays.asList(cols).indexOf(name);
    }

    /**
     * Generates column name list from indices
     *
     * @param indices column indices
     * @return list of names
     */
    private String[] indicesToNamesList(int[] indices) {
        String[] names = new String[indices.length];
        for (int ii = 0; ii < indices.length; ii++) {
            names[ii] = getCols()[ii];
        }
        return names;
    }

    /**
     * Generates list of column names with full reference - 'table.column' for use in join queries
     *
     * @param indices column indices
     * @return list of universal column names
     */
    private String[] indicesToUniversalNamesList(int[] indices) {
        return colNamesToUniversal(indicesToNamesList(indices));
    }

    /**
     * Prepends table name to columns list - 'table.column'
     *
     * @param columns column names
     * @return
     */
    private String[] colNamesToUniversal(String[] columns) {
        for (int ii = 0; ii < columns.length; ii++) {
            columns[ii] = getUniversalColReference(columns[ii]);
        }
        return columns;
    }

    /**
     * Creates table create SQL query
     * @return create table query
     */
    public String getCreateString(){
        StringBuffer s = new StringBuffer();
        s.append("create table ");
        s.append(getName());
        s.append(" (");
        for(int ii = 0; ii<getCols().length; ii++){
            if(ii!=0){
                s.append(",");
            }
            s.append(" "+getCols()[ii]+" "+getColTypes()[ii]);
        }
        s.append(", ID TEXT PRIMARY KEY , Deleted BOOLEAN , Modified DATETIME , Created DATETIME DEFAULT CURRENT_TIMESTAMP)");
        return s.toString();
    }

    /**
     * Creates SQL table drop string
     * @return drop string
     */
    public String getDropString(){
        return "DROP TABLE IF EXISTS " + getName();
    }

    /**
     * Gets query string for specified columns - empty for all cols
     * @param columns column indices - empty for all cols
     * @return query string
     */
    public String getUniversalColumnQuery(int[] columns) {
        return getUniversalColumnQuery(indicesToNamesList(columns));
    }

    /**
     * Gets query string for specified columns - empty for all cols
     * @param columns column names - empty for all cols
     * @return query string
     */
    public String getUniversalColumnQuery(String[] columns) {
        String selectQuery = "";
        if (columns == null || columns.length == 0) {
            selectQuery += getName() + ".*";
        } else {
            columns = colNamesToUniversal(columns);
            selectQuery = TextUtils.join(", ", columns);
        }
        return selectQuery;
    }

    /**
     * Makes SQL 'from' string (prepends 'FROM' to data table name)
     * @return from string
     */
    public String getFromString() {
        return " FROM " + getName();
    }

    public String getEqualsSQL(int column, String val) {
        return getEqualsSQL(getCols()[column], val);
    }

    public String getEqualsSQL(String column, String val) {
        return makeWhereClause(column, val, " = ");
    }

    public String getNotEqualsSQL(int column, String val) {
        return getNotEqualsSQL(getCols()[column], val);
    }

    public String getNotEqualsSQL(String column, String val) {
        return makeWhereClause(column, val, " != ");
    }

    public String getGreaterThanSQL(int column, String val) {
        return getGreaterThanSQL(getCols()[column], val);
    }

    public String getGreaterThanSQL(String column, String val) {
        return makeWhereClause(column, val, " > ");
    }

    public String getGreaterThanEqualSQL(int column, String val) {
        return getGreaterThanEqualSQL(getCols()[column], val);
    }

    public String getGreaterThanEqualSQL(String column, String val) {
        return makeWhereClause(column, val, " >= ");
    }

    public String getLessThanSQL(int column, String val) {
        return getLessThanSQL(getCols()[column], val);
    }

    public String getLessThanSQL(String column, String val) {
        return makeWhereClause(column, val, " < ");
    }

    public String getLessThanEqualSQL(int column, String val) {
        return getLessThanEqualSQL(getCols()[column], val);
    }

    public String getLessThanEqualSQL(String column, String val) {
        return makeWhereClause(column, val, " <= ");
    }

    public String getGlobSQL(int column, String val) {
        return getGlobSQL(getCols()[column], val);
    }

    public String getGlobSQL(String column, String val) {
        return makeWhereClause(column, val, " GLOB ");
    }

    public String getLikeSQL(int column, String val) {
        return getLikeSQL(getCols()[column], val);
    }

    public String getLikeSQL(String column, String val) {
        return makeWhereClause(column, val, " LIKE ");
    }

    public String getNullSQL(int column) {
        return getNullSQL(getCols()[column]);
    }

    public String getNullSQL(String column) {
        return makeSpecialWhereClause(column, " IS NULL ");
    }

    public String getNotNullSQL(int column) {
        return getNotNullSQL(getCols()[column]);
    }

    public String getNotNullSQL(String column) {
        return makeSpecialWhereClause(column, " IS NOT NULL ");
    }

    public String getBeforeDateSQL(int column, Date d) {
        return getBeforeDateSQL(getCols()[column], d);
    }

    public String getBeforeDateSQL(String column, Date d) {
        return makeDateWhereClause(column, d, " < ");
    }

    public String getBeforeOrOnDateSQL(int column, Date d) {
        return getBeforeOrOnDateSQL(getCols()[column], d);
    }

    public String getBeforeOrOnDateSQL(String column, Date d) {
        return makeDateWhereClause(column, d, " <= ");
    }

    public String getAfterDateSQL(int column, Date d) {
        return getAfterDateSQL(getCols()[column], d);
    }

    public String getAfterDateSQL(String column, Date d) {
        return makeDateWhereClause(column, d, " > ");
    }

    public String getAfterOrOnDateSQL(int column, Date d) {
        return getAfterOrOnDateSQL(getCols()[column], d);
    }

    public String getAfterOrOnDateSQL(String column, Date d) {
        return makeDateWhereClause(column, d, " >= ");
    }

    public String getDateEqualsSQL(int column, Date d) {
        return getDateEqualsSQL(getCols()[column], d);
    }

    public String getDateEqualsSQL(String column, Date d) {
        return makeDateWhereClause(column, d, " = ");
    }

    public String getBetweenExclusiveSQL(String column, String min, String max) {
        return getGreaterThanSQL(column, min) + " AND " + getLessThanSQL(column, max);
    }

    public String getBetweenExclusiveSQL(String column, Date min, Date max) {
        return getAfterDateSQL(column, min) + " AND " + getBeforeDateSQL(column, max);
    }

    public String getBetweenInclusiveSQL(String column, String min, String max) {
        return getGreaterThanEqualSQL(column, min) + " AND " + getLessThanEqualSQL(column, max);
    }

    public String getBetweenInclusiveSQL(String column, Date min, Date max) {
        return getAfterOrOnDateSQL(column, min) + " AND " + getBeforeOrOnDateSQL(column, max);
    }

    private String makeWhereClause(String column, String val, String comparator) {
        if (val == null || val.isEmpty()) {
            return getUniversalColReference(column) + comparator + "?";
        }
        return getUniversalColReference(column) + comparator + "'" + val + "'";
    }

    private String makeSpecialWhereClause(String column, String comparator) {
        return getUniversalColReference(column) + comparator;
    }

    private String makeDateWhereClause(String column, Date d, String comparator) {
        String s = formatDateForSQL(d);
        return dateStringToSQLDate(getUniversalColReference(column)) + comparator + dateStringToSQLDate(s);
    }

    private String dateStringToSQLDate(String date) {
        return " Datetime('" + date + "') ";
    }

    public String getUniversalColReference(int column) {
        return getName() + "." + getCols()[column];
    }

    public String getUniversalColReference(String column) {
        return getName() + "." + column;
    }

    private String formatDateForSQL(Date d) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
        return formatter.format(d);
    }
}
