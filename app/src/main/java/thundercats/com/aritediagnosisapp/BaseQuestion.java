package thundercats.com.aritediagnosisapp;


import android.os.Build;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;


/**
 * Base class for dynamic questions. This is the core for all questions within the decision tree.
 */

public abstract class BaseQuestion extends BaseDynamicItem{
    private String DBColumnID;
    private TextView titleTextView,subtitleTextView;
    private LinearLayout navigationContent,cardContent;
    private int titleTextResID,subtitleTextResID;
    private long screenTimeStamp;
    private Button skipButton;
    private View.OnClickListener skipButtonListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            setCompleted(true);
            String next = nextNavigation();
            if (isShowDiagnosis()) {
                ((GlobalClass) getHostActivity().getApplication()).currentPatient.pneumoniaSection.diagnosis.showPopup(getHostActivity());
            } else {
                toNext(next);
            }
        }
    };
    private boolean completed, DBQuestion, showDiagnosis, allowSkip;


    /**
     * @param ID                question ID
     * @param section           parent section
     * @param glossaryTerm      associated glossary term
     * @param titleTextResID    title text resource ID
     * @param subtitleTextResID subtitle text resource ID
     */
    public BaseQuestion(String ID, QuestionSection section, String glossaryTerm, int titleTextResID, int subtitleTextResID) {
        super(ID, Type.Question, section, glossaryTerm);
        this.titleTextResID = titleTextResID;
        this.subtitleTextResID = subtitleTextResID;
        DBQuestion = false;
        section.putItem(this);
    }

    /**
     *
     * @param ID question ID
     * @param section parent section
     * @param glossaryTerm associated glossary term
     * @param titleTextResID title text resource ID
     * @param subtitleTextResID subtitle text resource ID
     * @param DBColumnID associated database column ID
     */
    public BaseQuestion(String ID, QuestionSection section, String glossaryTerm, String DBColumnID, int titleTextResID, int subtitleTextResID) {
        super(ID, Type.Question, section, glossaryTerm);
        this.DBColumnID = DBColumnID;
        this.titleTextResID = titleTextResID;
        this.subtitleTextResID = subtitleTextResID;
        DBQuestion = true;
        section.putItem(this);
    }

    /**
     *
     * @param ID question ID
     * @param titleTextResID title text resource ID
     * @param subtitleTextResID subtitle text resource ID
     * @param section parent section
     */
    public BaseQuestion(String ID, int titleTextResID, int subtitleTextResID, QuestionSection section) {
        super(ID, Type.Question,section);
        this.DBColumnID = "";
        this.DBQuestion = false;
        this.titleTextResID = titleTextResID;
        this.subtitleTextResID = subtitleTextResID;
        section.putItem(this);
    }

    /**
     *
     * @param ID question ID
     * @param section parent section
     * @param DBColumnID associated database column ID
     * @param titleTextResID title text resource ID
     * @param subtitleTextResID subtitle text resource ID
     */
    public BaseQuestion(String ID, String DBColumnID, int titleTextResID, int subtitleTextResID, QuestionSection section) {
        super(ID,Type.Question,section);
        this.DBColumnID = DBColumnID;
        this.DBQuestion = true;
        this.titleTextResID = titleTextResID;
        this.subtitleTextResID = subtitleTextResID;
        completed = false;
        section.putItem(this);
    }

    /**
     * Sets up basic question layout with content card, title and subtitle. If glossary term is linked, shows the info button and links term popup.
     */
    @Override
    public void setupView(){

        getHostActivity().setContentView(R.layout.question_layout_shell);
        setCardContent((LinearLayout)getHostActivity().findViewById(R.id.DynamicContent));
        getHostActivity().findViewById(R.id.skipLayout).setVisibility(View.VISIBLE);

        this.skipButton = getHostActivity().findViewById(R.id.skipButton);
        if (allowSkip) {
            skipButton.setVisibility(View.VISIBLE);
            skipButton.setOnClickListener(skipButtonListener);
        } else {
            skipButton.setVisibility(View.GONE);
        }

        if (glossaryTermID != null) {
            final GlossaryItem glossaryItem = ((GlobalClass) getHostActivity().getApplication()).findGlossaryItemByID(glossaryTermID);
            if (glossaryItem != null) {
                setTitleTextView((TextView)getHostActivity().findViewById(R.id.Title));
                this.titleTextView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        glossaryItem.showPopup(getHostActivity(), false);
                    }
                });
                titleTextView.setText(getTitleTextResID());
                String highlightTerm = titleTextView.getText().toString();
                String replacement = glossaryTermID.replaceAll("_", " ");
                String newString = highlightTerm.replaceAll(replacement, "<font color='blue'><u><b>"+replacement+"</b></u></font>");
                if (Build.VERSION.SDK_INT >= 24) {
                    titleTextView.setText(Html.fromHtml(newString, Html.FROM_HTML_MODE_LEGACY)); // for 24 api and more
                } else {
                    titleTextView.setText(Html.fromHtml(newString)); // or for older api
                }


            } else {
                setTitleTextView((TextView)getHostActivity().findViewById(R.id.Title));
                updateTitleText();
            }
        } else {
            setTitleTextView((TextView)getHostActivity().findViewById(R.id.Title));
            updateTitleText();
        }
        setSubtitleTextView((TextView)getHostActivity().findViewById(R.id.Subtitle));
        updateSubtitleText();

        super.setupView();
        this.screenTimeStamp = System.currentTimeMillis();


    }

    public void updateTitleText(){
        titleTextView.setText(getTitleTextResID());
    }

    public void updateSubtitleText(){
        subtitleTextView.setText(getSubtitleTextResID());
    }

    public String getDBColumnID() {
        return DBColumnID;
    }

    public void setDBColumnID(String DBColumnID) {
        this.DBColumnID = DBColumnID;
    }

    public int getTitleTextResID() {
        return titleTextResID;
    }

    public void setTitleTextResID(int titleTextResID) {
        this.titleTextResID = titleTextResID;
    }

    public int getSubtitleTextResID() {
        return subtitleTextResID;
    }

    public void setSubtitleTextResID(int subtitleTextResID) {
        this.subtitleTextResID = subtitleTextResID;
    }

    public TextView getTitleTextView() {
        return titleTextView;
    }

    public void setTitleTextView(TextView titleTextView) {
        this.titleTextView = titleTextView;
    }

    public TextView getSubtitleTextView() {
        return subtitleTextView;
    }

    public void setSubtitleTextView(TextView subtitleTextView) {
        this.subtitleTextView = subtitleTextView;
    }

    public LinearLayout getNavigationContent() {
        return navigationContent;
    }

    public void setNavigationContent(LinearLayout navigationContent) {
        this.navigationContent = navigationContent;
    }

    public LinearLayout getCardContent() {
        return cardContent;
    }

    public void setCardContent(LinearLayout cardContent) {
        this.cardContent = cardContent;
    }

    public void fillCardContent(int id){
        putContent(cardContent,id);
    }

    public void fillNavigationContent(int id){
        putContent(navigationContent,id);
    }

    public boolean isCompleted() {
        return completed;
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;

    }

    public void setScreenTimeStamp(long screenTimeStamp1) {this.screenTimeStamp = screenTimeStamp1;}
    public long getScreenTimeStamp(){return screenTimeStamp;}


    public abstract String getDBValue();

    /**
     * this function dictates behavior after a navigation item has been clicked. Will navigate to next item and sets question as complete.
     */
    public void afterClick(){
        setCompleted(true);
        screenTimeStamp = System.currentTimeMillis() - screenTimeStamp;
        String next = nextNavigation();
        if (isShowDiagnosis()) {
            ((GlobalClass) getHostActivity().getApplication()).currentPatient.pneumoniaSection.diagnosis.showPopup(getHostActivity());
        } else {
            toNext(next);
        }
    }



    /**
     * Custom behavior for when question is answered. Add logic specific to question type here.
     * @return returns next action ID
     */
    public abstract String nextNavigation();

    public boolean isDBQuestion() {
        return DBQuestion;
    }

    public void setDBQuestion(boolean DBQuestion) {
        this.DBQuestion = DBQuestion;
    }

    public void reset(){
        setCompleted(false);
    }

    /**
     * Override to show previously given answer. Called when question is accessed again after initial answer given.
     */
    public abstract void renderPrevious();

    /**
     * Converts question to list item for displaying in previously answered questions list.
     * @return listItem representing question
     */
    public ListItem toListItem() {
        String title = getHostActivity().getResources().getString(titleTextResID);
        String sub = getHostActivity().getResources().getString(subtitleTextResID);
        return new ListItem(title, sub, getID());
    }

    public boolean isShowDiagnosis() {
        return showDiagnosis;
    }

    /**
     * If the question should trigger a diagnosis popup when answered.
     * @param showDiagnosis
     */
    public void setShowDiagnosis(boolean showDiagnosis) {
        this.showDiagnosis = showDiagnosis;
    }

    public void setAllowSkip(boolean b) {this.allowSkip = b;}


}
