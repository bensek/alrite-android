package thundercats.com.aritediagnosisapp;


import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ScrollView;

import thundercats.com.aritediagnosisapp.activities.MainActivity;

public class ClinicResourcesRecorderActivity extends AppCompatActivity implements View.OnClickListener{
    protected GlobalClass globalClass;
    protected DatabaseHelper dbHelper;
    protected DatabaseModel dbModel;
    protected ClinicResourcesRecorderActivity clinicResourcesRecorderActivity;
    private ScrollView view;
    private Button exit_button;
    private CheckBox bronchodilator_cb, spacer_cb, pulseOx_cb, thermometer_cb, stethoscope_cb, antibiotics_cb, steroids_cb, nebulizer_cb, suppOx_cb;
    private ContentValues values;
    private int count;

    @Override
    protected void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        globalClass = (GlobalClass) getApplication();
        dbHelper = globalClass.databaseHelper;
        dbModel = new DatabaseModel(dbHelper.CLINIC, globalClass);
        dbModel.setId("clinic");
        values = dbModel.makeContentValues();
        setContentView(R.layout.checklist_equipment);

        setClinicResourcesRecorder(ClinicResourcesRecorderActivity.this);
        setupViews();
        setupListeners();

        if(globalClass.getIsSettings_done()) {
            setClinicResources(dbHelper, dbHelper.CLINIC);
        }
        count = 0;


        exit_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CheckBox[] cbArray = new CheckBox[] {bronchodilator_cb, spacer_cb, pulseOx_cb, stethoscope_cb, thermometer_cb, nebulizer_cb, suppOx_cb, antibiotics_cb, steroids_cb};
                String[] clinic_cols = new String[]{"bronchodilator","spacer", "pulse_ox", "stethoscope", "thermometer", "nebulizer", "supp_ox", "antibiotics", "steroids"};
                for(int i = 0; i<cbArray.length;i++) {
                    if(!cbArray[i].isChecked()){
                        values.put(clinic_cols[i], "false");
                    }
                }
                values.put("resources_set", "true");
                saveToClinicDB();
                globalClass.updateIsSettingsDone();

                if (!spacer_cb.isChecked() && count == 0) {
                    final GlossaryItem glossaryItem = (globalClass.findGlossaryItemByID("improv_spacer"));
                    if(glossaryItem!=null){
                        count++;
                        glossaryItem.showPopup(ClinicResourcesRecorderActivity.this, false);
                    }
                } else {
                    Intent intent = new Intent(globalClass.getApplicationContext(), MainActivity.class);
                    view.getContext().startActivity(intent);
                }
            }
        } );


        }


    @Override
    public void onResume() {
        super.onResume();
        if(globalClass.getIsSettings_done()) {
            setClinicResources(dbHelper, dbHelper.CLINIC);
        }
    }

    @Override
    public void onClick(View view) {
        // Is the view now checked?
        boolean checked = ((CheckBox) view).isChecked();

        // Check which checkbox was clicked
        switch(view.getId()) {
            case R.id.bronchodilator_cb:
                if (checked) {
                    values.put("bronchodilator", "true");
                    globalClass.bronchodilator_present = true;
                } else {
                    globalClass.bronchodilator_present = false;
                }
                break;
            case R.id.spacer_CB:
                if (checked) {
                    values.put("spacer", "true");
                    globalClass.spacer_present = true;
                } else {
                    globalClass.spacer_present = false;
                }
                    break;

            case R.id.pulseox_CB:
                if (checked) {
                    values.put("pulse_ox", "true");
                    globalClass.pulseOx_present = true;
                } else {
                    globalClass.pulseOx_present = false;
                }
                    break;

            case R.id.stethoscope_CB:
                if (checked) {
                    values.put("stethoscope", "true");
                    globalClass.stethoscope_present = true;
                } else {
                    globalClass.stethoscope_present = false;
                }
                    break;
            case R.id.thermometer:
                if (checked) {
                    values.put("thermometer", "true");
                    globalClass.thermometer_present = true;
                } else {
                    globalClass.thermometer_present = false;
                }
                    break;

            case R.id.nebulizer:
                if (checked) {
                    values.put("nebulizer", "true");
                    globalClass.nebulizer_present = true;
                } else {
                    globalClass.nebulizer_present = false;
                }
                    break;
            case R.id.sup_ox:
                if (checked) {
                    values.put("supp_ox", "true");
                    globalClass.suppOx_present = true;
                } else {
                    globalClass.suppOx_present = false;
                }
                    break;

            case R.id.antibiotics:
                if (checked) {
                    values.put("antibiotics", "true");
                    globalClass.antibiotics_present = true;
                } else {
                    globalClass.antibiotics_present = false;
                }
                    break;

            case R.id.steroids:
                if (checked) {
                    values.put("steroids", "true");
                    globalClass.steroids_present = true;
                } else {
                    globalClass.steroids_present = false;
                }
                    break;
        }
    }


    public void setClinicResourcesRecorder(ClinicResourcesRecorderActivity clinicResourcesRecorder) {
        this.clinicResourcesRecorderActivity = clinicResourcesRecorder;
    }

    public void setClinicResources(DatabaseHelper dbHelper, DataTable dataTable) {
        Cursor c = dbHelper.getTableItemByID("clinic", dataTable);

        for(int i =1; i<10; i++) {
            String s = c.getString(i);
            setResourceBox(i, s);

        }

    }

    public void setResourceBox(int box, String s) {
        switch(box) {
            case 1:
                bronchodilator_cb.setChecked(Boolean.valueOf(s));
                globalClass.bronchodilator_present= Boolean.valueOf(s);
                break;

            case 2:
                spacer_cb.setChecked(Boolean.valueOf(s));
                globalClass.spacer_present = Boolean.valueOf(s);
                break;

            case 3:
                pulseOx_cb.setChecked(Boolean.valueOf(s));
                globalClass.pulseOx_present = Boolean.valueOf(s);
                break;

            case 4:
                stethoscope_cb.setChecked(Boolean.valueOf(s));
                globalClass.stethoscope_present = Boolean.valueOf(s);
                break;

            case 5:
                thermometer_cb.setChecked(Boolean.valueOf(s));
                globalClass.thermometer_present = Boolean.valueOf(s);
                break;

            case 6:
                nebulizer_cb.setChecked(Boolean.valueOf(s));
                globalClass.nebulizer_present = Boolean.valueOf(s);
                break;

            case 7:
                suppOx_cb.setChecked(Boolean.valueOf(s));
                globalClass.suppOx_present = Boolean.valueOf(s);
                break;

            case 8:
                antibiotics_cb.setChecked(Boolean.valueOf(s));
                globalClass.antibiotics_present = Boolean.valueOf(s);
                break;

            case 9:
                steroids_cb.setChecked(Boolean.valueOf(s));
                globalClass.steroids_present = Boolean.valueOf(s);
                break;

        }
    }

    /*public void updateResourceDB(int box, String s) {
        switch(box) {
            case 1:
                bronchodilator_cb.setChecked(Boolean.valueOf(s));
                break;

            case 2:
                spacer_cb.setChecked(Boolean.valueOf(s));
                break;

            case 3:
                pulseOx_cb.setChecked(Boolean.valueOf(s));
                break;

            case 4:
                stethoscope_cb.setChecked(Boolean.valueOf(s));
                break;

            case 5:
                thermometer_cb.setChecked(Boolean.valueOf(s));
                break;

            case 6:
                nebulizer_cb.setChecked(Boolean.valueOf(s));
                break;

            case 7:
                suppOx_cb.setChecked(Boolean.valueOf(s));
                break;

            case 8:
                antibiotics_cb.setChecked(Boolean.valueOf(s));
                break;

            case 9:
                steroids_cb.setChecked(Boolean.valueOf(s));
                break;

        }
    }*/

    public void saveToClinicDB(){
        Cursor c = globalClass.databaseHelper.getTableItemByID("clinic", dbHelper.CLINIC);
        if (c.getCount() > 0) {
            globalClass.databaseHelper.updateTableItem(values, dbModel.getId(), dbHelper.CLINIC);
            return;
        }

        globalClass.databaseHelper.createTableItem(values, dbHelper.CLINIC);
    }

    private void setupViews() {
        bronchodilator_cb = findViewById(R.id.bronchodilator_cb);
        spacer_cb = findViewById(R.id.spacer_CB);
        pulseOx_cb = findViewById(R.id.pulseox_CB);
        thermometer_cb = findViewById(R.id.thermometer);
        stethoscope_cb = findViewById(R.id.stethoscope_CB);
        nebulizer_cb = findViewById(R.id.nebulizer);
        suppOx_cb = findViewById(R.id.sup_ox);
        antibiotics_cb = findViewById(R.id.antibiotics);
        steroids_cb = findViewById(R.id.steroids);
        exit_button = findViewById(R.id.done_button);
    }

    private void setupListeners() {
        bronchodilator_cb.setOnClickListener(this);
        spacer_cb.setOnClickListener(this);
        pulseOx_cb.setOnClickListener(this);
        thermometer_cb.setOnClickListener(this);
        stethoscope_cb.setOnClickListener(this);
        nebulizer_cb.setOnClickListener(this);
        suppOx_cb.setOnClickListener(this);
        antibiotics_cb.setOnClickListener(this);
        steroids_cb.setOnClickListener(this);
    }

}
