package thundercats.com.aritediagnosisapp;

import android.content.ContentValues;
import android.database.Cursor;
import java.util.Date;
import java.util.UUID;

/**
 * Base model for database entry item.
 */

public class DatabaseModel {
    protected GlobalClass globalClass;
    private String id;
    private Date createdDate;
    private Date lastModified;
    private Boolean deleted;
    private DataTable dataTable;

    /**
     * Constructor
     *
     * @param dataTable   associated data table
     * @param globalClass instance of global application class
     */
    public DatabaseModel(DataTable dataTable, GlobalClass globalClass) {
        this.dataTable = dataTable;
        this.globalClass = globalClass;
    }

    public DatabaseModel(String id) {
        this.id = id;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getLastModified() {
        return lastModified;
    }

    public void setLastModified(Date lastModified) {
        this.lastModified = lastModified;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    /**
     * Generates unique universal identifier for database identification
     * @return
     */
    public String generateID() {
        UUID uuid = UUID.randomUUID();
        return uuid.toString();
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    /**
     * Function to create content values object from model values. Override for child classes.
     * @return content values object
     */
    public ContentValues makeContentValues() {
        ContentValues values = new ContentValues();
        String id = getId();
        if(id == null || id.isEmpty()) {
            setId(generateID());
        }
        values.put("ID", getId());
        values.put("Modified", globalClass.databaseHelper.formatDateForSQL(new Date()));
        values.put("Deleted", getDeleted());
        return values;
    }

    /**
     * Load values from database cursor
     * @param c cursor
     * @param dt data table
     */
    public void fromCursor(Cursor c, DataTable dt) {
        setId(c.getString(c.getColumnIndex(dt.getUniversalColReference("ID"))));
        setCreatedDate(globalClass.databaseHelper.dateFromSQLString(c.getString(c.getColumnIndex(dt.getUniversalColReference("Created")))));
        setLastModified(globalClass.databaseHelper.dateFromSQLString(c.getString(c.getColumnIndex(dt.getUniversalColReference("Modified")))));
        setDeleted(c.getInt(c.getColumnIndex(dt.getUniversalColReference("Deleted"))) > 0);
    }

    /**
     * Function to save this instance to the database
     */
    public void saveToDB(){
        if(id!=null) {
            Cursor c = globalClass.databaseHelper.getTableItemByID(id, dataTable);
            if (c.getCount() > 0) {
                globalClass.databaseHelper.updateTableItem(makeContentValues(), id, dataTable);
                return;
            }
            globalClass.databaseHelper.createTableItem(makeContentValues(), dataTable);
        }
    }


}
