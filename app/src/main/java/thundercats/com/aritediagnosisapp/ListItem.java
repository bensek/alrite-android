package thundercats.com.aritediagnosisapp;

import androidx.annotation.NonNull;

/**
 * Container for list item shown in searchable lists. Has title and subtitle text (definition) as well as ID.
 * Extends comparable for sortability
 */

public class ListItem implements Comparable {
    private String title, definition, id;

    /**
     * Constructor takes all three params
     *
     * @param title      Text to display at top of list item card
     * @param definition text to display as subtitle
     * @param id         card reference ID for actionable list items
     */
    public ListItem(String title, String definition, String id) {
        this.title = title;
        this.definition = definition;
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDefinition() {
        return definition;
    }

    public void setDefinition(String definition) {
        this.definition = definition;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    /**
     * Default sort (now commented out) is by title alphabetically, currently altered to sort by time left to wait
     * @param o
     * @return
     */
    @Override
    public int compareTo(@NonNull Object o) {
        return title.toLowerCase().compareTo(((ListItem) o).getTitle().toLowerCase());
    }
}
