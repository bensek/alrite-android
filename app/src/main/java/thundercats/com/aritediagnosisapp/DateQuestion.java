package thundercats.com.aritediagnosisapp;

import android.app.DatePickerDialog;
import android.icu.text.LocaleDisplayNames;
import android.text.InputType;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * Date input question type.
 */

public class DateQuestion extends BaseQuestion {
    private Date value;
    private int years, months, days;
    private long earlyBound,lateBound;
    private TextView displayAge;
    private EditText editText;
    private Button submitButton,picker;
    private DatePickerDialog datePickerDialog;
    private SimpleDateFormat dateFormatter;
    private String beforeEarly, inBounds, afterLate;
    private View.OnClickListener editTextListener = new View.OnClickListener() {

        @Override
        public void onClick(View view) {
            datePickerDialog.show();

        }
    };
    private View.OnClickListener submitButtonListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (editText.getText().toString().isEmpty()) {
                return;
            }
            if (value != null) {
                afterClick();
            }
        }
    };



    public DateQuestion(String ID, QuestionSection section, String glossaryTerm, int titleTextResID, int subtitleTextResID, long earlyBound, long lateBound, String beforeEarly, String inBounds, String afterLate) {
        super(ID, section, glossaryTerm, titleTextResID, subtitleTextResID);
        this.earlyBound = earlyBound;
        this.lateBound = lateBound;
        this.beforeEarly = beforeEarly;
        this.inBounds = inBounds;
        this.afterLate = afterLate;
    }

    public DateQuestion(String ID, QuestionSection section, String glossaryTerm, String DBColumnID, int titleTextResID, int subtitleTextResID, long earlyBound, long lateBound, String beforeEarly, String inBounds, String afterLate) {
        super(ID, section, glossaryTerm, DBColumnID, titleTextResID, subtitleTextResID);
        this.earlyBound = earlyBound;
        this.lateBound = lateBound;
        this.beforeEarly = beforeEarly;
        this.inBounds = inBounds;
        this.afterLate = afterLate;
    }

    public DateQuestion(String ID, String DBColumnID, int titleTextResID, int subtitleTextResID,
                        long earlyBound, long lateBound, String beforeEarly, String inBounds, String afterLate, QuestionSection section) {
        super(ID, DBColumnID, titleTextResID, subtitleTextResID,section);
        this.earlyBound = earlyBound;
        this.lateBound = lateBound;
        this.beforeEarly = beforeEarly;
        this.inBounds = inBounds;
        this.afterLate = afterLate;
    }

    public DateQuestion(String ID, int titleTextResID, int subtitleTextResID,long earlyBound,
                        long lateBound, String beforeEarly, String inBounds, String afterLate, QuestionSection section) {
        super(ID, titleTextResID, subtitleTextResID,section);
        this.earlyBound = earlyBound;
        this.lateBound = lateBound;
        this.beforeEarly = beforeEarly;
        this.inBounds = inBounds;
        this.afterLate = afterLate;
    }

    public DateQuestion(String ID, int titleTextResID, int subtitleTextResID, QuestionSection section) {
        super(ID, titleTextResID, subtitleTextResID, section);
    }

    public DateQuestion(String ID, String DBColumnID, int titleTextResID, int subtitleTextResID, QuestionSection section) {
        super(ID, DBColumnID, titleTextResID, subtitleTextResID, section);
    }

    /**
     * Sets up the date question layout with submit button and date picker.
     */
    @Override
    public void setupView() {
        super.setupView();
        fillNavigationContent(R.layout.submit_layout);
        fillCardContent(R.layout.date_question_layout_full);
        displayAge = getHostActivity().findViewById(R.id.displayAge);
        displayAge.setVisibility(View.GONE);
        picker = getHostActivity().findViewById(R.id.dateChange);
        picker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                datePickerDialog.show();
            }
        });
        linkDateView();
        linkSubmitButtonView();
        setListeners();
        setDateTimeField();

    }

    /**
     * Returns date value as SQL string
     *
     * @return date as SQL string
     */
    @Override
    public String getDBValue() {
        return formatSQLDate();
    }

    /**
     * Dictates action based on date bounds - returns appropriate next question ID
     * @return
     */
    @Override
    public String nextNavigation() {
        if(isBeforeEarlyBound()){
            return beforeEarly;
        } else if(isAfterLateBound()){
            return afterLate;
        } else{
            return inBounds;
        }
    }

    /**
     * links datepicker to var
     */
    public void linkDateView(){
        setEditText((EditText)getHostActivity().findViewById(R.id.EditText));
        editText.setInputType(InputType.TYPE_NULL);
    }

    public EditText getEditText() {
        return editText;
    }

    public void setEditText(EditText editText) {
        this.editText = editText;
    }

    public Button getSubmitButton() {
        return submitButton;
    }

    public void setSubmitButton(Button submitButton) {
        this.submitButton = submitButton;
    }

    public SimpleDateFormat getDateFormatter() {
        return dateFormatter;
    }

    public void setDateFormatter(SimpleDateFormat dateFormatter) {
        this.dateFormatter = dateFormatter;
    }

    public Date getValue() {
        return value;
    }

    public void setValue(Date value) {
        this.value = value;
    }

    public String formatSQLDate(){
        DateFormat dateFormatISO8601 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String strDob = dateFormatISO8601.format(getValue());
        return strDob;
    }

    public void linkSubmitButtonView(){
        setSubmitButton((Button)getHostActivity().findViewById(R.id.SubmitButton));
    }

    public void setListeners(){
        editText.setOnClickListener(editTextListener);
        submitButton.setOnClickListener(submitButtonListener);
    }

    public void setSubmitButtonListener(View.OnClickListener submitButtonListener) {
        this.submitButtonListener = submitButtonListener;
        setListeners();
    }

    /**
     * Adds formatted text to textbox once date has been selected. Formatted based on locale.
     */
    public void setDateTimeField() {

        Calendar newCalendar = Calendar.getInstance();
        datePickerDialog = new DatePickerDialog(this.getHostActivity(),
                android.R.style.Theme_Holo_Light_Dialog,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        Calendar newDate = Calendar.getInstance();
                        newDate.set(year, monthOfYear, dayOfMonth);
                        setValue(newDate.getTime());
                        editText.setText(((GlobalClass) getHostActivity().getApplication()).formatLocaleDate(newDate.getTime()));
                        showAge(displayAge, value);
                    }

                },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
    }

    public boolean isBeforeEarlyBound(){
        return (fromNow()<earlyBound);
    }

    public boolean isAfterLateBound() {
        return (fromNow()>lateBound);
    }

    public long fromNow(){
        return value.getTime()-(new Date().getTime());
    }

    public void showAge(TextView displayAge, Date value) {
        Calendar birthday = new GregorianCalendar();
        birthday.setTime(value);
        Calendar today = new GregorianCalendar();
        years = today.get(Calendar.YEAR)-birthday.get(Calendar.YEAR);
        months = today.get(Calendar.MONTH) - birthday.get(Calendar.MONTH);

        if(years < 1){
            displayAge.setText("Patient is " + months + " months old");
        } else if (years == 1 && months<0) {
            months = 12 + months;
            displayAge.setText("Patient is " + months + " months old");
        } else {
            displayAge.setText("Patient is " + years + " years old");
        }


        displayAge.setVisibility(View.VISIBLE);
    }

    /**
     * Resets question
     */
    @Override
    public void reset() {
        super.reset();
        value = null;
        if (editText != null) {
            editText.setText("");
        }
        displayAge.setVisibility(View.GONE);
    }

    /**
     * Shows previously selected date value in textbox if user navigates back.
     */
    @Override
    public void renderPrevious() {
        editText.setText(((GlobalClass) getHostActivity().getApplication()).formatLocaleDate(value));
        showAge(displayAge, value);

    }
}
