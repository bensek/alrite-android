package thundercats.com.aritediagnosisapp;

import java.util.LinkedList;


/**
 * Class for generating list of visited and answered questions within question section.
 * Helps users navigate back and forth within diagnosis.
 */

public class NavigationGenerator {
    private LinkedList<BaseQuestion> navigationStack;
    private QuestionSection section;

    /**
     * constructor takes current question section
     *
     * @param section question section to generate navigation for
     */
    public NavigationGenerator(QuestionSection section) {
        this.section = section;
        navigationStack = new LinkedList<>();
    }

    /**
     * Method to generate list of previously answered questions. Loops through questions and checks if they are completed.
     */
    public void generateList(){
        navigationStack.clear();
        BaseQuestion first = section.getFirstQuestion();
        BaseQuestion current = first;

        while(true){
            if(current==null){
                break;
            }
            navigationStack.add(current);
            if (current.isCompleted()) {
                current = section.getItemByID(current.nextNavigation());
            } else {
                break;
            }
        }
    }

    public LinkedList<BaseQuestion> getList() {
        generateList();
        return navigationStack;
    }
}
