package thundercats.com.aritediagnosisapp;

import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.View;
import android.widget.AdapterView;

import java.util.ArrayList;

/**
 * This activity represents the glossary or 'learn' page. It displays a searchable list of glossary terms.
 * The terms are clickable and yield a popup showing more detailed information.
 */

public class GlossaryActivity extends SearchableListViewHelper {


    /**
     * Change the toolbar title on creation of the page
     */
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setToolbarTextID(R.string.glossary_text);
    }

    /**
     * Sets up the ListView to show the list of ListItems from pre-generated glossary items list.
     */
    @Override
    public void onStart() {
        super.onStart();
        ArrayList<ListItem> tmp = new ArrayList<ListItem>(globalClass.glossaryItems);
        setArrayList(tmp);
        listener();
        updateListView();
        updateListListener();
    }

    /**
     * Set the click listener for glossary items to show the corresponding popup
     */
    public void listener() {
        setItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> adapter, View v, int position,
                                    long arg3) {
                ((GlossaryItem) adapter.getItemAtPosition(position)).showPopup(GlossaryActivity.this, false);
            }

        });

    }
}