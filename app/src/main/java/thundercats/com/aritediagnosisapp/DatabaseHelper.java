package thundercats.com.aritediagnosisapp;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.ContactsContract;
import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * Class to help with database interactions
 */

public class DatabaseHelper extends SQLiteOpenHelper{

    private static final String DATABASE_NAME = "ALRITE.db";
    private static final int DATABASE_VERSION = 13;
    private static final String PROVIDER_TABLE_NAME = "provider_table";

    private static final String[] PROVIDER_COLS = {"Name","Birthday","Gender","Organization","Years_Experience","Language"};
    private static final String[] PROVIDER_COLS_TYPES = {"TEXT","LONG","TEXT","TEXT","INTEGER","TEXT"};

    private static final String PATIENT_TABLE_NAME = "patient_table";
//    private static final String[] PATIENT_COLS= {"Name","Birthday","Gender", "Weight"};

    private static final String[] PATIENT_COLS= {"Surname","Givenname", "Nickname","Birthday","Gender", "Weight", "Number"};
//    private static final String[] PATIENT_COLS_TYPES = {"TEXT", "INTEGER", "TEXT", "TEXT"};
    private static final String[] PATIENT_COLS_TYPES = {"TEXT","TEXT", "TEXT",  "INTEGER", "TEXT", "TEXT", "TEXT"};


    private static final String REPORT_TABLE_NAME = "report_table";
    private static final String[] REPORT_COLS = {"Latitude", "Longitude", "Patient_ID", "Diagnosis_Title", "general_danger_checklist", "main_symptoms_difficulty", "main_symptoms_how_long", "main_symptoms_fever", "main_symptoms_temp",
            "main_symptoms_stridor","main_symptoms_hiv", "main_symptoms_past_wheezing", "respiratory_rate_baseline",
            "main_symptoms_indrawing","main_symptoms_oxygen","main_symptoms_wheezing", "not_given", "reevaluation_respiratory_rate", "reevaluation_oxygen", "reevaluation_improved",
            "reevaluation_indrawing", "reevaluation_wheezing", "respiratory_score", "respiratory_score_final"};
    private static final String[] REPORT_COLS_TYPES = {"REAL", "REAL", "TEXT", "TEXT", "TEXT", "TEXT", "TEXT", "TEXT", "TEXT", "TEXT", "TEXT", "TEXT", "TEXT",
            "TEXT", "TEXT", "TEXT", "TEXT", "TEXT", "TEXT", "TEXT", "TEXT", "TEXT", "TEXT", "TEXT"};

    private static final String SCREENTIME_TABLE_NAME = "screentime_table";
    private static final String[] SCREENTIME_COLS = {"total_assessments", "general_danger_checklist", "main_symptoms_difficulty", "main_symptoms_how_long", "main_symptoms_fever", "main_symptoms_temp",
            "main_symptoms_stridor","main_symptoms_hiv", "main_symptoms_past_wheezing", "respiratory_rate_baseline",
            "main_symptoms_indrawing","main_symptoms_oxygen","main_symptoms_wheezing", "not_given", "reevaluation_respiratory_rate", "reevaluation_oxygen", "reevaluation_improved",
            "reevaluation_indrawing", "reevaluation_wheezing"};
    private static final String[] SCREENTIME_COLS_TYPES = {"LONG", "LONG", "LONG", "LONG", "LONG", "LONG", "LONG", "LONG", "LONG", "LONG",
            "LONG", "LONG", "LONG", "LONG", "LONG", "LONG", "LONG", "LONG", "LONG"};

    private static final String CLINIC_TABLE_NAME = "clinic_table";
    private static final String[] CLINIC_COLS= {"resources_set","bronchodilator","spacer", "pulse_ox", "stethoscope", "thermometer", "nebulizer", "supp_ox", "antibiotics", "steroids"};
    private static final String[] CLINIC_COLS_TYPES = {"TEXT", "TEXT", "TEXT", "TEXT", "TEXT", "TEXT", "TEXT", "TEXT", "TEXT", "TEXT"};

    public DataTable PROVIDER, PATIENT, REPORT, CLINIC, SCREENTIME;
    private DataTable[] TABLES;



    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);

        PROVIDER = new DataTable(PROVIDER_TABLE_NAME,PROVIDER_COLS,PROVIDER_COLS_TYPES);
        PATIENT = new DataTable(PATIENT_TABLE_NAME,PATIENT_COLS,PATIENT_COLS_TYPES);
        REPORT = new DataTable(REPORT_TABLE_NAME,REPORT_COLS,REPORT_COLS_TYPES);
        CLINIC = new DataTable(CLINIC_TABLE_NAME,CLINIC_COLS,CLINIC_COLS_TYPES);
        SCREENTIME = new DataTable(SCREENTIME_TABLE_NAME, SCREENTIME_COLS, SCREENTIME_COLS_TYPES);
        TABLES = new DataTable[]{PROVIDER, PATIENT, REPORT, CLINIC, SCREENTIME};
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        for(int ii=0;ii<TABLES.length;ii++){
            db.execSQL(TABLES[ii].getCreateString());
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        for (int ii = 0; ii < TABLES.length; ii++) {
            db.execSQL(TABLES[ii].getDropString());
        }
        onCreate(db);
    }

    public void closeDB() {
        SQLiteDatabase db = this.getReadableDatabase();
        if (db != null && db.isOpen())
            db.close();
    }

    /**
     * Creates new entry in specified data table
     *
     * @param contentValues values to add
     * @param dt            target table
     * @return whether insertion was successful
     */
    public boolean createTableItem(ContentValues contentValues, DataTable dt) {
        SQLiteDatabase db = this.getWritableDatabase();

        long result = db.insert(dt.getName(), null, contentValues);
        db.close();
        return result > 0;
    }

    /**
     * Get cursor representing item from specified table by ID
     * @param id item ID
     * @param dt data table
     * @return database cursor with selected item
     */
    public Cursor getTableItemByID(String id, DataTable dt) {
        return getTableItemsByCol("ID", id, dt);
    }

    /**
     * Get items by column and value from specified table
     * @param colName column to check
     * @param val value to filter by
     * @param dt data table
     * @return cursor with results
     */
    public Cursor getTableItemsByCol(String colName, String val, DataTable dt) {
        String selectQuery = "SELECT " + dt.getUniversalColumnQuery(new String[]{});
        selectQuery += dt.getFromString();
        selectQuery += " WHERE " + dt.getEqualsSQL(colName, null);

        return runCustomQuery(selectQuery, new String[]{val});
    }

    /**
     * Gets all items from specified table
     * @param dt table
     * @return cursor with results
     */
    public Cursor getAllTableItems(DataTable dt) {
        String selectQuery = "SELECT " + dt.getUniversalColumnQuery(new int[]{});
        selectQuery += dt.getFromString();

        return runCustomQuery(selectQuery, null);
    }

    /**
     * Performs custom query on database
     * @param query query string
     * @param args arguments
     * @return cursor with results
     */
    public Cursor runCustomQuery(String query, String[] args) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(query, args);
        if (c != null)
            c.moveToFirst();
        db.close();
        return c;
    }

    /**
     * Updates item in specified table
     * @param contentValues new values to be updated
     * @param id entry id
     * @param dt data table
     * @return whether update was successful
     */
    public boolean updateTableItem(ContentValues contentValues, String id , DataTable dt) {
        SQLiteDatabase db = this.getWritableDatabase();

        int result = db.update(dt.getName(), contentValues, dt.getEqualsSQL("ID", null), new String[]{id});
        db.close();
        return result > 0;
    }

    /**
     * Removes table item from specified table
     * @param id item id
     * @param dt table
     * @return success or fail
     */
    public boolean deleteTableItem(String id, DataTable dt) {
        SQLiteDatabase db = this.getWritableDatabase();
        int result = db.delete(dt.getName(), dt.getEqualsSQL("ID", null), new String[]{id});
        db.close();
        return result > 0;
    }

    /**
     * Creates a table join string connecting providers to patients
     * @param patientCols columns to select from patients. empty for all
     * @param reportCols columns to select from reports. empty for all
     * @param providerCols columns to select from providers. empty for all
     * @return custom query string
     */
    public String getProviderToPatientTableJoinString(int[] patientCols, int[] reportCols, int[] providerCols) {
        String selectQuery = "SELECT ";
        selectQuery += PROVIDER.getUniversalColumnQuery(providerCols) + ", ";
        selectQuery += REPORT.getUniversalColumnQuery(reportCols) + ", ";
        selectQuery += PATIENT.getUniversalColumnQuery(patientCols);
        selectQuery += PROVIDER.getFromString();
        selectQuery += getTwoTableInnerJoinString(PROVIDER, "ID", REPORT, "Provider_ID");
        selectQuery += getTwoTableInnerJoinString(REPORT, "Patient_ID", PATIENT, "ID");

        return selectQuery;
    }

    /**
     * Creates a table join string connecting patients to providers
     * @param patientCols columns to select from patients. empty for all
     * @param reportCols columns to select from reports. empty for all
     * @param providerCols columns to select from providers. empty for all
     * @return custom query string
     */
    public String getPatientToProviderTableJoinString(int[] patientCols, int[] reportCols, int[] providerCols) {
        String selectQuery = "SELECT ";
        selectQuery += PATIENT.getUniversalColumnQuery(patientCols) + ", ";
        selectQuery += REPORT.getUniversalColumnQuery(reportCols) + ", ";
        selectQuery += PROVIDER.getUniversalColumnQuery(providerCols);
        selectQuery += PATIENT.getFromString();
        selectQuery += getTwoTableInnerJoinString(PATIENT, "ID", REPORT, "Patient_ID");
        selectQuery += getTwoTableInnerJoinString(REPORT, "Provider_ID", PROVIDER, "ID");

        return selectQuery;
    }

    /**
     * Generates join string for tables
     * @param t_1 first table
     * @param col_1 column index to join on table 1
     * @param t_2 table 2
     * @param col_2 column index to join on table 2
     * @return custom join string
     */
    public String getTwoTableInnerJoinString(DataTable t_1, int col_1, DataTable t_2, int col_2) {
        String selectQuery = "";
        selectQuery += " INNER JOIN ";
        selectQuery += t_2.getName() + " ON " + t_2.getUniversalColReference(col_2);
        selectQuery += " = " + t_1.getUniversalColReference(col_1);
        return selectQuery;
    }

    /**
     * Generates join string for tables
     * @param t_1 first table
     * @param col_1 column name to join on table 1
     * @param t_2 table 2
     * @param col_2 column name to join on table 2
     * @return custom join string
     */
    public String getTwoTableInnerJoinString(DataTable t_1, String col_1, DataTable t_2, String col_2) {
        String selectQuery = "";
        selectQuery += " INNER JOIN ";
        selectQuery += t_2.getName() + " ON " + t_2.getUniversalColReference(col_2);
        selectQuery += " = " + t_1.getUniversalColReference(col_1);
        return selectQuery;
    }

    /**
     * Creates new provider
     * @param contentValues values to add
     * @return success or failure
     */
    public boolean createProvider(ContentValues contentValues){
        return createTableItem(contentValues, PROVIDER);
    }

    /**
     * Creates new patient
     * @param contentValues values to add
     * @return success or failure
     */
    public boolean createPatient(ContentValues contentValues){
        return createTableItem(contentValues, PATIENT);
    }

    /**
     * Creates new report
     * @param contentValues values to add
     * @return success or failure
     */
    public boolean createReport(ContentValues contentValues){
        return createTableItem(contentValues, REPORT);
    }

    /**
     * Creates new clinic resources
     * @param contentValues values to add
     * @return success or failure
     */
    public boolean createClinic(ContentValues contentValues){
        return createTableItem(contentValues, CLINIC);
    }

    /**
     * Gets all providers that have diagnosed specified patient
     * @param id patient ID
     * @return cursor with results
     */
    public Cursor getProvidersByPatient(String id){
        String selectQuery = "SELECT " + PROVIDER.getUniversalColumnQuery(new String[]{});
        selectQuery += PROVIDER.getFromString();
        selectQuery += getPatientToProviderTableJoinString(new int[]{}, new int[]{}, new int[]{});
        selectQuery += " WHERE " + PATIENT.getEqualsSQL("ID", null);

        return runCustomQuery(selectQuery, new String[]{id});
    }

    /**
     * Gets all reports for specified patient
     * @param id patient ID
     * @return cursor with results
     */
    public Cursor getReportsByPatient(String id){
        String selectQuery = "SELECT " + REPORT.getUniversalColumnQuery(new String[]{});
        selectQuery += REPORT.getFromString();
        selectQuery += getTwoTableInnerJoinString(REPORT, "Patient_ID", PATIENT, "ID");
        selectQuery += " WHERE " + PATIENT.getEqualsSQL("ID", null);

        return runCustomQuery(selectQuery, new String[]{id});
    }

    /**
     * Gets all patients diagnosed by provider
     * @param id provider ID
     * @return cursor with results
     */
    public Cursor getPatientsByProvider(String id){
        String selectQuery = "SELECT " + PATIENT.getUniversalColumnQuery(new String[]{});
        selectQuery += PATIENT.getFromString();
        selectQuery += getProviderToPatientTableJoinString(new int[]{}, new int[]{}, new int[]{});
        selectQuery += " WHERE " + PROVIDER.getEqualsSQL("ID", null);

        return runCustomQuery(selectQuery, new String[]{id});
    }

    /**
     * Gets all reports created by provider
     * @param id provider ID
     * @return cursor with results
     */
    public Cursor getReportsByProvider(String id){
        String selectQuery = "SELECT " + REPORT.getUniversalColumnQuery(new String[]{});
        selectQuery += REPORT.getFromString();
        selectQuery += getTwoTableInnerJoinString(REPORT, "Patient_ID", PROVIDER, "ID");
        selectQuery += " WHERE " + PROVIDER.getEqualsSQL("ID", null);

        return runCustomQuery(selectQuery, new String[]{id});
    }

    /**
     * Gets all providers
     * @return cursor with results
     */
    public Cursor getAllProviders() {
        return getAllTableItems(PROVIDER);
    }

    /**
     * Gets all patients
     * @return cursor with results
     */
    public Cursor getAllPatients() {
        return getAllTableItems(PATIENT);
    }

    /**
     * Gets all reports
     * @return cursor with results
     */
    public Cursor getAllReports() {
        return getAllTableItems(REPORT);
    }

    public void debug(String msg) {
        Log.d("DatabaseHelper", msg);
    }

    /**
     * Converts date to standard SQL UTC string
     * @param d date
     * @return converted date string
     */
    public String formatDateForSQL(Date d) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
        return formatter.format(d);
    }

    /**
     * SQL date string to UTC date
     * @param s date string
     * @return parsed date
     */
    public Date dateFromSQLString(String s) {
        Date date = null;
        SimpleDateFormat iso8601Format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        iso8601Format.setTimeZone(TimeZone.getTimeZone("UTC"));
        try {
            date = iso8601Format.parse(s);
        } catch (Exception e) {
            debug("Parsing ISO8601 datetime failed \n" + e);
        }
        return date;
    }
}
