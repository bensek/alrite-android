package thundercats.com.aritediagnosisapp;

import android.view.View;
import android.widget.Button;

/**
 * Question with three buttons to establish ranked baseline. None | A little | A lot
 * Same methods as all other question types
 */

public class ThreeOptionQuestion extends BaseQuestion {
    private String[] next;
    private String continueID;
    private int value;
    private Button button1,button2,button3, button4;
    private View.OnClickListener button1Listener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            setValue(0);
            afterClick();
        }
    };
    private View.OnClickListener button2Listener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            setValue(2);
            afterClick();
        }
    };
    private View.OnClickListener button3Listener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            setValue(3);
            afterClick();
        }
    };
    private View.OnClickListener button4Listener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            setValue(1);
            afterClick();
        }
    };

    public ThreeOptionQuestion(String ID, QuestionSection section, String glossaryTerm, int titleTextResID, int subtitleTextResID, String continueID) {
        super(ID, section, glossaryTerm, titleTextResID, subtitleTextResID);
        this.continueID = continueID;
    }

    public ThreeOptionQuestion(String ID, QuestionSection section, String glossaryTerm, String DBColumnID, int titleTextResID, int subtitleTextResID, String continueID) {
        super(ID, section, glossaryTerm, DBColumnID, titleTextResID, subtitleTextResID);
        this.continueID = continueID;
    }

    public ThreeOptionQuestion(String ID, QuestionSection section, String glossaryTerm, int titleTextResID, int subtitleTextResID, String[] next) {
        super(ID, section, glossaryTerm, titleTextResID, subtitleTextResID);
        this.next = next;
    }

    public ThreeOptionQuestion(String ID, QuestionSection section, String glossaryTerm, String DBColumnID, int titleTextResID, int subtitleTextResID, String[] next) {
        super(ID, section, glossaryTerm, DBColumnID, titleTextResID, subtitleTextResID);
        this.next = next;
    }

    public ThreeOptionQuestion(String ID, int titleTextResID, int subtitleTextResID, String continueID, QuestionSection section) {
        super(ID, titleTextResID, subtitleTextResID, section);
        this.continueID = continueID;
    }

    public ThreeOptionQuestion(String ID, String DBColumnID, int titleTextResID, int subtitleTextResID, String continueID, QuestionSection section) {
        super(ID, DBColumnID, titleTextResID, subtitleTextResID, section);
        this.continueID = continueID;
    }

    public ThreeOptionQuestion(String ID, int titleTextResID, int subtitleTextResID, String[] next, QuestionSection section) {
        super(ID, titleTextResID, subtitleTextResID, section);
        this.next = next;
    }

    public ThreeOptionQuestion(String ID, String DBColumnID, int titleTextResID, int subtitleTextResID, String[] next, QuestionSection section) {
        super(ID, DBColumnID, titleTextResID, subtitleTextResID, section);
        this.next = next;
    }

    @Override
    public void setupView() {
        super.setupView();
        fillNavigationContent(R.layout.three_button_layout_full);
        linkButton1View();
        linkButton2View();
        linkButton3View();
        linkButton4View();
        setListeners();
    }

    @Override
    public String getDBValue() {
        return getValue()+"";
    }

    @Override
    public String nextNavigation() {
        if(continueID!=null) {
            if (!continueID.isEmpty()) {
                return continueID;
            }
        }
        if (value > -1 && next != null) {
            return next[value];
        }
        return "";
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public void linkButton1View(){
        button1 = getHostActivity().findViewById(R.id.Button1);
    }

    public void linkButton2View(){
        button2 = getHostActivity().findViewById(R.id.Button2);
    }

    public void linkButton3View(){
        button3 = getHostActivity().findViewById(R.id.Button3);
    }

    public void linkButton4View(){
        button4 = getHostActivity().findViewById(R.id.Button4);
    }

    public View.OnClickListener getButton1Listener() {
        return button1Listener;
    }

    public void setButton1Listener(View.OnClickListener button1Listener) {
        this.button1Listener = button1Listener;
    }

    public View.OnClickListener getButton2Listener() {
        return button2Listener;
    }

    public void setButton2Listener(View.OnClickListener button2Listener) {
        this.button2Listener = button2Listener;
    }

    public View.OnClickListener getButton3Listener() {
        return button3Listener;
    }

    public void setButton3Listener(View.OnClickListener button3Listener) {
        this.button3Listener = button3Listener;
    }

    public View.OnClickListener getButton4Listener() {
        return button4Listener;
    }

    public void setButton4Listener(View.OnClickListener button4Listener) {
        this.button4Listener = button4Listener;
    }

    public void setListeners(){
        button1.setOnClickListener(getButton1Listener());
        button2.setOnClickListener(getButton2Listener());
        button3.setOnClickListener(getButton3Listener());
        button4.setOnClickListener(getButton4Listener());
    }

    @Override
    public void reset() {
        super.reset();
        value = -1;
    }

    @Override
    public void renderPrevious() {
        switch (value) {
            case 0:
                button1.setBackgroundResource(R.drawable.none_button_focused);
                break;
            case 1:
                button4.setBackgroundResource(R.drawable.mild_button_focused);
                break;
            case 2:
                button2.setBackgroundResource(R.drawable.mild_button_focused);
                break;
            case 3:
                button3.setBackgroundResource(R.drawable.moderate_button_focused);
                break;

        }
    }
}
