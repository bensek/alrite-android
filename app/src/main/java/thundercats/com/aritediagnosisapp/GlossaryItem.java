package thundercats.com.aritediagnosisapp;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.res.AssetManager;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import java.io.IOException;
import java.util.ArrayList;

/**
 * This class represents an individual item within the glossary. These items are all required to have a term, short and long definitions.
 * The term is simply the word or phrase to display, short definition will be displayed on the listview, and long definition in the detailed view popup.
 * Each term can also include optional items such as an image, video, steps and a pdf. These items will automatically generate the appropriate cards.
 *
 * Each id matches the base naming convention used to generate card types.
 * termID_glossary_def for main definition (required for all terms) string array in strings.xml
 * termID_glossary_video for associated video (optional) located in res/raw
 * termID_glossary_image for associated image (optional) res/drawable
 * termID_glossary_pdf for associated pdf (optional) assets folder
 * termID_glossary_steps for associated numbered steps (optional) string array in strings.xml
 *
 * extends ListItem to make term list generation easier within searchablelistview
 */

public class GlossaryItem extends ListItem {
    public ArrayList<BaseDynamicCard> glossaryCards;
    private GlobalClass globalClass;

    /**
     * Constructor takes definitions, item ID and creates base ListItem class as well as auto generates any optional cards such as image or video if matching id is found.
     *
     * @param terms       array of strings with term, short def and long def. Needed for ListItem constructor
     * @param id          item id
     * @param globalClass reference to global application class
     */
    public GlossaryItem(String[] terms, String id, GlobalClass globalClass) {
        super(terms[0], terms[1], id);
        this.globalClass = globalClass;
        glossaryCards = new ArrayList<>();

        if (id == "readyAssessment" || id == "soonAssessment") {
            makeAlertCard();
        } else {
            makeImageCard();
            if (terms[2] != null) {
                makeDefinitionCard(terms[2]);
            } else {
                makeDefinitionCard(terms[1]);
            }

            makeStepsCard();
            makeVideoCard();
            makePDFCard();
        }
    }

    /**
     * This method handles displaying the popup over the current activity.
     * Popup is scrollable if content exceeds page height and is dismissible.
     * All cards (if generated) will be displayed on the popup according to generation order.
     * @param context current activity to display popup over.
     */
    public void showPopup(Activity context, boolean topbarColor) {
        final Dialog dialog = new Dialog(context);
        final AlertDialog.Builder alert;
        alert = new AlertDialog.Builder(context);
        LayoutInflater inflater = context.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dynamic_card_popup_layout, null);
        ((TextView) dialogView.findViewById(R.id.Title)).setText(getTitle());
        inflateAll(inflater, (LinearLayout) dialogView.findViewById(R.id.MainContainer));
        (dialogView.findViewById(R.id.respScoreInfo)).setVisibility(View.GONE);
        (dialogView.findViewById(R.id.respScoreInfo2)).setVisibility(View.GONE);
        ScrollView scrollView = dialogView.findViewById(R.id.ScrollView);
        scrollView.smoothScrollTo(0, 0);
        if(topbarColor) {
            if(getId() == "soonAssessment") {
                dialogView.findViewById(R.id.TopBar).setBackgroundColor(dialogView.getResources().getColor(R.color.buttonHighlight));
            }
            else {
                dialogView.findViewById(R.id.TopBar).setBackgroundColor(dialogView.getResources().getColor(R.color.severeDiagnosisColor));
            }
        }
        alert.setPositiveButton("OK", null);  //This is my Solution to this question(adding OK button)
        alert.setCancelable(true);
        alert.setInverseBackgroundForced(true);
        alert.setView(dialogView);
        alert.show();
    }

    /**
     * Method searches if the glossary image resource exists and builds the corresponding card if it does.
     * Appends image card to glossary term.
     */
    public void makeImageCard() {
        int imgRes = globalClass.getResourceIdByName(getId() + "_glossary_image", "drawable");
        if (imgRes != 0) {
            ImageCard imageCard = new ImageCard(imgRes);
            glossaryCards.add(imageCard);
        }
    }

    public void makeAlertCard() {
        String[] steps = globalClass.getWaitingPatients();
        if(getId() == "readyAssessment"){
            NumberedStepsCard numberedStepsCard = new NumberedStepsCard(steps, true, R.string.notifyAssessments);
            glossaryCards.add(numberedStepsCard);
        } else {
            NumberedStepsCard numberedStepsCard = new NumberedStepsCard(steps, true, R.string.soon_alert_text);
            glossaryCards.add(numberedStepsCard);
        }

    }

    /**
     * Method searches if the glossary definition resource exists and builds the corresponding card if it does.
     * Appends definition card to glossary term.
     */
    public void makeDefinitionCard(String def) {
        DefinitionCard definitionCard;
        if (def.isEmpty()) {
            definitionCard = new DefinitionCard(getDefinition());
        } else {
            definitionCard = new DefinitionCard(def);
        }
        glossaryCards.add(definitionCard);
    }

    /**
     * Method searches if the glossary steps resource exists and builds the corresponding card if it does.
     * Appends steps card to glossary term.
     */
    public void makeStepsCard() {
        int stepsRes = globalClass.getResourceIdByName(getId() + "_glossary_steps", "array");
        if (stepsRes != 0) {
            String[] steps = globalClass.getResources().getStringArray(stepsRes);
            NumberedStepsCard numberedStepsCard = new NumberedStepsCard(steps);
            glossaryCards.add(numberedStepsCard);
        }
    }

    /**
     * Method searches if the glossary video resource exists and builds the corresponding card if it does.
     * Appends video card to glossary term.
     */
    public void makeVideoCard() {
        int vidRes = globalClass.getResourceIdByName(getId() + "_glossary_video", "raw");
        if (vidRes != 0) {
            Uri uri2 = Uri.parse("android.resource://" + globalClass.getPackageName() + "/" + vidRes);
            VideoCard videoCard = new VideoCard(uri2);
            glossaryCards.add(videoCard);
        }
    }

    /**
     * Method searches if the glossary pdf resource exists and builds the corresponding card if it does.
     * Appends pdf card to glossary term.
     */
    public void makePDFCard() {
        AssetManager assetManager = globalClass.getAssets();
        try {
            String[] assets = assetManager.list("");
            for (String s : assets) {
                if (s.equals(getId() + "_glossary_pdf.pdf")) {
                    PDFCard pdfCard = new PDFCard(getId() + "_glossary_pdf.pdf");
                    glossaryCards.add(pdfCard);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Inflates all cards in glossaryCards list.
     * @param inflater layout inflater of current activity
     * @param container linear layout where to populate glossary cards. Currently inside the popup.
     */
    public void inflateAll(LayoutInflater inflater, LinearLayout container) {
        for (BaseDynamicCard bc : glossaryCards) {
            container.addView(bc.inflateCard(inflater, container.getContext()));
        }
    }
}
