package thundercats.com.aritediagnosisapp;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

import thundercats.com.aritediagnosisapp.activities.MainActivity;

/**
 * Activity to host dynamic items
 */

public class DynamicBaseActivity extends AppCompatActivity {
    protected BaseDynamicItem baseItem;
    protected String ID;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(savedInstanceState!=null){
            ID = savedInstanceState.getString("ID");
        }
        else {
            Intent intent = getIntent();
            ID = intent.getStringExtra("ID");
        }
    }

    /**
     * Captures back arrow on toolbar and sets action to back button equivalent
     *
     * @return true
     */
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    /**
     * Loads the dynamic item and calls setupView to render it
     */
    @Override
    protected void onStart() {
        if((((GlobalClass) this.getApplication())).currentPatient == null){
            return;
        }
        setBaseItem(((GlobalClass) this.getApplication()).currentPatient.getItemByID(getID()));
        ((GlobalClass) getApplication()).currentPatient.pneumoniaSection.recalculateDiagnosis();

        if(getBaseItem()!=null){
            baseItem.setHostActivity(this);
            baseItem.setupView();
            if (baseItem.getType() == BaseDynamicItem.Type.Question) {
                if (((BaseQuestion) baseItem).isCompleted()) {
                    ((BaseQuestion) baseItem).renderPrevious();
                }
            }
            baseItem.getSection().setCurrentQuestion(baseItem);
        }
        super.onStart();
    }

    /**
     * Reloads question when activity restarts
     * @param outState
     */
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putString("ID",ID);
        super.onSaveInstanceState(outState);
    }

    public BaseDynamicItem getBaseItem() {
        return baseItem;
    }

    public void setBaseItem(BaseDynamicItem baseItem) {
        this.baseItem = baseItem;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    /**
     * Sets up custom toolbar
     * @param menu menu instance
     * @return true
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.dynamic_menu, menu);
        GlobalClass globalClass = (GlobalClass) getApplication();
        DynamicDiagnosis diagnosis = globalClass.currentPatient.pneumoniaSection.diagnosis;

        menu.findItem(R.id.PreviousQuestions).setVisible(true);
        menu.findItem(R.id.CurrentPatients).setVisible(true);
        menu.findItem(R.id.SaveAndRevisit).setVisible(true);
        menu.findItem(R.id.SaveAndRemove).setVisible(true);
        menu.findItem(R.id.Diagnosis).setVisible(false);
        menu.findItem(R.id.DiscardVisit).setVisible(true);

        if (diagnosis != null) {
            menu.findItem(R.id.Diagnosis).setVisible(true);
        }

        if (getBaseItem().getSection() instanceof PatientSection) {
            menu.findItem(R.id.CurrentPatients).setVisible(false);
            menu.findItem(R.id.PreviousQuestions).setVisible(false);
            menu.findItem(R.id.SaveAndRevisit).setVisible(false);
            menu.findItem(R.id.SaveAndRemove).setVisible(false);
            menu.findItem(R.id.Diagnosis).setVisible(false);
        }
        baseItem.setMenu(menu);
        return true;
    }

    /**
     * Performs next action when option selected in dropdown toolbar menu
     * @param item selected item
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        GlobalClass globalClass = (GlobalClass) getApplication();

        Intent homeIntent = new Intent(this, MainActivity.class);

        switch (id) {
            case R.id.CurrentPatients:
                Intent intent = new Intent(this, CurrentPatientsActivity.class);
                startActivity(intent);
                break;
            case R.id.PreviousQuestions:
                intent = new Intent(this, AnsweredQuestionsActivity.class);
                startActivity(intent);
                break;
            case R.id.Diagnosis:
                DynamicDiagnosis diagnosis = globalClass.currentPatient.pneumoniaSection.diagnosis;
                diagnosis.setDismissableLayout(this);
                diagnosis.showPopup(this);
                break;
            case R.id.SaveAndRemove:
                globalClass.currentPatient.saveAndRemove();
                startActivity(homeIntent);
                break;
            case R.id.SaveAndRevisit:
                globalClass.currentPatient.saveToDB();
                startActivity(homeIntent);
                break;
            case R.id.DiscardVisit:
                globalClass.currentPatient.removeFromCurrent();
                startActivity(homeIntent);
                break;
        }

        return super.onOptionsItemSelected(item);
    }
}
