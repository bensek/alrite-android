package thundercats.com.aritediagnosisapp;


import android.app.Activity;
import android.app.ProgressDialog;
import android.widget.Toast;

import com.kolastudios.KSUtils;
import com.pixplicity.easyprefs.library.Prefs;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import thundercats.com.aritediagnosisapp.models.Patient;
import thundercats.com.aritediagnosisapp.utils.APIInterface;
import thundercats.com.aritediagnosisapp.utils.AccountManager;
import thundercats.com.aritediagnosisapp.utils.RetrofitClient;
import thundercats.com.aritediagnosisapp.utils.Utils;

/**
 * Question section for individual patients. This section hosts questions for name, birthday and gender.
 * Section also contains the pneumonia section which has the actual diagnosis questions.
 * Uses this format in case another question section is added for malnutrition. All info stored under one patient.
 */

public class PatientSection extends QuestionSection {
    public PneumoniaSection pneumoniaSection;
    private double weight;
    private String surname, givenname, nickname;
    private int gender, birthday;
    public boolean clicked;
    public Activity hostActivity;
    String genderStr = "";
    String age_range = "";

    /**
     * Constructor for existing patient. Load name, ID, birthday.
     * Appends patient to current patients list and sets current patient to instance of this.
     * Generates pneumonia section.
     *
     * @param id          patient id
     * @param surname        patient surname
     * @param birthday    patient age category
     * @param globalClass reference to global activity
     */
    public PatientSection(String id, String surname, String givenname, String nickname, int birthday, GlobalClass globalClass) {
        super(globalClass.databaseHelper.PATIENT, globalClass);
        setSurname(surname);
        setGivenname(givenname);
        setNickname(nickname);
        setId(id);
        setBirthday(birthday);
        clicked = false;
        makeQuestions();
        globalClass.patientsMap.put(getId(), PatientSection.this);
        globalClass.currentPatient = this;
        pneumoniaSection = new PneumoniaSection(globalClass, this);
    }

    /**
     * new patient constructor
     * @param globalClass reference to global activity
     */
    public PatientSection(GlobalClass globalClass) {
        super(globalClass.databaseHelper.PATIENT, globalClass);

        setId(generateID());
        makeQuestions();
        globalClass.currentPatient = this;
        pneumoniaSection = new PneumoniaSection(globalClass, this);
    }

    /**
     * method to generate questions related to this section.
     * Name question, Birthday question and gender question.
     */
    public void makeQuestions() {
        final BaseQuestion surname = new TextQuestion("patient_surname", "Surname",R.string.patient_surname, R.string.blank, "patient_givenname", this);

        setFirstQuestion(surname);

        final BaseQuestion givenname = new TextQuestion("patient_givenname", "Givenname",R.string.patient_givenname, R.string.blank, "patient_nickname", this);

        final BaseQuestion nickname = new TextQuestion("patient_nickname", "Nickname",R.string.patient_nickname, R.string.blank, "patient_gender", this);


        final BaseQuestion gender = new RadioQuestion("patient_gender", "Gender", R.string.patient_gender, R.string.blank, R.array.patient_gender,
                "patient_age", this);
        final BaseQuestion age = new RadioQuestion("patient_age", "Birthday", R.string.patient_age, R.string.patient_age_subtitle, R.array.age_categories, "patient_weight",  this){
            @Override
            public String nextNavigation() {
                int bday = this.getValue();
                PatientSection.this.setBirthday(bday);

//                if (getBirthday()==4) {
//                    if(clicked) {
//                        return "patient_weight";
//                    }
//                    basicAlertDialog(R.string.age_alert_text, R.string.too_old_text);
//                    return "";
//
//                } else if (getBirthday()==0) {
//                    if(clicked) {
//                        return "patient_weight";
//                    }
//                    basicAlertDialog(R.string.age_alert_text, R.string.too_old_text);
//                    return "";
//
//                } else if (getBirthday()>0 && getBirthday()<4){
//                    return "patient_weight";
//                }
//                return "";
                return "patient_weight";
            }
        };


        BaseQuestion weighti = new NumberInputQuestion("patient_weight", "weight", R.string.patient_weight, R.string.patient_weight_sub, R.string.patient_weight_hint, this){
            @Override
            public String nextNavigation() {
                    saveToDB();
                    PatientSection.this.surname = ((TextQuestion) surname).getValue();
                PatientSection.this.givenname = ((TextQuestion) givenname).getValue();
                PatientSection.this.nickname = ((TextQuestion) nickname).getValue();

                PatientSection.this.gender = ((RadioQuestion) gender).getValue();

                    PatientSection.this.birthday = ((RadioQuestion) age).getValue();


                    PatientSection.this.weight = this.getValue();

                    continueToAssessment(this.getHostActivity());
//                    globalClass.patientsMap.put(getId(), PatientSection.this);
//                    globalClass.currentPatient = PatientSection.this;
//                    pneumoniaSection.startSection(this.getHostActivity());


                KSUtils.log("CREATE PATIENT HERE...");
                KSUtils.log("NEW PATIENT \n Surname -> "+ PatientSection.this.surname + "\n Givenname -> " +  PatientSection.this.givenname + "\n Nickname -> " +  PatientSection.this.nickname
                        + "\n Sex -> " + PatientSection.this.gender+ "\n Age -> " + PatientSection.this.birthday + "\n Weight -> " + PatientSection.this.weight);
                return "";
                }

        };
        /**BaseQuestion age = new DateQuestion("patient_age", "Birthday", R.string.patient_age, R.string.blank,
                -1 * (globalClass.ONE_YEAR * 5), -1 * (globalClass.ONE_MONTH * 2), "", "", "", this) {
            @Override
            public String nextNavigation() {
                if (isBeforeEarlyBound()) {
                    basicAlertDialog(R.string.alert_text, R.string.too_old_text);
                    return "";
                } else if (isAfterLateBound()) {
                    basicAlertDialog(R.string.alert_text, R.string.too_young_text);
                    return "";
                } else {
                    saveToDB();
                    birthday = this.getValue();
                    PatientSection.this.name = ((TextQuestion) name).getValue();
                    PatientSection.this.gender = ((RadioQuestion) gender).getValue();
                    globalClass.patientsMap.put(getId(), PatientSection.this);
                    globalClass.currentPatient = PatientSection.this;
                    pneumoniaSection.startSection(this.getHostActivity());

                    return "";
                }
            }
        }; */
    }

    private void continueToAssessment(Activity hostActivity) {
        if(PatientSection.this.gender == 0){
            genderStr = "Male";
        }else{
            genderStr = "Female";
        }

        if(PatientSection.this.birthday == 0){
            age_range = "Less than 1 month";
        }else if(PatientSection.this.birthday == 1){
            age_range = "1–11 months";
        }else if(PatientSection.this.birthday == 2){
            age_range = "1–5 years";
        }else{
            age_range = "N/A";
        }

        if(Prefs.getString(AccountManager.TOKEN, "").isEmpty()){
            startAssessment(hostActivity);
            return;
        }

        // If User is Logged In Save Patient Data On Server
        createPatient(hostActivity);
    }

    private void startAssessment(Activity hostActivity){
        Patient patient = new Patient();
        patient.surname = PatientSection.this.surname;
        patient.given_name = PatientSection.this.givenname;
        patient.nickname = PatientSection.this.nickname;
        patient.age_range = age_range;
        patient.sex = genderStr;
        patient.weight = (int) PatientSection.this.weight;
        patient.save();

        globalClass.patientsMap.put(getId(), PatientSection.this);
        globalClass.currentPatient = PatientSection.this;
        pneumoniaSection.startSection(hostActivity);

    }

    private void createPatient(Activity hostActivity){

        ProgressDialog progressDialog = new ProgressDialog(hostActivity);
        progressDialog.setMessage("Please wait...");
        progressDialog.setTitle("Saving Patient Data");
        progressDialog.setCancelable(true);

        progressDialog.show();

        Call<Patient> call = RetrofitClient.getInstance().create(APIInterface.class).createPatient(PatientSection.this.surname,PatientSection.this.givenname,PatientSection.this.nickname,
                genderStr, age_range, String.valueOf(PatientSection.this.weight));

        call.enqueue(new Callback<Patient>() {
            @Override
            public void onResponse(Call<Patient> call, Response<Patient> response) {
                if(response.isSuccessful()){
                    Toast.makeText(hostActivity, "Patient saved", Toast.LENGTH_LONG).show();
                    Prefs.putInt(AccountManager.CURRENT_PATIENT, response.body().getId().intValue());

                    startAssessment(hostActivity);
                }else{
                    progressDialog.dismiss();
                    Utils.errorDialog(hostActivity, "An error occurred. Please try again later.");
                    try {
                        KSUtils.logE("Create Patient OnError -> "+ response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

            }

            @Override
            public void onFailure(Call<Patient> call, Throwable t) {
                progressDialog.dismiss();
                if(t instanceof IOException){
                    Utils.errorDialog(hostActivity, "Please check your internet settings to ensure the device has network connection.");

                }else{
                    Utils.errorDialog(hostActivity, "An error occurred. Please try again later.");

                }

                KSUtils.logE("Create Patient OnFailure -> "+ t.getLocalizedMessage());
            }
        });



    }


    @Override
    public BaseQuestion getItemByID(String id) {
        BaseQuestion question = super.getItemByID(id);
        if (question != null) {
            return question;
        }
        return pneumoniaSection.getItemByID(id);
    }

    public int getBirthday() {
        return birthday;
    }

    public void setBirthday(int birthday) {
        this.birthday = birthday;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getGivenname() {
        return givenname;
    }

    public void setGivenname(String givenname) {
        this.givenname = givenname;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String name) {
        this.nickname = name;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double wt) {
        this.weight = wt;
    }

    /**
     * generates list for current patients section to list assessment info
     */
    public ListItem toListItem() {
        Integer cd = globalClass.getAssessedTimeLeft(getId());
        String countDown = cd.toString() + " minutes until reassessment";
        if(cd<0){
            cd = Math.abs(cd);
          countDown = cd.toString() + " minutes past reassessment time";
        }

        return new ListItem(surname + " " + givenname, countDown, getId());
    }

    public void saveAndRemove() {
        pneumoniaSection.saveToDataBase();
        pneumoniaSection.saveTimestoDB();
        removeFromCurrent();
    }

    /**
     * removes patient from current patients list
     */
    public void removeFromCurrent() {
        globalClass.patientsMap.remove(getId());
    }
}
