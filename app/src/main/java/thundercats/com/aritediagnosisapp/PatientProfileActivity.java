package thundercats.com.aritediagnosisapp;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;

/**
 * Activity showing individual patient info including visit history.
 */

public class PatientProfileActivity extends SearchableListViewHelper {
    private TextView nameView, birthdayView, genderView;
    private Toolbar myToolbar;
    private ListView listView;
    private String patientID;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setToolbarTextID(R.string.patient_profile);
    }

    /**
     * General activity setup.
     * Sets up top info section with name, birthday, gender and the create new visit button.
     * Sets up bottom patient history list view with diagnosis type and date of visit.
     */
    @Override
    public void onStart() {
        super.onStart();

        Intent intent = getIntent();
        patientID = intent.getStringExtra("ID");

        setContentView(R.layout.patient_profile_layout);
        setupToolbar();

        listView = findViewById(R.id.ListView);
        nameView = findViewById(R.id.Name);
        birthdayView = findViewById(R.id.Birthday);
        genderView = findViewById(R.id.Gender);

        setList(listView);
        setArrayList(getReportsForPatient());
        updateListView();

        Cursor cursor = dbHelper.getTableItemByID(patientID, dbHelper.PATIENT);
        cursor.moveToFirst();

        final Integer age_category = cursor.getInt(cursor.getColumnIndex("Birthday"));
        final String patientID = cursor.getString(cursor.getColumnIndex("ID"));
        final String surname = cursor.getString(cursor.getColumnIndex("Surname"));
        final String givenname = cursor.getString(cursor.getColumnIndex("Givenname"));
        final String nickname = cursor.getString(cursor.getColumnIndex("Nickname"));

        String gender = cursor.getString(cursor.getColumnIndex("Gender"));
        //String birthday = globalClass.formatLocaleDate(date);

        int maleRes = R.string.male_text, femaleRes = R.string.female_text;
        String genderRes = getResources().getString(gender.equals("0") ? maleRes : femaleRes);

        String age_cat = age_category.toString();
        String bdayRes = getResources().getStringArray(R.array.age_categories)[age_category];

        nameView.setText(surname + " " + givenname + " " + nickname);
        birthdayView.setText(bdayRes);
        genderView.setText(genderRes);

        findViewById(R.id.NewVisit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PatientSection patientSection = new PatientSection(patientID, surname, givenname, nickname, age_category, globalClass);
                patientSection.pneumoniaSection.startSection(PatientProfileActivity.this);
            }
        });

    }

    /**
     * Generates the list representation of previous visits. ListItem with diagnosis type and visit date
     *
     * @return ArrayList of ListItems
     */
    public ArrayList<ListItem> getReportsForPatient() {
        ArrayList<ListItem> items = new ArrayList<>();
        Cursor cursor = dbHelper.getReportsByPatient(patientID);

        try {
            for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                Date date = dbHelper.dateFromSQLString(cursor.getString(cursor.getColumnIndex("Created")));

                String reportID = cursor.getString(cursor.getColumnIndex("ID")),
                        diagnosis = cursor.getString(cursor.getColumnIndex("Diagnosis_Title")),
                        createdDate = getResources().getString(R.string.date_of_evaluation) + ": " + globalClass.formatLocaleDate(date);

                if (diagnosis == null) {
                    diagnosis = getResources().getString(R.string.incomplete);
                }
                if (diagnosis.isEmpty()) {
                    diagnosis = getResources().getString(R.string.incomplete);
                }
                items.add(new ListItem(diagnosis, createdDate, date.getTime() + "@" + reportID));
            }
        } finally {
            cursor.close();
        }
        return items;
    }

    /**
     * Handles back arrow press in toolbar to perform same action as back button press
     * @return
     */
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    /**
     * Sort patient history based on last visit date. Extract date from id string. format: datestring@diagnosisID
     */
    @Override
    public void sortArrayList() {
        Collections.sort(getArrayList(), new Comparator<ListItem>() {
            @Override
            public int compare(ListItem listItem, ListItem t1) {
                String id = listItem.getId();
                String timestamp = id.split("@")[0];
                String id1 = t1.getId();
                String timestamp1 = id1.split("@")[0];
                long d1 = Long.parseLong(timestamp),
                        d2 = Long.parseLong(timestamp1);
                return (int) (d2 - d1);
            }
        });
    }
}
