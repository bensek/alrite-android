package thundercats.com.aritediagnosisapp;

import android.Manifest;
import android.content.ContentValues;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.location.Location;
import androidx.core.app.ActivityCompat;
import android.util.Log;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;
import com.kolastudios.KSUtils;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import thundercats.com.aritediagnosisapp.activities.MainActivity;
import thundercats.com.aritediagnosisapp.utils.Utils;


/**
 * Main question section representing the meat of the application. Hosts all questions related to the pneumonia decision tree.
 * Contains diagnosis object and reference to patient section.
 * Each instance of new patient will generate a new pneumonia section to separate the question responses for multiple patients.
 * All pneumonia sections have their own diagnosis associated.
 */

public class PneumoniaSection extends QuestionSection {
    public DynamicDiagnosis diagnosis;
    private PatientSection patientSection;
    private Location location;
    private static int resp_score=0;
    private static int resp_score_final=0;
    private FusedLocationProviderClient mFusedLocationClient;
    private int age;
    private double weight;


    /**
     * This is where all the questions are generated and all navigation is set.
     * Questions are linked by IDs and the nextNavigation functions handle how questions navigate individually.
     * Once a question branches and all sub-questions lead to a single diagnosis type such as severe pneumonia, the diagnosis object is set at the branch.
     * Diagnosis popup is not always shown when diagnosis is set, and individual items can be appended to the diagnosis depending on the questions. Examples below.
     *
     * @param globalClass    reference to global application
     * @param patientSection reference to patient section
     */

    public PneumoniaSection(final GlobalClass globalClass, final PatientSection patientSection) {
        super(globalClass.databaseHelper.REPORT, globalClass);

        //get location of report to pinpoint clinic locations
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(globalClass);
        if (ActivityCompat.checkSelfPermission(globalClass, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(globalClass, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Log.d("AJ", "not granted");
        } else {
            mFusedLocationClient.getLastLocation().addOnSuccessListener(new OnSuccessListener<Location>() {
                        @Override
                        public void onSuccess(Location loc) { if (loc != null) { location = loc; }}});
        }

        this.patientSection = patientSection;

        //Calculate age for dosing and other purposes
        if (patientSection.getBirthday()>-1 && patientSection.getWeight() > 0.0) {
            this.age = patientSection.getBirthday();
            this.weight = patientSection.getWeight();
        }

        /**
         * Diagnosis Creation Section
         * Creates all final possible diagnosis objects
         */

        final DynamicDiagnosis verySevere = new DynamicDiagnosis(R.string.severe_pneumonia, DynamicDiagnosis.Severity.High, false);
        verySevere.stepsFromIDs(new int[]{R.string.refer_urgently, R.string.first_dose, R.string.first_dose_more, R.string.give_diazepam_if, R.string.keep_child_warm}, globalClass);

        final DynamicDiagnosis noPneumonia = new DynamicDiagnosis(R.string.no_signs, DynamicDiagnosis.Severity.None, false);
        noPneumonia.stepsFromIDs(new int[]{R.string.child_is_healthy, R.string.soothe_throat}, globalClass);

        final DynamicDiagnosis coughOrCold = new DynamicDiagnosis(R.string.cough_or_cold, DynamicDiagnosis.Severity.Mild, true);
        coughOrCold.stepsFromIDs(new int[]{R.string.no_signs, R.string.soothe_throat, R.string.follow_up_5}, globalClass);

        final DynamicDiagnosis pneumonia = new DynamicDiagnosis(R.string.pneumonia, DynamicDiagnosis.Severity.Moderate, true);
        final DynamicDiagnosis pneumonia_wheezing = new DynamicDiagnosis(R.string.pneumonia_wheezing, DynamicDiagnosis.Severity.Moderate, true);
        final DynamicDiagnosis lowOx = new DynamicDiagnosis(R.string.lowOx, DynamicDiagnosis.Severity.High, false);

        if (age<2 || weight < 10.0) {
            verySevere.stepsFromIDs(new int[]{R.string.refer_urgently, R.string.first_dose, R.string.first_dose_IM, R.string.IM_dosing_under1, R.string.give_diazepam_if}, globalClass);
            pneumonia.stepsFromIDs(new int[]{R.string.give_amoxicillin_under1, R.string.follow_up_3}, globalClass);
            pneumonia_wheezing.stepsFromIDs(new int[]{R.string.score_recommends, R.string.give_amoxicillin_under1, R.string.follow_up_3}, globalClass);

        }else if (age<4 || weight < 14.0){
            verySevere.stepsFromIDs(new int[]{R.string.refer_urgently, R.string.first_dose, R.string.first_dose_IM, R.string.IM_dosing_under3, R.string.give_diazepam_if}, globalClass);
            pneumonia.stepsFromIDs(new int[]{R.string.give_amoxicillin_under3, R.string.follow_up_3}, globalClass);
            pneumonia_wheezing.stepsFromIDs(new int[]{R.string.score_recommends, R.string.give_amoxicillin_under3, R.string.follow_up_3}, globalClass);
        } else {
            verySevere.stepsFromIDs(new int[]{R.string.refer_urgently, R.string.first_dose, R.string.first_dose_IM, R.string.IM_dosing_under5, R.string.give_diazepam_if}, globalClass);
            pneumonia.stepsFromIDs(new int[]{R.string.give_amoxicillin_under5, R.string.follow_up_3}, globalClass);
            pneumonia_wheezing.stepsFromIDs(new int[]{R.string.score_recommends, R.string.give_amoxicillin_under5, R.string.follow_up_3}, globalClass);
        }


        /**
        Binary input options (yes/no)

        Constructor()
        We want to save the response to the DB, so DBColumnID is set.
        Question title and subtitle text resources are linked.
        Navigation is added - if user answers yes -> nextYes else -> nextNo

        nextNavigation()
        if the response is 'true' or yes
            set the diagnosis to very severe
            Set the diagnosis layout to save or continue options. User can either save and exit or continue diagnosis.
            Show the diagnosis
            popup will have its own navigation, so last line isn't actually used
        else
            Don't show any diagnosis
            Go to next question
         */


       BaseQuestion generalDanger = new UrgentQuestions("general_danger_checklist", "general_danger_checklist", R.string.general_danger_title, R.string.assess_general_danger_title, "main_symptoms_difficulty", this) {
            @Override
            public String nextNavigation() {
                if (getValue(this.none)) {
                    setShowDiagnosis(false);
                    return getNextID();
                } else {
                    diagnosis = verySevere;
                    diagnosis.setExitOnlyLayout(getHostActivity());
                    setShowDiagnosis(true);
                    return getNextID();
                }
            }
        };

        setFirstQuestion(generalDanger);

       new BinaryQuestion("main_symptoms_difficulty", "main_symptoms_difficulty",
                R.string.main_symptoms_difficulty, R.string.patient_history_section, "main_symptoms_how_long", "",
                this, false) {
            @Override
            public String nextNavigation() {
                if (getValue()) {
                    setShowDiagnosis(false);
                    return getNextYesID();
                } else {
                    diagnosis = noPneumonia;
                    diagnosis.setExitOnlyLayout(getHostActivity());
                    setShowDiagnosis(true);
                    return getNextNoID();
                }
            }
        };

        /**
         The first numberInputQuestion

         Constructor()
         Set ID, Save to DB, Title, Subtitle, hint text, next question (no specific logic), questionSection

         no overrides
         */

        /**
         This question has an associated glossary term so it's added in the constructor.
         question mark icon will automatically show up and link to appropriate glossary popup.
         */

        new NumberInputQuestion("main_symptoms_how_long", "main_symptoms_how_long",
                R.string.main_symptoms_how_long, R.string.patient_history_section, R.string.days_text, "main_symptoms_hiv",
                this);

        new BinaryQuestion("main_symptoms_hiv", "main_symptoms_hiv",
                R.string.main_symptoms_HIV, R.string.patient_history_section, "main_symptoms_past_wheezing", "main_symptoms_past_wheezing",
                this,true);

        new BinaryQuestion("main_symptoms_past_wheezing", "main_symptoms_past_wheezing",  R.string.main_symptoms_past_wheezing, R.string.patient_history_section, "main_symptoms_fever", "main_symptoms_fever",
                this, true) {
            @Override
            public String nextNavigation() {
                if (globalClass.thermometer_present) {
                    return "main_symptoms_temp";
                } else {
                    return "main_symptoms_fever";
                }
            }
        };

        new BinaryQuestion("main_symptoms_fever", "main_symptoms_fever",
                 R.string.main_symptoms_fever, R.string.patient_exam_section,"main_symptoms_stridor", "main_symptoms_stridor",
                this, false) {
            @Override
            public String nextNavigation() {
                if (globalClass.pulseOx_present) {
                    return "main_symptoms_oxygen";
                } else {
                    return "respiratory_rate_baseline";
                }
            }
        };

        new NumberInputQuestion("main_symptoms_temp", "main_symptoms_temp", R.string.main_symptoms_temperature, R.string.patient_exam_section, R.string.degrees_text, "main_symptoms_oxygen", this) {

            @Override
            public String nextNavigation() {
                if (globalClass.pulseOx_present) {
                    return "main_symptoms_oxygen";
                } else {
                    return "respiratory_rate_baseline";
                }
            }
        };

        new NumberInputQuestion("main_symptoms_oxygen", "main_symptoms_oxygen", R.string.main_symptoms_oxygen, R.string.patient_exam_section, R.string.percent, "respiratory_rate_baseline",
                this) {
            @Override
            public String nextNavigation() {
                if (getValue() < 90) {
                    diagnosis = lowOx;
                    if(globalClass.suppOx_present) {
                        diagnosis.stepsFromIDs(new int[]{R.string.administer_supp_ox}, globalClass);
                        diagnosis.setContinueLayout(getHostActivity(), "respiratory_rate_baseline");
                        setShowDiagnosis(true);
                    } else {
                        diagnosis.stepsFromIDs(new int[]{R.string.no_supp_ox}, globalClass);
                        diagnosis.setContinueLayout(getHostActivity(), "bronchodilator");
                        setShowDiagnosis(true);
                    }
                }

                return "respiratory_rate_baseline";
            }
        };

        /**
        Custom respiratory rate question
         */
        new RespiratoryRate("respiratory_rate_baseline", "respiratory_rate_baseline",
                "main_symptoms_stridor", "main_symptoms_stridor", this) {
            @Override
            public String nextNavigation() {
                if (isFastBreathing()) {
                    return getIfFastBreathing();
                } else {
                    return getIfNormalBreathing();
                }
            }
        };


        new BinaryQuestion("main_symptoms_stridor", this,
                "stridor", "main_symptoms_stridor",R.string.main_symptoms_stridor, R.string.patient_exam_section,"", "main_symptoms_indrawing", false) {
            @Override
            public String nextNavigation() {
                if (getValue()) {
                    diagnosis = verySevere;
                    diagnosis.setExitOnlyLayout(getHostActivity());
                    setShowDiagnosis(true);
                    return getNextYesID();
                } else {
                    setShowDiagnosis(false);
                    return getNextNoID();
                }
            }
        };


        new ThreeOptionQuestion("main_symptoms_indrawing", this,
                "chest_indrawing", "main_symptoms_indrawing",
                R.string.main_symptoms_indrawing, R.string.patient_exam_section, new String[]{}) {
            BinaryQuestion hiv = (BinaryQuestion) getItemByID("main_symptoms_hiv");

            @Override
            public String nextNavigation() {
                /* Code leftover from HIV guideline that we decided to leave out for now
                if (hiv.getValue() && getValue()>1) {
                    diagnosis = verySevere;
                    diagnosis.safeReplaceStep(R.string.indrawing_hiv,  R.string.refer_urgently, 0);
                    diagnosis.safeInsertStep(R.string.suspected_asthma, 1);
                    diagnosis.removeSafeStep(R.string.first_dose);
                    diagnosis.safeAddStep(R.string.still_refer_urgently);
                    diagnosis.setContinueLayout(getHostActivity(), "main_symptoms_wheezing");
                    setShowDiagnosis(true);
                }*/
                return "main_symptoms_wheezing";
            }
        };


        /**
        This is where most of the actual logic lies for the diagnosis and branching behavior.
        Various diagnoses are set depending on decision tree protocol and steps are added to diagnoses.
         */
        new ThreeOptionQuestion("main_symptoms_wheezing", this,
                "wheezing", "main_symptoms_wheezing", R.string.main_symptoms_wheezing, R.string.patient_exam_section, "cough_or_cold") {
            @Override
            public String nextNavigation() {
                String bronchodilatorID = "bronchodilator";
                NumberInputQuestion howLong = (NumberInputQuestion) getItemByID("main_symptoms_how_long");
                RespiratoryRate rr = (RespiratoryRate) getItemByID("respiratory_rate_baseline");
                ThreeOptionQuestion indrawing = (ThreeOptionQuestion) getItemByID("main_symptoms_indrawing");
                BinaryQuestion hiv = (BinaryQuestion) getItemByID("main_symptoms_hiv");
                BinaryQuestion past_wheezing = (BinaryQuestion) getItemByID("main_symptoms_past_wheezing");
                BinaryQuestion fever = (BinaryQuestion) getItemByID("main_symptoms_fever");
                NumberInputQuestion temp = (NumberInputQuestion) getItemByID("main_symptoms_temp");

                int score = rr.getScore() + indrawing.getValue() + getValue();
                setResp_score(score);


                String ret = "cough_or_cold";

                if (hiv.getValue() && indrawing.getValue()>1 && score >=4) {
                    diagnosis = verySevere;
                    if(age<1) {
                        diagnosis.stepsFromIDs(new int[]{R.string.score_recommends, R.string.indrawing_hiv, R.string.IM_dosing_under1, R.string.give_diazepam_if, R.string.still_refer_urgently}, globalClass);
                    } else if (age==2) {
                        diagnosis.stepsFromIDs(new int[]{R.string.score_recommends, R.string.indrawing_hiv, R.string.IM_dosing_under3, R.string.give_diazepam_if, R.string.still_refer_urgently}, globalClass);
                    } else {
                        diagnosis.stepsFromIDs(new int[]{R.string.score_recommends, R.string.indrawing_hiv, R.string.IM_dosing_under5, R.string.give_diazepam_if, R.string.still_refer_urgently}, globalClass);
                    }
                    ret = bronchodilatorID;

                } else if (hiv.getValue() && score >= 4) {
                    diagnosis = pneumonia_wheezing;
                    ret = bronchodilatorID;

                } else if (score >= 6) {
                    diagnosis = pneumonia_wheezing;
                    ret = bronchodilatorID;

                } else {
                    diagnosis = coughOrCold;
                    if (score >= 2 || getValue()>0) {
                        diagnosis.safeInsertStep(R.string.score_over_two, 0);
                    }
                }
                if (howLong.getValue() > 14) {
                    diagnosis.safeAddStep(R.string.coughing_more_than_14);
                }
                if (past_wheezing.getValue() && howLong.getValue()<=14) {
                    diagnosis.safeAddStep(R.string.past_breathing);
                }

                diagnosis.score1 = score;
                if (ret.equals(bronchodilatorID)) {
                    diagnosis.setContinueLayout(getHostActivity(), ret);

                } else {
                    diagnosis.setExitOnlyLayout(getHostActivity());
                }
                setShowDiagnosis(true);
                return ret;
            }
        };


        new BinaryQuestion("bronchodilator", this, "bronchodilator",
                R.string.bronchodilator_subtitle, R.string.patient_treatment_section, R.string.administered_text, R.string.not_administered_text, false) {
            @Override
            public String nextNavigation() {
                if (getValue()) {
                    globalClass.setAssessedTime(patientSection.getId(), Calendar.getInstance().getTime().getMinutes());
                    return "first_trial";
                } else {
                    return "not_given";
                }
            }
        };


        new ChecklistQuestion("not_given", "not_given", R.string.reason_for_no_trial_subtitle, R.string.reason_for_no_trial_title, new int[]{R.string.out_of_stock, R.string.refused, R.string.not_indicated, R.string.no_time}, "", this ){
            @Override
            public String nextNavigation() {
                diagnosis.setExitOnlyLayout(getHostActivity());
                setShowDiagnosis(true);
                return getNextID();
            }
        };

        new BinaryQuestion("first_trial", R.string.first_trial, R.string.patient_treatment_section, this, R.string.save_return, R.string.cannot_wait, false) {
            @Override
            public String nextNavigation() {
                if(getNotFirstTrial()){
                    if(getValue()){
                        globalClass.deleteAssessmentTracker(patientSection.getId());
                        return "reevaluation_improved";
                    } else {
                        setCompleted(false);
                        Intent intent = new Intent(getHostActivity(), MainActivity.class);
                        getHostActivity().startActivity(intent);
                        return "";
                    }

                } else {
                    if (getValue()) {
                        saveToDataBase();
                        setCompleted(false);
                        Intent intent = new Intent(getHostActivity(), MainActivity.class);
                        getHostActivity().startActivity(intent);
                    } else {
                        globalClass.currentPatient.saveAndRemove();
                        globalClass.deleteAssessmentTracker(patientSection.getId());
                        Intent intent = new Intent(getHostActivity(), MainActivity.class);
                        getHostActivity().startActivity(intent);
                    }
                }
                return "";
            }
        };



        new BinaryQuestion("reevaluation_improved", "reevaluation_improved",
                R.string.improvement_breathing, R.string.patient_exam_section, "reevaluation_oxygen", "try_again", this, false) {
            @Override
            public String nextNavigation() {
                if (getValue()) {
                    if(globalClass.pulseOx_present) {
                        return "reevaluation_oxygen";
                    } else {
                        return "reevaluation_respiratory_rate";
                    }
                    /*if (globalClass.pulseOx_present) {
                        return "main_symptoms_oxygen";
                    } else {
                        return "respiratory_rate_baseline";
                    }*/
                } else {
                    return getNextNoID();
                }
            }
        };

        new NumberInputQuestion("reevaluation_oxygen", "reevaluation_oxygen", R.string.main_symptoms_oxygen, R.string.patient_exam_section, R.string.percent, "reevaluation_respiratory_rate",
                this){
                @Override
                public String nextNavigation() {
                    if (getValue() < 90) {
                        diagnosis = lowOx;
                        if(globalClass.suppOx_present) {
                            diagnosis.stepsFromIDs(new int[]{R.string.continue_supp_ox}, globalClass);
                            diagnosis.setContinueLayout(getHostActivity(), "reevaluation_respiratory_rate");
                            setShowDiagnosis(true);

                        } else {
                            diagnosis.stepsFromIDs(new int[]{R.string.still_no_supp_ox}, globalClass);
                            diagnosis.setContinueLayout(getHostActivity(), "bronchodilator");
                            setShowDiagnosis(true);
                        }
                    }

                    return "reevaluation_respiratory_rate";
                }
        };

        new RespiratoryRate("reevaluation_respiratory_rate", "reevaluation_respiratory_rate",
                "reevaluation_indrawing", "reevaluation_indrawing",
                this) {
            @Override
            public String nextNavigation() {
                resp_score_final = this.getScore();
                return "reevaluation_indrawing";
            }
        };



        new BinaryQuestion("try_again",
                R.string.try_again, R.string.try_again_prompt, this,
                R.string.no_save_and_exit_text, R.string.yes_try_again_text, false) {
            @Override
            public String nextNavigation() {
                globalClass.deleteAssessmentTracker(patientSection.getId());
                if (getValue()) {
                    diagnosis.score2 = resp_score_final;
                    diagnosis.safeInsertStep(R.string.broncho_ineffective, 0);
                    diagnosis.removeDuplicates(R.string.broncho_ineffective, 1);
                    diagnosis.setShowFinal(true);
                    diagnosis.setExitOnlyLayout(getHostActivity());
                    setShowDiagnosis(true);
                    return "";
                } else {
                    NumberInputQuestion oxNew = (NumberInputQuestion) getItemByID("reevaluation_oxygen");
                    RespiratoryRate rrNew = (RespiratoryRate) getItemByID("reevaluation_respiratory_rate");
                    BinaryQuestion broncho = (BinaryQuestion) getItemByID("bronchodilator");
                    BinaryQuestion reassess = (BinaryQuestion) getItemByID("reassess");

                    oxNew.reset();
                    rrNew.reset();
                    broncho.reset();
                    reassess.reset();
                    setShowDiagnosis(false);
                    return "bronchodilator";
                }
            }
        };

        new ThreeOptionQuestion("reevaluation_indrawing", this,
                "chest_indrawing", "reevaluation_indrawing", R.string.indrawing_improved, R.string.patient_exam_section, "reevaluation_wheezing") {
            @Override
            public String nextNavigation() {
                resp_score_final += getValue();
                return "reevaluation_wheezing";
            }
        };

        new ThreeOptionQuestion("reevaluation_wheezing", this,
                "wheezing", "reevaluation_wheezing", R.string.wheezing_improved, R.string.patient_exam_section, "") {
            @Override
            public String nextNavigation() {
                globalClass.deleteAssessmentTracker(patientSection.getSurname() + " " + patientSection.getGivenname()+ " " + patientSection.getNickname());
                resp_score_final += getValue();
                if (resp_score_final<resp_score) {
                    if(diagnosis == verySevere) {
                        diagnosis.removeSafeStep(R.string.suspected_asthma);
                        diagnosis.safeInsertStep(R.string.severe_give_bronchodilator_5,0);
                        diagnosis.removeDuplicates(R.string.severe_give_bronchodilator_5, 1);
                    }else {
                        diagnosis.safeInsertStep(R.string.give_bronchodilator_5,0);
                        diagnosis.safeInsertStep(R.string.show_guardian, 1);
                    }

                } else {
                    diagnosis.safeInsertStep(R.string.broncho_ineffective, 0);
                }

                diagnosis.removeSafeStep(R.string.score_recommends);
                diagnosis.removeSafeStep(R.string.score_recommends);

                diagnosis.score2 = resp_score_final;
                diagnosis.setShowFinal(true);
                diagnosis.setExitOnlyLayout(getHostActivity());
                setShowDiagnosis(true);
                return "";
            }
        };
    }

    /**
     * Convert question responses into content values object for database insertion
     * @return values
     */

    @Override
    public ContentValues makeContentValues() {
        ContentValues values = super.makeContentValues();
        values.put("Patient_ID", patientSection.getId());
        if (diagnosis != null) {
            String title = globalClass.getResources().getString(diagnosis.getTitleRes());
            values.put("Diagnosis_Title", title);
            values.put("respiratory_score", String.valueOf(resp_score));
            values.put("respiratory_score_final", String.valueOf(resp_score_final));
//            KSUtils.log("DIAGNOSIS -> "+ Utils.toJSONString(diagnosis));
            KSUtils.log("\n\nDIAGNOSIS -> \n Title -> " + title +  "\n Respiratory Scrore -> " + resp_score + "\n Resp final score -> "+ resp_score_final);
        }
        if (location != null) {
            values.put("Latitude", location.getLatitude());
            values.put("Longitude", location.getLongitude());
        }

        return values;
    }

    /**
     * recalculates diagnosis based on answered questions
     */
    public void recalculateDiagnosis() {
        diagnosis = null;
        getAnsweredQuestions();
    }

    public void setResp_score(int sc) {
        resp_score = sc;
    }
    public int getResp_score() {return resp_score; }

    public void saveToDataBase(){
        if(patientSection.getId()!=null) {
            String id = patientSection.getId();
            Cursor c = globalClass.databaseHelper.getTableItemByID(id, globalClass.databaseHelper.REPORT);
            if(c.getCount()>0){
                globalClass.databaseHelper.updateTableItem(makeContentValues(), id, globalClass.databaseHelper.REPORT);
                return;
            }
            globalClass.databaseHelper.createTableItem(makeContentValues(), globalClass.databaseHelper.REPORT);

        }

    }
    //captures time spent on each screen (for analytics)
    public void saveTimestoDB() {
        String id = "screentimes";
        ContentValues values = new ContentValues();
        values.put("ID", id);
        values.put("Modified", globalClass.databaseHelper.formatDateForSQL(new Date()));
        values.put("Deleted", this.getDeleted());
        values.put("total_assessments", (long)1);
        Cursor c = globalClass.databaseHelper.getTableItemByID(id, globalClass.databaseHelper.SCREENTIME);
        boolean count = c.getCount() > 0;
        HashMap<String, BaseQuestion>itemss = this.getItems();
        for(BaseDynamicItem item:itemss.values()) {
            if (item.getType().equals(BaseDynamicItem.Type.Question)) {
                BaseQuestion baseQuestion = (BaseQuestion) item;
                if (baseQuestion.isCompleted() && baseQuestion.isDBQuestion()) {
                    if (count) {
                        try {
                            long prev = c.getLong(c.getColumnIndex(baseQuestion.getDBColumnID()));
                            values.put(baseQuestion.getDBColumnID(), (baseQuestion.getScreenTimeStamp() + prev));
                        } catch(NullPointerException e) {
                            System.err.println("null db value caught");
                        }
                    } else {
                        values.put(baseQuestion.getDBColumnID(), baseQuestion.getScreenTimeStamp());
                    }
                }
            }
        }
        int index = c.getColumnIndex("total_assessments");
        if(count && !c.isNull(index)){
            long total_assessments = c.getLong(c.getColumnIndex("total_assessments"));
            total_assessments++;
            values.remove("total_assessments");
            values.put("total_assessments", total_assessments);
            globalClass.databaseHelper.updateTableItem(values, id, globalClass.databaseHelper.SCREENTIME);
            return;
        }
        globalClass.databaseHelper.createTableItem(values, globalClass.databaseHelper.SCREENTIME);
    }

}
