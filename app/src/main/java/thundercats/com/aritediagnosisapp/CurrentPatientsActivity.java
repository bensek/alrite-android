package thundercats.com.aritediagnosisapp;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.View;
import android.widget.AdapterView;

import java.util.ArrayList;

/**
 * Activity to show list of currently active patients
 */

public class CurrentPatientsActivity extends SearchableListViewHelper {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setToolbarTextID(R.string.current_patients);
    }

    @Override
    public void onStart() {
        super.onStart();
        ArrayList<ListItem> tmp = makeListItems();
        setArrayList(tmp);
        listener();
        updateListView();
        updateListListener();
    }

    /**
     * Sets click listener for each patient - will switch between patients and open most recently answered question.
     */
    public void listener() {
        setItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> adapter, View v, int position,
                                    long arg3) {
                ListItem item = (ListItem) adapter.getItemAtPosition(position);
                PatientSection patientSection =((GlobalClass) getApplication()).patientsMap.get(item.getId());

                if (patientSection != null) {
                    ((GlobalClass) getApplication()).currentPatient = patientSection;
                    BaseQuestion currentQuestion = (BaseQuestion) patientSection.pneumoniaSection.getCurrentQuestion();
                    Intent intent = new Intent(CurrentPatientsActivity.this, DynamicQuestionActivity.class);
                    intent.putExtra("ID", currentQuestion.getID());
                    startActivity(intent);
                }
            }

        });

    }

    /**
     * Generates list of currently active patients
     *
     * @return arraylist of listItems representing patients
     */
    public ArrayList<ListItem> makeListItems() {
        ArrayList<ListItem> items = new ArrayList<>();
        for (PatientSection bq : globalClass.patientsMap.values()) {
            items.add(bq.toListItem());
        }
        return items;
    }
}
