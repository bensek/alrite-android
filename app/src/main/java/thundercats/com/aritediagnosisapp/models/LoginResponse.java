package thundercats.com.aritediagnosisapp.models;

public class LoginResponse {
    public int code;
    public String message;
    public User data;
}
