package thundercats.com.aritediagnosisapp.models;

import com.orm.SugarRecord;

import java.io.Serializable;
import java.util.List;

public class Patient extends SugarRecord implements Serializable {
    //public int id;
    public String surname;
    public String given_name;
    public String nickname;
    public String sex;
    public String age_range;
    public int weight;
    public String number;
    public int user_id;
    public int center_id;
    public String created_at;
    public List<Assessment> assessments;


    public String getName() {
        if(nickname == null){
            return surname + " " + given_name;
        }
        return surname + " " + given_name + " " + nickname;
    }

    public String getInitials() {
        return surname.charAt(0) + "" + given_name.charAt(0);
    }
}
