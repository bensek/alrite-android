package thundercats.com.aritediagnosisapp.models;

import com.orm.SugarRecord;

public class User extends SugarRecord {

    public String first_name;
    public String last_name;
    public String other_name;
    public String number;
    public String phone;
    public String email;
    public String token;
    public String sex;
    public String mobile;
    public String address;
    public String marital_status;
    public String dob;
    public String created_at;
    public Center center;

    public String center_name;
    public String center_address;
    public String center_number;

    public String getName(){
        if (other_name == null) {
            return first_name + " " + last_name;

        }
        return first_name + " " + last_name + " " +other_name;
    }

    public String getInitials(){
        return last_name.charAt(0) + "" + first_name.charAt(0);
    }
}
