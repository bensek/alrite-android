package thundercats.com.aritediagnosisapp.models;

public class Center {
    public String name;
    public String address;
    public String number;
}
