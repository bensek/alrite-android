package thundercats.com.aritediagnosisapp.models;

import com.orm.SugarRecord;

import java.io.Serializable;

public class Assessment extends SugarRecord implements Serializable {
    public String title;
    public String created_at;
    public int patient_id;
    public String local_date;
}
