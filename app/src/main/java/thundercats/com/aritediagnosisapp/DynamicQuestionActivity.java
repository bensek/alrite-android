package thundercats.com.aritediagnosisapp;

import android.content.DialogInterface;
import androidx.appcompat.app.AlertDialog;

/**
 * Activity to host and render dynamic questions
 */

public class DynamicQuestionActivity extends DynamicBaseActivity {

    /**
     * set current question to instance of this question and render question
     */
    @Override
    protected void onStart() {
        super.onStart();
        if (getBaseItem().getSection() instanceof PneumoniaSection) {
            PatientSection currentPatient = ((GlobalClass) getApplication()).currentPatient;
            baseItem.setToolbarText(currentPatient.getSurname() + " " + currentPatient.getGivenname() + " " + currentPatient.getNickname());
            ((GlobalClass) getApplication()).currentPatient.pneumoniaSection.setCurrentQuestion(getBaseItem());
        }
    }

    /**
     * currently does nothing, but captures back press to warn user of exiting question section.
     */
    @Override
    public void onBackPressed() {
        if (getBaseItem().getSection() instanceof PneumoniaSection) {
            if (getBaseItem().getSection().getFirstQuestion().getID().equals(getID())) { //currently disabled
                //firstQuestionAlert();
                super.onBackPressed();
            } else {
                super.onBackPressed();
            }
        } else {
            super.onBackPressed();
        }
    }

    /**
     * Currently disabled, but warns users when they press back on the first question in a section sp they don't lose any data accidentally.
     */
    public void firstQuestionAlert(){
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle(R.string.alert_text)
                .setMessage(R.string.survey_exit_alert_text)
                .setNegativeButton(R.string.cancel_text, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialoginterface, int i) {
                        dialoginterface.cancel();
                    }})
                .setPositiveButton(R.string.continue_text, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialoginterface, int i) {
                        GlobalClass globalClass = (GlobalClass) getApplication();
                        PatientSection patientSection = globalClass.currentPatient;
                        patientSection.pneumoniaSection = new PneumoniaSection(globalClass, patientSection);
                        patientSection.removeFromCurrent();
                        DynamicQuestionActivity.super.onBackPressed();
                    }
                }).show();
    }

}
