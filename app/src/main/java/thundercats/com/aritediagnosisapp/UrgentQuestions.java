package thundercats.com.aritediagnosisapp;

import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;

/**
 * Class for Checklist question format
 */

public class UrgentQuestions extends BaseQuestion {
    protected CheckBox breastfeeding, vomiting, unresponsive, convulsions,  none;
    protected Button submitButton;
    private int breasffeedingButtonTextRes = R.string.danger_breastfeeding, vomitingButtonTextRes = R.string.danger_vomit, unresponsiveButtonTextRes = R.string.danger_unresponsive,
            convulsionsButtonTextRes = R.string.danger_convulsions, noneoftheseButtonTextRes = R.string.none_symptoms_text;
    private boolean breastfeeding_value = false, unrespo_value = false, vomiting_value = false, convulsions_value = false, none_value = false;
    private String nextID;
    private View.OnClickListener vomitingButtonListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            setValue(view, !vomiting_value);

        }
    };
    private View.OnClickListener breastfeedingButtonListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            setValue(view,!breastfeeding_value);

        }
    };
    private View.OnClickListener unresponsiveButtonListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            setValue(view, !unrespo_value);

        }
    };
    private View.OnClickListener convulsionsButtonListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            setValue(view, !convulsions_value);

        }
    };
    private View.OnClickListener noneButtonListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            setValue(view, !none_value);

        }
    };
    private View.OnClickListener submitButtonListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            afterClick();
        }
    };




    public UrgentQuestions(String ID, String DBColumnID, int titleTextResID, int subtitleTextResID, String nextQuestion, QuestionSection section) {
        super(ID, DBColumnID, titleTextResID, subtitleTextResID,section);
        this.nextID = nextQuestion;

    }

    /**
     * Add yes and no buttons and link views to vars then set listeners
     */
    @Override
    public void setupView() {
        super.setupView();
        fillNavigationContent(R.layout.submit_layout);
        fillCardContent(R.layout.urgent_questions_layout_full);
        linkButtonViews();
        setListeners();
    }

    /**
     * Set default navigation behavior: if yes then return yesID, else return noID
     * @return next dynamic item ID
     */
    @Override
    public String nextNavigation() {
        return getNextID();
    }

    public String getNextID() {
        return nextID;
    }

    public void setNextID(String nextID) {
        this.nextID = nextID;
    }




    @Override
    public String getDBValue() {
        return getCheckListDBValue(none);
    }

    /**
     * Converts binary value to string for database entry for each checkbox
     *
     * @return value to string
     */

    public String getCheckListDBValue(View view) {
        return none_value?"1":"0";
    }

    public boolean getValue(View view) {
        if( view == this.breastfeeding ) {
            return breastfeeding_value;
        }
        if( view == this.vomiting ) {
            return vomiting_value;
        }
        if( view == this.unresponsive ) {
            return unrespo_value;
        }
        if( view == this.convulsions ) {
            return convulsions_value;
        }
        if( view == this.none ) {
            return none_value;
        }
        else {
            return false;
        }
    }


    protected void setValue(View view,  boolean value) {
        if( view == this.breastfeeding ) {
            breastfeeding_value = value;
        }
        if( view == this.vomiting ) {
            vomiting_value = value;
        }
        if( view == this.unresponsive ) {
            unrespo_value = value;
        }
        if( view == this.convulsions ) {
            convulsions_value = value;
        }
        if( view == this.none ) {
            none_value = value;
        }
    }

    /**
     * Finds buttons in layout and sets button text accordingly
     */
    public void linkButtonViews(){
        breastfeeding = getHostActivity().findViewById(R.id.breastfeeding_cb);
        breastfeeding.setText(breasffeedingButtonTextRes);

        convulsions = getHostActivity().findViewById(R.id.convulsions_cb);
        convulsions.setText(convulsionsButtonTextRes);

        unresponsive = getHostActivity().findViewById(R.id.unresponsive_cb);
        unresponsive.setText(unresponsiveButtonTextRes);

        vomiting = getHostActivity().findViewById(R.id.vomiting_cb);
        vomiting.setText(vomitingButtonTextRes);

        none = getHostActivity().findViewById(R.id.none_of_these_cb);
        none.setText(noneoftheseButtonTextRes);

        submitButton = getHostActivity().findViewById(R.id.SubmitButton);

    }

    public void setListeners() {
        vomiting.setOnClickListener(vomitingButtonListener);
        breastfeeding.setOnClickListener(breastfeedingButtonListener);
        convulsions.setOnClickListener(convulsionsButtonListener);
        unresponsive.setOnClickListener(unresponsiveButtonListener);
        none.setOnClickListener(noneButtonListener);
        submitButton.setOnClickListener(submitButtonListener);
    }

    /**
     * Keeps boxes checked if returning to question
     */
    @Override
    public void renderPrevious() {
        unresponsive.setChecked(unrespo_value);
        vomiting.setChecked(vomiting_value);
        convulsions.setChecked(convulsions_value);
        breastfeeding.setChecked(breastfeeding_value);
        none.setChecked(none_value);
    }
}
