package thundercats.com.aritediagnosisapp.fragments;

import android.content.Intent;
import android.content.res.AssetManager;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.aquery.AQuery;
import com.kolastudios.KSUtils;
import com.pixplicity.easyprefs.library.Prefs;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import thundercats.com.aritediagnosisapp.GlobalClass;
import thundercats.com.aritediagnosisapp.PatientProfileActivity;
import thundercats.com.aritediagnosisapp.PatientProfileActivity2;
import thundercats.com.aritediagnosisapp.PatientSection;
import thundercats.com.aritediagnosisapp.R;
import thundercats.com.aritediagnosisapp.activities.MainActivity;
import thundercats.com.aritediagnosisapp.adapter.PatientsAdapter;
import thundercats.com.aritediagnosisapp.models.Assessment;
import thundercats.com.aritediagnosisapp.models.Patient;
import thundercats.com.aritediagnosisapp.utils.APIInterface;
import thundercats.com.aritediagnosisapp.utils.AccountManager;
import thundercats.com.aritediagnosisapp.utils.RetrofitClient;
import thundercats.com.aritediagnosisapp.utils.Utils;

public class PatientsFragment extends Fragment {
    private RecyclerView recyclerView;
    private AQuery aq;
    private PatientsAdapter adapter;
    private GlobalClass globalClass;
    private EditText searchView;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
       View root = inflater.inflate(R.layout.fragment_patients, container, false);
       aq = new AQuery(root);

        globalClass = (GlobalClass) getActivity().getApplication();

        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        MainActivity.setNavHeader();
        if(Prefs.getString(AccountManager.TOKEN, "").equals("")){
            loadPatients();
        }else{
            getPatients(false);
        }
    }

    private void getPatients(boolean hideList){
        aq.id(R.id.no_patients_found).hide();
        aq.id(R.id.patients_list).hide();
        aq.id(R.id.progress_layout).show();

        loadPatients();

        Call<List<Patient>> call = RetrofitClient.getInstance().create(APIInterface.class).getMyPatients();
        call.enqueue(new Callback<List<Patient>>() {
            @Override
            public void onResponse(Call<List<Patient>> call, Response<List<Patient>> response) {
                if(response.isSuccessful()){
                    Patient.deleteAll(Patient.class);
                    Assessment.deleteAll(Assessment.class);
                    for(Patient patient : response.body()){
                        patient.save();
                        if(patient.assessments.size() > 0) {
                            for (Assessment a : patient.assessments) {
                                a.save();
                            }
                        }
                    }
                    loadPatients();
                }else{
                    aq.id(R.id.progress_layout).hide();
                    Utils.errorDialog(getActivity(), "An error occurred. Please try again later.");
                    try {
                        KSUtils.logE("Get Patients OnError -> "+ response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<List<Patient>> call, Throwable t) {
                aq.id(R.id.progress_layout).hide();
//                if(t instanceof IOException){
//                    Utils.errorDialog(getActivity(), "Please check your internet settings to ensure the device has network connection.");
//
//                }else{
//                    Utils.errorDialog(getActivity(), "An error occurred. Please try again later.");
//
//                }

                KSUtils.logE("Get Patients OnFailure -> "+ t.getLocalizedMessage());
            }
        });
    }

    private void loadPatients() {
        List<Patient> list = Patient.listAll(Patient.class);
        if(list.size() == 0){
            aq.id(R.id.no_patients_found).show();
            aq.id(R.id.progress_layout).hide();
            aq.id(R.id.patients_list).hide();
            return;
        }

        aq.id(R.id.no_patients_found).hide();
        aq.id(R.id.progress_layout).hide();
        aq.id(R.id.patients_list).show();

        adapter = new PatientsAdapter(getActivity(), list, patient ->{
            Intent intent = new Intent(getActivity(), PatientProfileActivity2.class);
            intent.putExtra("PatientId", patient.getId());
            intent.putExtra("Patient" , patient);
            startActivity(intent);
        });
        recyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }
}