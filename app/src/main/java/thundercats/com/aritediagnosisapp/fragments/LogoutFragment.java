package thundercats.com.aritediagnosisapp.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import com.aquery.AQuery;
import com.pixplicity.easyprefs.library.Prefs;

import thundercats.com.aritediagnosisapp.R;
import thundercats.com.aritediagnosisapp.activities.MainActivity;
import thundercats.com.aritediagnosisapp.models.Assessment;
import thundercats.com.aritediagnosisapp.models.Patient;
import thundercats.com.aritediagnosisapp.models.User;

public class LogoutFragment extends Fragment {

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
       View root = inflater.inflate(R.layout.fragment_logout, container, false);

        (new AQuery(root).id(R.id.btn_logout)).click(k->{
            // Logout from here
            Prefs.clear();
            User.deleteAll(User.class);
            Patient.deleteAll(Patient.class);
            Assessment.deleteAll(Assessment.class);
            Navigation.findNavController(root).navigate(R.id.nav_home);
        });
        return root;
    }
}