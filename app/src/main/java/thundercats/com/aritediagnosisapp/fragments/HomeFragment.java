package thundercats.com.aritediagnosisapp.fragments;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;

import com.aquery.AQuery;
import com.kolastudios.KSUtils;
import com.pixplicity.easyprefs.library.Prefs;

import java.util.Calendar;
import java.util.Date;
import thundercats.com.aritediagnosisapp.ClinicResourcesRecorderActivity;
import thundercats.com.aritediagnosisapp.CurrentPatientsActivity;
import thundercats.com.aritediagnosisapp.GlobalClass;
import thundercats.com.aritediagnosisapp.GlossaryActivity;
import thundercats.com.aritediagnosisapp.GlossaryItem;
import thundercats.com.aritediagnosisapp.R;
import thundercats.com.aritediagnosisapp.SelectPatientActivity;
import thundercats.com.aritediagnosisapp.activities.MainActivity;
import thundercats.com.aritediagnosisapp.activities.NewPatientActivity;
import thundercats.com.aritediagnosisapp.activities.PatientsListActivity;
import thundercats.com.aritediagnosisapp.utils.AccountManager;
import thundercats.com.aritediagnosisapp.utils.Utils;

import static thundercats.com.aritediagnosisapp.activities.MainActivity.loggedInMenu;
import static thundercats.com.aritediagnosisapp.activities.MainActivity.loggedOutMenu;

public class HomeFragment extends Fragment {
    private Button assessButton;
    private CardView assessmentView, patientsView, settingView, learnView;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
       View root = inflater.inflate(R.layout.fragment_home, container, false);

       assessButton = root.findViewById(R.id.btn_start_assessment);
       assessmentView =root.findViewById(R.id.btn_assessment);
       patientsView = root.findViewById(R.id.btn_patients);
       settingView =root.findViewById(R.id.btn_clinic_setting);
       learnView = root.findViewById(R.id.btn_learn);

        KSUtils.log("Network Available -> "+ Utils.isNetworkConnected(getActivity()));

        assessButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(getActivity(), SelectPatientActivity.class);
                startActivity(intent);
            }
        });

        assessmentView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(getActivity(), SelectPatientActivity.class);
                startActivity(intent);
            }
        });
        patientsView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), CurrentPatientsActivity.class);
                startActivity(intent);

            }
        });

       settingView.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {

               Intent intent = new Intent(getActivity(), ClinicResourcesRecorderActivity.class);
               startActivity(intent);
           }
       });
       learnView.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               Intent intent = new Intent(getActivity(), GlossaryActivity.class);
               startActivity(intent);

           }
       });

//        Toolbar myToolbar = findViewById(R.id.my_toolbar);
//        setSupportActionBar(myToolbar);

//        diagnose = root.findViewById(R.id.HomeDiagnoseButton);
//        glossary =  root.findViewById(R.id.HomeGlossaryButton);
//        patients =  root.findViewById(R.id.HomePatientsButton);
//        resources =  root.findViewById(R.id.homeResourcesButton);
//
//        //Keeps track of when it's time to update what resources the user's clinic has (once every month)
//        //When resources have not been set or updated, the home screen button will be yellow
//        if(globalClass.databaseHelper.getTableItemByID("clinic",globalClass. databaseHelper.CLINIC).getCount() <1) {
//            resources.setTextColor(getResources().getColor(R.color.buttonHighlight));
//        } else {
//            resources.setTextColor(getResources().getColor(R.color.white));
//            String dateModified = globalClass.databaseHelper.getTableItemByID("clinic", globalClass.databaseHelper.CLINIC).getString(12);
//
//            if(dateModified == null) {
//                String dateCreated = globalClass.databaseHelper.getTableItemByID("clinic", globalClass.databaseHelper.CLINIC).getString(13);
//                Date created = globalClass.databaseHelper.dateFromSQLString(dateCreated);
//                if(Math.abs(Calendar.getInstance().getTime().getMonth() - created.getMonth()) == 1) {
//                    resources.setTextColor(getResources().getColor(R.color.buttonHighlight));
//                }
//            } else {
//                Date modified = globalClass.databaseHelper.dateFromSQLString(dateModified);
//
//                if(Math.abs(Calendar.getInstance().getTime().getMonth() - modified.getMonth()) == 1) {
//                    resources.setTextColor(getResources().getColor(R.color.buttonHighlight));
//                }
//            }
//        }
//
//
//        diagnose.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent intent = new Intent(getActivity(), SelectPatientActivity.class);
//                startActivity(intent);
//            }
//        });
//
//        glossary.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent intent = new Intent(getActivity(), GlossaryActivity.class);
//                startActivity(intent);
//            }
//        });
//
//
//        patients.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent intent = new Intent(getActivity(), CurrentPatientsActivity.class);
//                startActivity(intent);
//            }
//        });
//
//        resources.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent intent = new Intent(getActivity(), ClinicResourcesRecorderActivity.class);
//                startActivity(intent);
//            }
//        });
//
//        /**
//         * Disclaimer dialog for using ALRITE
//         **/
//        if(!globalClass.disclaimer){
//            AlertDialog.Builder builder1 = new AlertDialog.Builder(getActivity());
//            builder1.setTitle("Disclaimer");
//            builder1.setMessage("Note that the ALRITE Application is intended for academic uses only and should not be used alone to provide medical guidance");
//            builder1.setCancelable(true);
//            builder1.setPositiveButton(R.string.continue_text,
//                    new DialogInterface.OnClickListener() {
//                        public void onClick(DialogInterface dialog, int id) {
//                            dialog.cancel();
//                        }
//                    });
//            AlertDialog alert11 = builder1.create();
//            globalClass.disclaimer = true;
//            alert11.show();
//        }
//
//        //Code for managing timers for patients waiting 10 minutes after Bronchodilator delivery
//
//        if(globalClass.checkTimes()) {
//            if(!globalClass.waitingPatients.isEmpty()){
//                GlossaryItem alert = new GlossaryItem(new String[]{"Reassessment Alert", ""}, "readyAssessment", globalClass);
//                alert.showPopup(getActivity(), true);
//                patients.setTextColor(getResources().getColor(R.color.severeDiagnosisColor));
//            } else if(!globalClass.soonPatients.isEmpty()) {
//                GlossaryItem alert = new GlossaryItem(new String[]{"Reassessment Alert", ""}, "soonAssessment", globalClass);
//                alert.showPopup(getActivity(), true);
//                patients.setTextColor(getResources().getColor(R.color.buttonHighlight));
//            }
//        } else {
//            patients.setTextColor(getResources().getColor(R.color.white));
//        }
//
//
//
//
//
        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        MainActivity.setNavHeader();
//        if(Prefs.getString(AccountManager.TOKEN, "").equals("")){
//            loggedOutMenu();
//        }else{
//            loggedInMenu();
//        }
    }
}