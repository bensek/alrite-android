package thundercats.com.aritediagnosisapp.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.amulyakhare.textdrawable.TextDrawable;
import com.aquery.AQuery;
import com.pixplicity.easyprefs.library.Prefs;

import thundercats.com.aritediagnosisapp.R;
import thundercats.com.aritediagnosisapp.utils.AccountManager;

public class AccountFragment extends Fragment {
    private AQuery aq;
    private ImageView imageView;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
       View root = inflater.inflate(R.layout.fragment_account, container, false);
        aq = new AQuery(root);

        imageView = root.findViewById(R.id.imageView);
        TextDrawable drawable = TextDrawable.builder()
                .buildRound(Prefs.getString(AccountManager.INITIALS, ""), getActivity().getResources().getColor(R.color.primaryButtonColor));

        imageView.setImageDrawable(drawable);
        aq.id(R.id.name).text(Prefs.getString(AccountManager.NAME, ""));
        aq.id(R.id.emailAddress).text(Prefs.getString(AccountManager.EMAIL, ""));
        aq.id(R.id.phoneNumber).text(Prefs.getString(AccountManager.PHONE, ""));
        aq.id(R.id.mobileNumber).text(Prefs.getString(AccountManager.MOBILE, ""));
        aq.id(R.id.gender).text(Prefs.getString(AccountManager.SEX, ""));
        aq.id(R.id.number).text(Prefs.getString(AccountManager.NUMBER, ""));
        aq.id(R.id.address).text(Prefs.getString(AccountManager.ADDRESS, ""));
        aq.id(R.id.healthCenter).text(Prefs.getString(AccountManager.CENTER_NAME, ""));
        aq.id(R.id.hcAddress).text(Prefs.getString(AccountManager.CENTER_ADDRESS, ""));

        return root;
    }
}