package thundercats.com.aritediagnosisapp.fragments;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;
import com.aquery.AQuery;
import com.kolastudios.KSUtils;
import com.pixplicity.easyprefs.library.Prefs;
import java.io.IOException;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import thundercats.com.aritediagnosisapp.R;
import thundercats.com.aritediagnosisapp.models.LoginResponse;
import thundercats.com.aritediagnosisapp.models.User;
import thundercats.com.aritediagnosisapp.utils.APIInterface;
import thundercats.com.aritediagnosisapp.utils.AccountManager;
import thundercats.com.aritediagnosisapp.utils.RetrofitClient;
import thundercats.com.aritediagnosisapp.utils.Utils;

public class LoginFragment extends Fragment {
    private AQuery aq;
    private ProgressDialog progressDialog;
    private View view;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_login, container, false);
        aq = new AQuery(view);
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setCancelable(true);
        progressDialog.setTitle("Logging In");
        progressDialog.setMessage("Please wait...");

        aq.id(R.id.btn_login).click(l->{
            login();
        });

        return view;
    }

    private void login(){
        String phone = aq.id(R.id.phone_input).text();
        String pin = aq.id(R.id.pin_input).text();

        if(phone.length() != 10){
            aq.toast("Invalid Phone Number");
            return;
        }

        if(pin.length() != 4){
            aq.toast("PIN must be 4 digits");
            return;
        }

        progressDialog.show();

        Call<LoginResponse> call = RetrofitClient.getInstance().create(APIInterface.class).login(phone,pin);

        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                if(response.isSuccessful()){
                    progressDialog.dismiss();
                    if(response.body().code == 1){
                        aq.toast(response.body().message);
                        processLogin(response.body().data);
                    }else{
                        Utils.errorDialog(getActivity(), response.body().message);
                    }

                }else{
                    progressDialog.dismiss();
                    if(response.code() == 403){
                        Utils.errorDialog(getActivity(), "Invalid credentials. Your phone number or PIN entered are incorrect");
                    }else {
                        Utils.errorDialog(getActivity(), "An error occurred. Please try again later.");
                    }
                    try {
                        KSUtils.logE("Login OnError -> "+ response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                progressDialog.dismiss();
                if(t instanceof IOException){
                    Utils.errorDialog(getActivity(), "Please check your internet settings to ensure the device has network connection.");

                }else{
                    Utils.errorDialog(getActivity(), "An error occurred. Please try again later.");

                }

                KSUtils.logE("Login OnFailure -> "+ t.getLocalizedMessage());
            }
        });
    }

    private void processLogin(User user){
        Prefs.putString(AccountManager.TOKEN, user.token);
        Prefs.putString(AccountManager.NAME, user.getName());
        Prefs.putString(AccountManager.PHONE, user.phone);
        Prefs.putString(AccountManager.MOBILE, user.mobile);
        Prefs.putString(AccountManager.MARITAL_STATUS, user.marital_status);
        Prefs.putString(AccountManager.EMAIL, user.email);
        Prefs.putString(AccountManager.NUMBER, user.number);
        Prefs.putString(AccountManager.ADDRESS, user.address);
        Prefs.putString(AccountManager.SEX, user.sex);
        Prefs.putString(AccountManager.INITIALS, user.getInitials());
        Prefs.putString(AccountManager.CENTER_NAME, user.center_name);
        Prefs.putString(AccountManager.CENTER_ADDRESS, user.center_address);
        Navigation.findNavController(view).navigate(R.id.nav_home);
    }
}