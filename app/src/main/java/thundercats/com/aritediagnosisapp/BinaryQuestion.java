package thundercats.com.aritediagnosisapp;


import android.view.View;
import android.widget.Button;
import android.widget.TextView;



/**
 * Class for yes/no question format
 */

public class BinaryQuestion extends BaseQuestion {
    protected Button yesButtonView,noButtonView;
    private int yesButtonTextRes = R.string.yes_text, noButtonTextRes = R.string.no_text;
    private boolean value, not_first_trial;
    private String nextYesID, nextNoID;
    private View.OnClickListener yesButtonListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            setValue(true);
            afterClick();
        }
    };
    private View.OnClickListener noButtonListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            setValue(false);
            afterClick();
        }
    };

    /*
    Lots of different constructors for question configurations.
    titleTextResID, subtitleTextResID, ID, section mandatory
    all others optional for various configs.
     */

    public BinaryQuestion(String ID, QuestionSection section, String glossaryTerm, int titleTextResID, int subtitleTextResID, String nextYesID, String nextNoID, boolean skip) {
        super(ID, section, glossaryTerm, titleTextResID, subtitleTextResID);
        this.nextYesID = nextYesID;
        this.nextNoID = nextNoID;
        this.setAllowSkip(skip);
    }

    public BinaryQuestion(String ID, QuestionSection section, String glossaryTerm, String DBColumnID, int titleTextResID, int subtitleTextResID, String nextYesID, String nextNoID, boolean skip) {
        super(ID, section, glossaryTerm, DBColumnID, titleTextResID, subtitleTextResID);
        this.nextYesID = nextYesID;
        this.nextNoID = nextNoID;
        this.setAllowSkip(skip);
    }

    public BinaryQuestion(String ID, String DBColumnID, int titleTextResID, int subtitleTextResID, String nextYes, String nextNo, QuestionSection section, boolean skip) {
        super(ID, DBColumnID, titleTextResID, subtitleTextResID,section);
        this.nextYesID = nextYes;
        this.nextNoID = nextNo;
        this.setAllowSkip(skip);
    }
    public BinaryQuestion(String ID, int titleTextResID, int subtitleTextResID, String nextYesID, String nextNoID,QuestionSection section, boolean skip) {
        super(ID, titleTextResID, subtitleTextResID,section);
        this.nextYesID = nextYesID;
        this.nextNoID = nextNoID;
        this.setAllowSkip(skip);
    }

    public BinaryQuestion(String ID, QuestionSection section, String glossaryTerm, int titleTextResID, int subtitleTextResID, int yesButtonTextRes, int noButtonTextRes, boolean skip) {
        super(ID, section, glossaryTerm, titleTextResID, subtitleTextResID);
        this.yesButtonTextRes = yesButtonTextRes;
        this.noButtonTextRes = noButtonTextRes;
        this.setAllowSkip(skip);
    }

    public BinaryQuestion(String ID, QuestionSection section, String glossaryTerm, String DBColumnID, int titleTextResID, int subtitleTextResID, int yesButtonTextRes, int noButtonTextRes, boolean skip) {
        super(ID, section, glossaryTerm, DBColumnID, titleTextResID, subtitleTextResID);
        this.yesButtonTextRes = yesButtonTextRes;
        this.noButtonTextRes = noButtonTextRes;
        this.setAllowSkip(skip);
    }

    public BinaryQuestion(String ID, int titleTextResID, int subtitleTextResID, QuestionSection section, int yesButtonTextRes, int noButtonTextRes, boolean skip) {
        super(ID, titleTextResID, subtitleTextResID, section);
        this.yesButtonTextRes = yesButtonTextRes;
        this.noButtonTextRes = noButtonTextRes;
        this.setAllowSkip(skip);
    }

    public BinaryQuestion(String ID, String DBColumnID, int titleTextResID, int subtitleTextResID, QuestionSection section, int yesButtonTextRes, int noButtonTextRes, boolean skip) {
        super(ID, DBColumnID, titleTextResID, subtitleTextResID, section);
        this.yesButtonTextRes = yesButtonTextRes;
        this.noButtonTextRes = noButtonTextRes;
        this.setAllowSkip(skip);
    }

    /**
     * Add yes and no buttons and link views to vars then set listeners
     */
    @Override
    public void setupView() {
        super.setupView();
        fillNavigationContent(R.layout.binary_question_layout_full);
        linkYesButtonView();
        linkNoButtonView();
        setListeners();
        GlobalClass gc = ((GlobalClass) getHostActivity().getApplication());
        if(gc.isPatientActive(gc.currentPatient.getId())){
            int minsLeft = gc.getAssessedTimeLeft(gc.currentPatient.getId());
            int minutes = (10-minsLeft);
            String lastSeen = "Last assessed " + minutes + " minutes ago.\n";
            TextView string = getHostActivity().findViewById(R.id.lastSeenText);
            if (minutes >= 10) {
                not_first_trial = true;
                yesButtonView.setText(R.string.reassess_now);
                noButtonView.setText(R.string.reassess_later);
                lastSeen += "Patient ready for reassessment";
                string.setTextColor(getHostActivity().getResources().getColor(R.color.severeDiagnosisColor));
            } else if(minutes<10) {
                lastSeen += "\n Patient will be ready for reassessment in " + minsLeft + " minutes";
            } else {
                string.setTextColor(getHostActivity().getResources().getColor(R.color.darkGray));
            }
            string.setText(lastSeen);
            getHostActivity().findViewById(R.id.lastSeenText).setVisibility(View.VISIBLE);
        } else {
            getHostActivity().findViewById(R.id.lastSeenText).setVisibility(View.GONE);
        }

    }

    /**
     * Converts binary value to string for database entry
     *
     * @return value to string
     */
    @Override
    public String getDBValue() {
        return getValue()?"1":"0";
    }

    public boolean getValue() {
        return value;
    }

    protected void setValue(boolean value) {
        this.value = value;
    }

    /**
     * Finds yes button in layout and sets button text accordingly
     */
    public void linkYesButtonView(){
        yesButtonView = getHostActivity().findViewById(R.id.YesButton);
        yesButtonView.setText(yesButtonTextRes);
    }

    /**
     * Finds no button in layout and sets button text accordingly
     */
    public void linkNoButtonView(){
        noButtonView = getHostActivity().findViewById(R.id.NoButton);
        noButtonView.setText(noButtonTextRes);
    }

    /**
     * set button listeners
     */
    public void setListeners(){
        yesButtonView.setOnClickListener(yesButtonListener);
        noButtonView.setOnClickListener(noButtonListener);
    }

    public void setYesButtonListener(View.OnClickListener yesButtonListener) {
        this.yesButtonListener = yesButtonListener;
        setListeners();
    }

    public void setNoButtonListener(View.OnClickListener noButtonListener) {
        this.noButtonListener = noButtonListener;
        setListeners();
    }

    /**
     * Set default navigation behavior: if yes then return yesID, else return noID
     * @return next dynamic item ID
     */
    @Override
    public String nextNavigation() {
        return value?getNextYesID():getNextNoID();
    }

    public String getNextYesID() {
        return nextYesID;
    }

    public void setNextYesID(String nextYesID) {
        this.nextYesID = nextYesID;
    }

    public String getNextNoID() {
        return nextNoID;
    }

    public void setNextNoID(String nextNoID) {
        this.nextNoID = nextNoID;
    }

    public boolean getNotFirstTrial(){return not_first_trial;} //checks for repeat bronchodilator trials in PneumoniaSection

    /**
     * Highlights previous answer selection with orange border
     */
    @Override
    public void renderPrevious() {
        if (value) {
            yesButtonView.setBackgroundResource(R.drawable.primary_focused);
        } else {
            noButtonView.setBackgroundResource(R.drawable.secondary_focused);
        }
    }
}
