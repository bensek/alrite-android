package thundercats.com.aritediagnosisapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

/**
 * Base class for card
 * Used in glossary popup and in diagnosis popup
 */

public class BaseDynamicCard {
    protected int layoutRes;            //resource ID for layout
    protected LinearLayout container;   //parent layout

    public BaseDynamicCard() {
    }

    /**
     * This function inflates the card and sets the parent container variable
     *
     * @param inflater The activity's layout inflater
     * @param context  activity context
     * @return inflated view
     */
    public View inflateCard(LayoutInflater inflater, Context context) {
        View view = inflater.inflate(R.layout.base_card_layout, null);
        this.container = view.findViewById(R.id.Container);
        return view;
    }
}
