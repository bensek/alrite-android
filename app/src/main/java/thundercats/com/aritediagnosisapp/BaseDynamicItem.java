package thundercats.com.aritediagnosisapp;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.Menu;
import android.view.View;
import android.widget.LinearLayout;

import thundercats.com.aritediagnosisapp.activities.MainActivity;

/**
 * Base class for dynamically rendered items (currently only questions)
 */

public abstract class BaseDynamicItem {
    protected String glossaryTermID;
    private String ID;
    private Activity hostActivity;
    private LinearLayout navigationContent;
    private Type type;
    private Toolbar myToolbar;
    private Menu menu;
    private QuestionSection section;


    /**
     * @param ID      item ID
     * @param type    type declaration (for use when multiple dynamic item types are present)
     * @param section parent question section
     */
    public BaseDynamicItem(String ID, Type type, QuestionSection section) {
        this.ID = ID;
        this.type = type;
        this.section = section;
    }

    /**
     *
     * @param ID item ID
     * @param type type declaration (for use when multiple dynamic item types are present)
     * @param section parent question section
     * @param glossaryTerm ID of associated glossary term
     */
    public BaseDynamicItem(String ID, Type type, QuestionSection section, String glossaryTerm) {
        this.ID = ID;
        this.type = type;
        this.section = section;
        this.glossaryTermID = glossaryTerm;
    }


    /**
     * Main function called when host activity starts. This function should handle all layout rendering
     * and setup necessary.
     */
    public void setupView(){
        myToolbar = getHostActivity().findViewById(R.id.my_toolbar);
        ((AppCompatActivity)getHostActivity()).setSupportActionBar(myToolbar);
        ((AppCompatActivity) getHostActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((AppCompatActivity) getHostActivity()).getSupportActionBar().setDisplayShowHomeEnabled(true);
        setNavigationContent((LinearLayout)getHostActivity().findViewById(R.id.DynamicNavigation));
    }

    /**
     * Function to navigate to next dynamic item
     * @param nextID next dynamic item ID, empty does nothing, "home" leads to home screen
     */
    public void toNext(String nextID){
        if(!nextID.isEmpty()) {
            if (nextID.equals("home")) {
                Intent intent = new Intent(getHostActivity(), MainActivity.class);
                getHostActivity().startActivity(intent);
            } else {
                BaseDynamicItem next = ((GlobalClass) getHostActivity().getApplication()).currentPatient.getItemByID(nextID);
                Intent intent = new Intent();
                if (next != null) {
                    switch (next.getType()) {
                        case Diagnosis:
                            intent = new Intent(getHostActivity(), DynamicBaseActivity.class);
                            break;
                        case Question:
                            intent = new Intent(getHostActivity(), DynamicQuestionActivity.class);
                            break;
                    }
                    intent.putExtra("ID", nextID);
                    getHostActivity().startActivity(intent);
                } else {
                    //Error
                }
            }
        } else{
            //Nothing
        }
    }


    /**
     * Menu setter function
     * @param menu menu object
     */
    public void setMenu(Menu menu){
        this.menu = menu;
    }

    /**
     * This function returns the linear layout of the navigation content - this is the bottom section
     * of the layout where navigation buttons are placed.
     * @return navigation content linear layout
     */
    public LinearLayout getNavigationContent() {
        return navigationContent;
    }

    /**
     * Sets the navigation content layout - this is the bottom section of the layout where navigation buttons appear.
     * @param navigationContent bottom section of the screen where navigation buttons appear.
     */
    public void setNavigationContent(LinearLayout navigationContent) {
        this.navigationContent = navigationContent;
    }

    /**
     * Renders navigation content layout with given layout resource ID
     * @param id layout resource ID
     */
    public void fillNavigationContent(int id){
        putContent(navigationContent,id);
    }

    /**
     * Fills linear  or constraint layout with given layout resource ID
     * @param layout layout to fill with new sub layout
     * @param id sub layout resource ID
     */
    public void putContent(LinearLayout layout, int id){
        layout.removeAllViews();
        View questionContent = getHostActivity().getLayoutInflater().inflate(id,layout,false);
        layout.addView(questionContent);
    }


    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public Activity getHostActivity() {
        return hostActivity;
    }

    /**
     * Sets the host activity for the dynamic item - should be an instance of DynamicBaseActivity.
     * Call this function first!
     * @param hostActivity hosting activity for this dynamic item
     */
    public void setHostActivity(Activity hostActivity) {
        this.hostActivity = hostActivity;
    }

    /**
     * Sets the toolbar color to the given color resource ID
     * @param id color resource ID
     */
    public void setToolbarColor(int id){
        myToolbar.setBackgroundColor(getHostActivity().getResources().getColor(id));
    }

    /**
     * Sets toolbar title text to string
     * @param s toolbar title text
     */
    public void setToolbarText(String s) {
        ((AppCompatActivity)getHostActivity()).setSupportActionBar(myToolbar);
        myToolbar.setTitle(s);
    }

    /**
     * Sets toolbar title text to string resource
     * @param id string resource ID
     */
    public void setToolbarText(int id) {
        setToolbarText(getHostActivity().getResources().getString(id));
    }

    /**
     * Sets toolbar title text color to color resource ID
     * @param id color resource ID
     */
    public void setToolbarTextColor(int id){
        myToolbar.setTitleTextColor(getHostActivity().getResources().getColor(id));
    }

    public Toolbar getMyToolbar() {
        return myToolbar;
    }

    public void setMyToolbar(Toolbar myToolbar) {
        this.myToolbar = myToolbar;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public QuestionSection getSection() {
        return section;
    }

    public void setSection(QuestionSection section) {
        this.section = section;
    }

    /**
     * Launches basic text alert dialog over host activity using text resource ids
     * @param titleID title text resource ID
     * @param messageID message text resource ID
     */
    public void basicAlertDialog(int titleID, int messageID){
        basicAlertDialog(hostActivity.getResources().getString(titleID),hostActivity.getResources().getString(messageID));
    }

    /**
     * Launches basic text alert dialog over host activity using strings
     * @param title title text
     * @param message message text
     */
    public void basicAlertDialog(String title, String message){
        AlertDialog.Builder builder1 = new AlertDialog.Builder(getHostActivity());
        builder1.setTitle(title);
        builder1.setMessage(message);
        builder1.setCancelable(true);

        if(title == "Invalid Temperature") {
            builder1.setPositiveButton(R.string.continue_text,
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });

        } else if(title == hostActivity.getResources().getString(R.string.age_alert_text)) {
            builder1.setPositiveButton(R.string.understand_text,
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            ((GlobalClass) getHostActivity().getApplication()).currentPatient.clicked = true;
                            dialog.cancel();
                        }
                    });
            builder1.setNegativeButton(R.string.return_home_text,
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            Intent intent = new Intent((getHostActivity()), MainActivity.class);
                            (getHostActivity()).startActivity(intent);
                        }
                    });

        } else {
            builder1.setPositiveButton(R.string.no_continue_text,
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });

            builder1.setNegativeButton(R.string.yes_save_and_exit_text, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {

                    ((GlobalClass) getHostActivity().getApplication()).currentPatient.pneumoniaSection.diagnosis.setExitOnlyLayout(getHostActivity());
                    ((GlobalClass) getHostActivity().getApplication()).currentPatient.pneumoniaSection.diagnosis.showPopup(getHostActivity());
                }
            });

        }
        AlertDialog alert11 = builder1.create();
        alert11.show();
    }

    /**
     * Dynamic item type
     */
    public enum Type {
        Diagnosis, Question
    }
}
