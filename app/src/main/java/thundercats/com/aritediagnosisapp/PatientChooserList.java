package thundercats.com.aritediagnosisapp;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.View;
import android.widget.AdapterView;

import java.util.ArrayList;


/**
 * Page showing patients in system when user navigates to select existing patient for diagnosis.
 */

public class PatientChooserList extends SearchableListViewHelper {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setToolbarTextID(R.string.choose_patient_title);
    }

    /**
     * setup list of patients and link click to show appropriate patient profile.
     */
    @Override
    public void onStart() {
        super.onStart();

        setItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                String patID = adapter.getItem(i).getId();
                Intent intent = new Intent(PatientChooserList.this, PatientProfileActivity.class);
                intent.putExtra("ID", patID);
                startActivity(intent);
            }
        });
        setArrayList(getPatientsList());
        updateListView();
        updateListListener();
    }

    /**
     * Generates list of current patients - each patient as ListItem with name and age category.
     *
     * @return ArrayList of patients as ListItems
     */
    public ArrayList<ListItem> getPatientsList() {
        ArrayList<ListItem> items = new ArrayList<>();
        Cursor cursor = dbHelper.getAllPatients();
        try {
            for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                int age = cursor.getInt(cursor.getColumnIndex("Birthday"));
                String age_cat = getResources().getStringArray(R.array.age_categories)[age];
                String patientID = cursor.getString(cursor.getColumnIndex("ID")),
                        title = cursor.getString(cursor.getColumnIndex("Surname")) +" "+ cursor.getString(cursor.getColumnIndex("Givenname")) +" "+ cursor.getString(cursor.getColumnIndex("Nickname"));

                if(globalClass.isPatientActive(patientID) == false) {
                    items.add(new ListItem(title, age_cat, patientID));
                }
            }
        } finally {
            cursor.close();
        }
        return items;
    }
}
