package thundercats.com.aritediagnosisapp;

import android.content.Context;
import android.media.MediaPlayer;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.VideoView;

/**
 * Class for video glossary card.
 * tap to toggle play/pause video
 * video loops automatically.
 * Video resources located in assets folder
 */

public class VideoCard extends BaseDynamicCard {
    private VideoView videoView;
    private Uri uri;
    private ImageButton play;
    private int width, height;

    public VideoCard(Uri uri) {
        this.uri = uri;
    }

    /**
     * Inflates glossary card with video resource provided.
     *
     * @param inflater The activity's layout inflater
     * @param context  activity context
     * @return view representing generated video card
     */
    @Override
    public View inflateCard(LayoutInflater inflater, Context context) {
        View view = super.inflateCard(inflater, context);

        final View innerView = inflater.inflate(R.layout.video_card_layout, null);

        videoView = innerView.findViewById(R.id.VideoView);
        videoView.setVideoURI(uri);

        play = innerView.findViewById(R.id.ImageButton);


        play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (videoView.isPlaying()) {
                    videoView.pause();
                    play.setAlpha(0.5f);
                    play.setImageResource(R.drawable.ic_play_circle_outline_black_24dp); //play icon
                } else {
                    videoView.start();
                    play.setAlpha(0.0f);
                }
            }
        });

        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mp.setLooping(true);
                width = mp.getVideoWidth();
                height = mp.getVideoHeight();

            }
        });

        container.addView(innerView);
        return view;
    }
}
