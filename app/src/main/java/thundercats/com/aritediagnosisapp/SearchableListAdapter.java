package thundercats.com.aritediagnosisapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Adapter for searchable lists. Makes creating lists much easier
 */

public class SearchableListAdapter extends BaseAdapter {
    private Context mContext;
    private LayoutInflater inflater;
    private List<ListItem> itemList;
    private ArrayList<ListItem> arrayList;

    public SearchableListAdapter(Context context, List<ListItem> itemList) {
        mContext = context;
        this.itemList = itemList;
        inflater = LayoutInflater.from(mContext);
        this.arrayList = new ArrayList<ListItem>();
        this.arrayList.addAll(itemList);
    }

    @Override
    public int getCount() {
        return itemList.size();
    }

    @Override
    public ListItem getItem(int position) {
        return itemList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    /**
     * Gets the individual view of the list item at specified position
     *
     * @param position
     * @param view
     * @param parent
     * @return
     */
    public View getView(final int position, View view, ViewGroup parent) {
        final ViewHolder holder;
        if (view == null) {
            holder = new ViewHolder();

            view = inflater.inflate(R.layout.list_item, null);

            holder.title = view.findViewById(R.id.Title);
            holder.subtitle = view.findViewById(R.id.Subtitle);

            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        // Set the results into TextViews
        holder.title.setText(itemList.get(position).getTitle());
        holder.subtitle.setText(itemList.get(position).getDefinition());
        return view;
    }

    /**
     * Filter listItems based on search box
     * @param charText searched text
     */
    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        itemList.clear();
        if (charText.length() == 0) {
            itemList.addAll(arrayList);
        } else {
            for (ListItem wp : arrayList) {
                if ((wp.getTitle()).toLowerCase(Locale.getDefault()).contains(charText)) {
                    itemList.add(wp);
                }
            }
        }
        notifyDataSetChanged();
    }

    public class ViewHolder {
        TextView title, subtitle;
    }

}