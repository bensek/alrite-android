package thundercats.com.aritediagnosisapp;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * Card class for the glossary section. Represents a card showing a list of instructions - see bronchodilator.
 */

public class NumberedStepsCard extends BaseDynamicCard {
    private String[] steps;
    private LinearLayout listView;
    private int alertNotificationText;
    private boolean alert;

    public NumberedStepsCard(String[] steps) {
        layoutRes = R.layout.numbered_steps_card_layout;
        this.steps = steps;
    }

    public NumberedStepsCard(String[] steps, boolean alert, int alertTextId) {
        layoutRes = R.layout.alert_card_layout;
        this.alertNotificationText = alertTextId;
        this.alert = alert;
        this.steps = steps;
    }

    /**
     * Inflates steps card
     *
     * @param inflater The activity's layout inflater
     * @param context  activity context
     * @return view representing steps card
     */
    @Override
    public View inflateCard(LayoutInflater inflater, Context context) {
        View view = super.inflateCard(inflater, context);
        View innerView = inflater.inflate(layoutRes, null);
        if(alert){
            TextView tmp = innerView.findViewById(R.id.alertText);
            tmp.setText(alertNotificationText);
        }
        listView = innerView.findViewById(R.id.ListView);

        for (int ii = 0; ii < steps.length; ii++) {
            View tmp = inflater.inflate(R.layout.single_step_fragment, null);
            TextView num = tmp.findViewById(R.id.Title),
                    def = tmp.findViewById(R.id.Subtitle);

            num.setText("\u2022");
            def.setText(steps[ii]);
            listView.addView(tmp);
        }

        container.addView(innerView);
        return view;
    }

    public String[] getSteps() {
        return steps;
    }

    public void setSteps(String[] steps) {
        this.steps = steps;
    }

}
