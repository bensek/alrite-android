package thundercats.com.aritediagnosisapp;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.View;
import android.widget.AdapterView;

import java.util.ArrayList;
import java.util.LinkedList;

/**
 * Activity to show previously answered questions linked to pneumoniaSection of current patient
 * displays a list of the previous questions, clicking an item brings up that question
 */

public class AnsweredQuestionsActivity extends SearchableListViewHelper {
    private PatientSection currentPatient;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setToolbarTextID(R.string.answered_questions);
    }

    @Override
    public void onStart() {
        super.onStart();
        currentPatient = ((GlobalClass) getApplication()).currentPatient;
        ArrayList<ListItem> tmp = makeListItems();
        setArrayList(tmp);
        listener();
        updateListView();
        updateListListener();
    }

    /**
     * Listener for card clicked - pulls up current question
     */
    public void listener() {
        setItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> adapter, View v, int position,
                                    long arg3) {
                ListItem item = (ListItem) adapter.getItemAtPosition(position);
                Intent intent = new Intent(AnsweredQuestionsActivity.this, DynamicQuestionActivity.class);
                intent.putExtra("ID", item.getId());
                startActivity(intent);
            }

        });

    }

    /**
     * This function generates the list items based on the current pneumonia section's answered questions
     * @return returns arraylist of listItems
     */
    public ArrayList<ListItem> makeListItems() {
        ArrayList<ListItem> items = new ArrayList<>();
        LinkedList<BaseQuestion> tmp = currentPatient.pneumoniaSection.getAnsweredQuestions();
        for (BaseQuestion bq : tmp) {
            items.add(bq.toListItem());
        }
        return items;
    }

    /**
     * override default alphabetical sort to retain question ordering - don't do any sorting
     */
    @Override
    public void sortArrayList() {
        //Don't sort
    }
}
