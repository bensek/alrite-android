package thundercats.com.aritediagnosisapp.utils;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;

import com.google.gson.Gson;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class Utils {

    public static void errorDialog(Context context, String message){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        //Setting message manually and performing action on button click
        builder.setMessage(message)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
//                .setNegativeButton("No", new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int id) {
//                        //  Action for 'NO' Button
//                        dialog.cancel();
//                        Toast.makeText(getApplicationContext(),"you choose no action for alertbox",
//                                Toast.LENGTH_SHORT).show();
//                    }
//                });
        //Creating dialog box
        AlertDialog alert = builder.create();
        //Setting the title manually
        alert.setTitle("Error Occurred");
        alert.show();
    }

    public static String toJSONString(Object object){
        Gson g = new Gson();
        String json = g.toJson(object);

        return json;
    }

    public static String currentDate(){
        Date c = Calendar.getInstance().getTime();
        System.out.println("Current time => " + c);

        SimpleDateFormat df = new SimpleDateFormat("EEE, dd MMM yyyy", Locale.getDefault());
        String formattedDate = df.format(c);

        return formattedDate;
    }

    public static String formatDate(String timestamp){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
//        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));

        Calendar calendar = Calendar.getInstance();
        try {
            calendar.setTime(sdf.parse(timestamp));
        } catch (Exception ex) {
            ex.printStackTrace();
        }

//        SimpleDateFormat returnFormat = new SimpleDateFormat("dd-mm-yyyy hh:mm:ss");
//        DateFormat rfc1123 = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss 'GMT'", Locale.US);
        DateFormat returnFormat = new SimpleDateFormat("EEE, dd MMM yyyy", Locale.ENGLISH);
        String finalStr = returnFormat.format(calendar.getTime());
        return finalStr;
    }

    public static boolean isNetworkConnected(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected();
    }
}
