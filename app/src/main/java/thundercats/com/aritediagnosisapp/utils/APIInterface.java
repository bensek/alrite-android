package thundercats.com.aritediagnosisapp.utils;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;
import thundercats.com.aritediagnosisapp.models.Assessment;
import thundercats.com.aritediagnosisapp.models.LoginResponse;
import thundercats.com.aritediagnosisapp.models.Patient;
import thundercats.com.aritediagnosisapp.models.User;

public interface APIInterface {

    @POST("login")
    Call<LoginResponse> login(@Query("phone") String phone, @Query("password") String pin);

    @GET("get-my-patients")
    Call<List<Patient>> getMyPatients();

    @POST("create-patient")
    Call<Patient> createPatient(@Query("surname") String surname, @Query("given_name") String given_name, @Query("nickname") String nickname,
                                @Query("sex") String sex, @Query("age_range") String age_range, @Query("weight") String weight);

    @POST("create-assessment")
    Call<Assessment> createAssessment(@Query("patient_id") int patient_id, @Query("title") String title);
}
