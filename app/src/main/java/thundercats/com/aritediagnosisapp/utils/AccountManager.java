package thundercats.com.aritediagnosisapp.utils;

import com.pixplicity.easyprefs.library.Prefs;

public class AccountManager {
    public static String TOKEN = "user_token";
    public static String CURRENT_PATIENT = "current_patient";
    public static String NAME = "name";
    public static String PHONE = "phone";
    public static String MOBILE = "mobile";
    public static String NUMBER = "number";
    public static String EMAIL = "email";
    public static String ADDRESS = "address";
    public static String MARITAL_STATUS = "marital_status";
    public static String SEX = "sex";
    public static String CENTER_NAME = "center_name";
    public static String CENTER_ADDRESS = "center_address";
    public static String INITIALS = "initials";
}
