package thundercats.com.aritediagnosisapp.utils;

import android.content.Context;
import android.content.Intent;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.GsonBuilder;
import com.pixplicity.easyprefs.library.Prefs;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;
import thundercats.com.aritediagnosisapp.GlobalClass;
import thundercats.com.aritediagnosisapp.activities.MainActivity;

public class RetrofitClient {
    public static Retrofit getInstance() {
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(chain -> {
                    Request.Builder builder = chain.request().newBuilder();
                    builder.addHeader("Accept", "application/json");
                    builder.addHeader("Authorization", "Bearer " + Prefs.getString(AccountManager.TOKEN, ""));
                    Request request = builder.build();
                    Response response = chain.proceed(request);

                    if(response.code() == 401){
                        Prefs.remove(AccountManager.TOKEN);
                        Context cxt = GlobalClass.getContext();
                        Intent intent = new Intent(cxt, MainActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        cxt.startActivity(intent);
                    }

                    return response;
                }).build();

        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.setLenient();
        gsonBuilder.setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES);

        return new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gsonBuilder.create()))
                .client(client)
                .build();
    }
}
