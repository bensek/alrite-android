package thundercats.com.aritediagnosisapp;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.kolastudios.KSUtils;
import com.pixplicity.easyprefs.library.Prefs;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import thundercats.com.aritediagnosisapp.adapter.AssessmentsAdapter;
import thundercats.com.aritediagnosisapp.models.Assessment;
import thundercats.com.aritediagnosisapp.models.Patient;
import thundercats.com.aritediagnosisapp.utils.AccountManager;

/**
 * Activity showing individual patient info including visit history.
 */

public class PatientProfileActivity2 extends SearchableListViewHelper {
    private TextView nameView, birthdayView, genderView, numberView;
    private Toolbar myToolbar;
    private RecyclerView listView;
    private Long patientID;
    private Patient patient;
    private List<Assessment> assessmentList;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setToolbarTextID(R.string.patient_profile);
    }

    /**
     * General activity setup.
     * Sets up top info section with name, birthday, gender and the create new visit button.
     * Sets up bottom patient history list view with diagnosis type and date of visit.
     */
    @Override
    public void onStart() {
        super.onStart();

        Intent intent = getIntent();
        patientID = intent.getLongExtra("PatientId", 0);
        patient = Patient.findById(Patient.class, patientID);
        assessmentList = Assessment.find(Assessment.class, "patientid =?", String.valueOf(patientID));

        setContentView(R.layout.patient_profile_layout);
        setupToolbar();

        listView = findViewById(R.id.ListView);
        listView.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        nameView = findViewById(R.id.Name);
        birthdayView = findViewById(R.id.Birthday);
        genderView = findViewById(R.id.Gender);
        numberView = findViewById(R.id.patient_number);

        AssessmentsAdapter assessmentsAdapter = new AssessmentsAdapter(this, assessmentList, l->{});
        listView.setAdapter(assessmentsAdapter);
        assessmentsAdapter.notifyDataSetChanged();

        //setList(listView);
//        setArrayList(getReportsForPatient());
        //updateListView();

        nameView.setText(patient.getName());
        birthdayView.setText(patient.age_range);
        genderView.setText(patient.sex);
        numberView.setText(patient.number);

        findViewById(R.id.NewVisit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Prefs.putInt(AccountManager.CURRENT_PATIENT, patientID.intValue());
                PatientSection patientSection = new PatientSection(String.valueOf(patientID), patient.surname, patient.given_name, patient.nickname, 0, globalClass);
                patientSection.pneumoniaSection.startSection(PatientProfileActivity2.this);

            }
        });
    }

    /**
     * Generates the list representation of previous visits. ListItem with diagnosis type and visit date
     *
     * @return ArrayList of ListItems
     */
//    public ArrayList<ListItem> getReportsForPatient() {
//        ArrayList<ListItem> items = new ArrayList<>();
//        Cursor cursor = dbHelper.getReportsByPatient(patientID);
//
//        try {
//            for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
//                Date date = dbHelper.dateFromSQLString(cursor.getString(cursor.getColumnIndex("Created")));
//
//                String reportID = cursor.getString(cursor.getColumnIndex("ID")),
//                        diagnosis = cursor.getString(cursor.getColumnIndex("Diagnosis_Title")),
//                        createdDate = getResources().getString(R.string.date_of_evaluation) + ": " + globalClass.formatLocaleDate(date);
//
//                if (diagnosis == null) {
//                    diagnosis = getResources().getString(R.string.incomplete);
//                }
//                if (diagnosis.isEmpty()) {
//                    diagnosis = getResources().getString(R.string.incomplete);
//                }
//                items.add(new ListItem(diagnosis, createdDate, date.getTime() + "@" + reportID));
//            }
//        } finally {
//            cursor.close();
//        }
//        return items;
//    }

    /**
     * Handles back arrow press in toolbar to perform same action as back button press
     * @return
     */
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    /**
     * Sort patient history based on last visit date. Extract date from id string. format: datestring@diagnosisID
     */
    @Override
    public void sortArrayList() {
        Collections.sort(getArrayList(), new Comparator<ListItem>() {
            @Override
            public int compare(ListItem listItem, ListItem t1) {
                String id = listItem.getId();
                String timestamp = id.split("@")[0];
                String id1 = t1.getId();
                String timestamp1 = id1.split("@")[0];
                long d1 = Long.parseLong(timestamp),
                        d2 = Long.parseLong(timestamp1);
                return (int) (d2 - d1);
            }
        });
    }
}
