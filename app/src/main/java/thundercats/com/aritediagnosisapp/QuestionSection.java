package thundercats.com.aritediagnosisapp;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;

import java.util.HashMap;
import java.util.LinkedList;

/**
 * Base class for question sections. Contains list of questions, reference to first question and answered questions.
 */

public class QuestionSection extends DatabaseModel{
    private LinkedList<BaseQuestion> answeredQuestions;
    private BaseQuestion firstQuestion;
    private BaseDynamicItem currentItem;
    private NavigationGenerator navigationGenerator;
    private HashMap<String, BaseQuestion> items;

    public QuestionSection(DataTable dataTable, GlobalClass globalClass) {
        super(dataTable, globalClass);
        items = new HashMap<>();
        navigationGenerator = new NavigationGenerator(this);
        answeredQuestions = navigationGenerator.getList();
    }

    public void putItem(BaseQuestion item) {
        items.put(item.getID(),item);
    }

    public BaseQuestion getFirstQuestion() {
        return firstQuestion;
    }

    public void setFirstQuestion(BaseQuestion firstQuestion) {
        this.firstQuestion = firstQuestion;
    }

    public BaseQuestion getItemByID(String id) {
        return items.get(id);
    }

    public boolean containsItem(String id){return items.containsKey(id);}

    public BaseDynamicItem getCurrentQuestion() {
        return currentItem;
    }

    public void setCurrentQuestion(BaseDynamicItem currentItem) {
        this.currentItem = currentItem;
    }

    /**
     * Converts all answered questions to content values for storage in database
     *
     * @return populated ContentValues object
     */
    @Override
    public ContentValues makeContentValues(){
        ContentValues values = super.makeContentValues();
        for(BaseDynamicItem item:items.values()){
            if(item.getType().equals(BaseDynamicItem.Type.Question)){
                BaseQuestion baseQuestion = (BaseQuestion)item;

                if (baseQuestion.isCompleted() && baseQuestion.isDBQuestion()) {
                    values.put(baseQuestion.getDBColumnID(),baseQuestion.getDBValue());
                }
            }
        }
        return values;
    }


    /**
     * Method to begin section - will show the first question of the section.
     * @param context current activity context
     */
    public void startSection(Context context) {
        Intent intent = new Intent(context, DynamicQuestionActivity.class);
        intent.putExtra("ID", firstQuestion.getID());
        context.startActivity(intent);
    }

    /**
     * returns list of answered questions made by navigationGenerator
     * @return answered questions
     */
    public LinkedList<BaseQuestion> getAnsweredQuestions() {
        answeredQuestions = navigationGenerator.getList();
        return answeredQuestions;
    }

    public HashMap<String, BaseQuestion> getItems() {
        return items;
    }
}
