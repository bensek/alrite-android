package thundercats.com.aritediagnosisapp;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.navigation.Navigation;

import android.view.View;
import android.widget.Button;

import thundercats.com.aritediagnosisapp.activities.MainActivity;
import thundercats.com.aritediagnosisapp.activities.NewPatientActivity;
import thundercats.com.aritediagnosisapp.activities.PatientsListActivity;

/**
 * Activity to show option to select existing patient or create new
 */

public class SelectPatientActivity extends AppCompatActivity {
    private Button newPatient, chooseExisting;
    private View view;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.select_patient_layout);
        Toolbar myToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(R.string.choose_patient_title);
        myToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        newPatient = findViewById(R.id.NewPatient);
        chooseExisting = findViewById(R.id.ChooseExisting);

        newPatient.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                GlobalClass globalClass = (GlobalClass) getApplication();
//                PatientSection patientSection = new PatientSection(globalClass);
//                patientSection.startSection(SelectPatientActivity.this);
                Intent intent = new Intent(SelectPatientActivity.this, NewPatientActivity.class);
                startActivity(intent);
            }
        });

        chooseExisting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SelectPatientActivity.this, PatientsListActivity.class);
//                intent.putExtra("FRAGMENT", "PATIENTS");
                startActivity(intent);
            }
        });
    }
}
