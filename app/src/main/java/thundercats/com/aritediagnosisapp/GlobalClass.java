package thundercats.com.aritediagnosisapp;

import android.app.Application;
import android.content.Context;

import androidx.appcompat.app.AppCompatDelegate;
import androidx.multidex.MultiDex;

import com.kolastudios.KSUtils;
import com.orm.SugarApp;
import com.orm.SugarContext;
import com.orm.SugarRecord;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import thundercats.com.aritediagnosisapp.utils.Constants;

/**
 * Instance of application. Holds all important global features. Initiates any items when app starts.
 */

public class GlobalClass extends SugarApp {
    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }


    public DatabaseHelper databaseHelper;
    public ArrayList<GlossaryItem> glossaryItems;
    public HashMap<String, PatientSection> patientsMap;
    public PatientSection currentPatient;
    private HashMap<String, Integer> trackTimes;
    private boolean isSettings_done;
    private static GlobalClass instance;

    public ArrayList<String> waitingPatients;
    public ArrayList<String> soonPatients;

    public boolean bronchodilator_present, spacer_present, pulseOx_present, thermometer_present, stethoscope_present, suppOx_present, antibiotics_present, steroids_present, nebulizer_present, disclaimer;


    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;

        new KSUtils.Builder()
                .setContext(this)
                .setLogTag(Constants.LOG_TAG)
                .setLogEnabled(Constants.LOG_ENABLED)
                .build();

        SugarContext.init(this);

        databaseHelper = new DatabaseHelper(this);
        patientsMap = new HashMap<>();
        trackTimes = new HashMap<>();


        if (databaseHelper.getTableItemByID("clinic", databaseHelper.CLINIC).getCount() > 0) {
            isSettings_done = Boolean.valueOf(databaseHelper.getTableItemByID("clinic", databaseHelper.CLINIC).getString(0));
            bronchodilator_present = Boolean.valueOf(databaseHelper.getTableItemByID("clinic", databaseHelper.CLINIC).getString(1));
            spacer_present = Boolean.valueOf(databaseHelper.getTableItemByID("clinic", databaseHelper.CLINIC).getString(2));
            pulseOx_present = Boolean.valueOf(databaseHelper.getTableItemByID("clinic", databaseHelper.CLINIC).getString(3));
            stethoscope_present = Boolean.valueOf(databaseHelper.getTableItemByID("clinic", databaseHelper.CLINIC).getString(4));
            thermometer_present = Boolean.valueOf(databaseHelper.getTableItemByID("clinic", databaseHelper.CLINIC).getString(5));
            nebulizer_present = Boolean.valueOf(databaseHelper.getTableItemByID("clinic", databaseHelper.CLINIC).getString(6));
            suppOx_present = Boolean.valueOf(databaseHelper.getTableItemByID("clinic", databaseHelper.CLINIC).getString(7));
            antibiotics_present = Boolean.valueOf(databaseHelper.getTableItemByID("clinic", databaseHelper.CLINIC).getString(8));
            steroids_present = Boolean.valueOf(databaseHelper.getTableItemByID("clinic", databaseHelper.CLINIC).getString(9));
        }

        makeGlossaryItems();
    }
    public static Context getContext(){
        return instance;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }


    /**
     * Converts date to locale text string
     *
     * @param date date
     * @return locale formatted date string
     */
    public String formatLocaleDate(Date date) {
        java.text.DateFormat dateFormat = android.text.format.DateFormat.getDateFormat(this);
        return dateFormat.format(date);
    }

    /**
     * Generate glossary terms and cards. Each id matches the base naming convention used to generate card types.
     * termID_glossary_def for main definition (required for all terms)
     * termID_glossary_video for associated video (optional) located in res/raw
     * termID_glossary_image for associated image (optional) res/drawable
     * termID_glossary_pdf for associated pdf (optional) assets folder
     * termID_glossary_steps for associated numbered steps (optional) string array in strings.xml
     */
    public void makeGlossaryItems() {
        glossaryItems = new ArrayList<>();
        String[] ids = new String[]{"bronchodilator", "convulsions", "diazepam", "pneumonia", "stridor",
                "unresponsive", "wheezing", "chest_indrawing", "pulse_oximeter", "improv_spacer"};

        for (int ii = 0; ii < ids.length; ii++) {
            String[] terms = findGlossaryDefResources(ids[ii]);
            if (terms[0] != null) {
                if (!terms[0].isEmpty()) {
                    GlossaryItem glossaryItem = new GlossaryItem(terms, ids[ii], this);
                    glossaryItems.add(glossaryItem);
                }
            }
        }
    }


    /**
     * This function searches the string resources and finds the term, short and long definitions
     * @param id base glossary term id
     * @return array of term, short def and long def
     */
    public String[] findGlossaryDefResources(String id) {
        String[] vals = new String[3];
        int defsRes = getResourceIdByName(id + "_glossary_def", "array");
        if (defsRes != 0) {
            vals = getResources().getStringArray(defsRes);
        }
        return vals;
    }

    /**
     * This function will return the resource id of a given resource name and type
     *
     * @param name resource name as a string
     * @param type resource type as a string (string, drawable, layout etc.)
     * @return resource id
     */
    public int getResourceIdByName(String name, String type) {
        return getResources().getIdentifier(name, type, getPackageName());
    }

    /**
     * THis function returns a glossary item object given its id
     * @param id glossary item id
     * @return glossary item object
     */
    public GlossaryItem findGlossaryItemByID(String id) {
        for (GlossaryItem gi : glossaryItems) {
            if (gi.getId().equals(id)) {
                return gi;
            }
        }
        return null;
    }

    //Starts bronchodilator trial time
    public void setAssessedTime(String patientName, int assessedTime1) {
        trackTimes.put(patientName, assessedTime1);
    }

    //Checks if clinic resource settings have been entered
    public boolean getIsSettings_done() { return isSettings_done; }

    //Gets a patient's bronchodilator trial time (mins)
    public int getAssessedTime(String name) {

        if(trackTimes.containsKey(name)) {
            return trackTimes.get(name);
        } else {
            return -1;
        }


    }
    public boolean isPatientActive(String name) {
        return trackTimes.containsKey(name);
    }

    //calculates time left in a patient's bronchodilator trial
    public int getAssessedTimeLeft(String name) {
        if(isPatientActive(name)) {
            int min = Calendar.getInstance().getTime().getMinutes() - trackTimes.get(name);
            if(min<0){min+=60;}
            min = 10-min;
            return (min);
        } else{
            return -1;
        }

    }

    //remove patient from list of patients with active bronchodilator waiting times
    public void deleteAssessmentTracker(String patientName) {
        trackTimes.remove(patientName);
    }

    public void updateIsSettingsDone() {
        isSettings_done = Boolean.valueOf(databaseHelper.getTableItemByID("clinic", databaseHelper.CLINIC).getString(0));
    }

    //provides the list for the pop up of patients currently in bronchodilator waiting period
    public String[] getWaitingPatients() {
        if(checkTimes() && !waitingPatients.isEmpty()) {
            return waitingPatients.toArray(new String[waitingPatients.size()-1]);
        } else if(checkTimes() && !soonPatients.isEmpty()) {
            return soonPatients.toArray(new String[soonPatients.size() - 1]);
        } else {
            String[] s = new String[1];
            s[1] = "none";
            return s;
        }
    }

    //checks if any patient waiting periods are done, raises warning
    public boolean checkTimes() {
        boolean patientsWaiting = false;
        waitingPatients = new ArrayList<String>();
        soonPatients = new ArrayList<String>();
        if(trackTimes.keySet().size() == 0) {return false;}
        else {
            for(String patient : trackTimes.keySet()) {
                int mins = trackTimes.get(patient);
                int diff = getAssessedTimeLeft(patient);
                PatientSection p = patientsMap.get(patient);
                patient = p.getSurname() + " " + p.getGivenname() + " " + p.getNickname();
                if(diff <=4 && diff >0) {
                    soonPatients.add(patient);
                    patientsWaiting = true;
                } else if(diff <1) {
                    waitingPatients.add(patient);
                    patientsWaiting = true;
                }
            }
        }

            return patientsWaiting;
    }

}
