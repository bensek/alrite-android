package thundercats.com.aritediagnosisapp;

import android.content.Context;
import android.content.Intent;
import androidx.cardview.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;

/**
 * Class for making a pdf card. pdf card shows link and icon to actual pdf activity. see bronchodilator in glossary
 */

public class PDFCard extends BaseDynamicCard {
    private CardView cardView;
    private String pdf;

    public PDFCard(String pdf) {
        this.pdf = pdf;
        layoutRes = R.layout.pdf_card_layout;
    }

    @Override
    public View inflateCard(LayoutInflater layoutInflater, final Context context) {
        View view = super.inflateCard(layoutInflater, context);
        final View innerView = layoutInflater.inflate(layoutRes, null);
        cardView = view.findViewById(R.id.BaseCard);
        cardView.setClickable(true);
        cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, PDFActivity.class);
                intent.putExtra("loc", pdf);
                context.startActivity(intent);
            }
        });
        container.addView(innerView);
        return view;
    }
}
