package thundercats.com.aritediagnosisapp;

import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;

/**
 * Class for Checklist question format
 * NOTE CURRENTLY ONLY SUPPORTS 4 OPTIONS PLUS OTHER. Add more if desired
 */
public class ChecklistQuestion extends BaseQuestion {
    private CheckBox cb1, cb2, cb3, cb4, other;
    private int[] checkboxLabels;
    private View.OnClickListener listener1 = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            reasonSelected = cb1.getText().toString();
        }
    };
    private View.OnClickListener listener2 = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            reasonSelected = cb2.getText().toString();
        }
    };
    private View.OnClickListener listener3 = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            reasonSelected = cb3.getText().toString();
        }
    };
    private View.OnClickListener listener4 = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            reasonSelected = cb4.getText().toString();
        }
    };
    private View.OnClickListener otherListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {

        }
    };


    private String reasonSelected = "null";
    protected EditText editText;
    protected Button submitButton;
    private View.OnClickListener submitButtonListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if(!editText.getText().toString().isEmpty()){
                reasonSelected = editText.getText().toString();
            }
            afterClick();
        }
    };

    private LinearLayout masterLayout;
    private String nextID;
    public ChecklistQuestion(String ID, String DBColumnID, int titleTextResID, int subtitleTextResID, int[]checkBoxTexts, String nextQuestion, QuestionSection section) {
        super(ID, DBColumnID, titleTextResID, subtitleTextResID,section);
        this.nextID = nextQuestion;
        this.checkboxLabels = checkBoxTexts;
    }

    @Override
    public void setupView() {
        super.setupView();
        fillNavigationContent(R.layout.submit_layout);
        fillCardContent(R.layout.checklist_questions_layout);
        this.masterLayout = getCardContent();
        addCheckBoxes();
        setViewsandListeners();

    }

    /**
     * Set default navigation behavior: if yes then return yesID, else return noID
     * @return next dynamic item ID
     */
    @Override
    public String nextNavigation() {

        return getNextID();
    }

    public String getNextID() {
        return nextID;
    }

    public void setNextID(String nextID) {
        this.nextID = nextID;
    }


    @Override
    public String getDBValue() {
        return this.reasonSelected;
    }


    /**
     * Keeps boxes checked if returning to question
     */
    @Override
    public void renderPrevious() {
        setupView();
        //empty, no returning once submitted
    }

    private void addCheckBoxes(){
        other = new CheckBox(getHostActivity());
        cb1 = new CheckBox(getHostActivity());
        cb2 = new CheckBox(getHostActivity());
        cb3 = new CheckBox(getHostActivity());
        cb4 = new CheckBox(getHostActivity());

        cb1.setText(checkboxLabels[0]);
        masterLayout.addView(cb1);
        cb2.setText(checkboxLabels[1]);
        masterLayout.addView(cb2);
        cb3.setText(checkboxLabels[2]);
        masterLayout.addView(cb3);
        cb4.setText(checkboxLabels[3]);
        masterLayout.addView(cb4);
        other.setText(R.string.other);
        masterLayout.addView(other);
        editText = new EditText(getHostActivity());
        editText.setHint("");
        editText.setBottom(10);
        editText.setMaxLines(2);
        editText.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        masterLayout.addView(editText);
    }

    private void setViewsandListeners(){
        submitButton = getHostActivity().findViewById(R.id.SubmitButton);
        submitButton.setOnClickListener(submitButtonListener);
        cb1.setOnClickListener(listener1);
        cb2.setOnClickListener(listener2);
        cb3.setOnClickListener(listener3);
        cb4.setOnClickListener(listener4);
        other.setOnClickListener(otherListener);
    }

}
