package thundercats.com.aritediagnosisapp;

import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

/**
 * Class for number input question activity. Number of days or weight/height inputs.
 * Configurable to navigate based on preset bounds. Values > 5 go to question A, values < 5 to question B.
 * Set hint text to correct units eg. cm, lbs etc.
 */

public class NumberInputQuestion extends BaseQuestion{
    protected Button submitButton;
    protected EditText editText;
    protected TextView hintTextView;
    private double value;
    private String typedText = "";
    private double lowerBound, upperBound;
    private String belowLower, aboveUpper, inBounds, continueID;
    private int hintTextResID;
    private View.OnClickListener submitButtonListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (editText.getText().toString().isEmpty()) {
                return;
            }
            try {
                typedText = editText.getText().toString();
                setValue(Double.parseDouble(typedText));

                if(getTitleTextResID() == R.string.main_symptoms_temperature && (value>110 || value <30)) {
                    basicAlertDialog("Invalid Temperature", "Temperature value out of range, please enter a reasonable temperature");
                } else {
                    afterClick();
                }

            } catch (Exception e) {
                Log.d("GERO", "No double value given");
            }
        }
    };


    /**
     * Lots of constructors for easy one-line config. Otherwise for more fidelity just use the simplest base constructor and override any desired functions.
     */

    public NumberInputQuestion(String ID, QuestionSection section, String glossaryTerm, int titleTextResID, int subtitleTextResID, String continueID, int hintTextResID) {
        super(ID, section, glossaryTerm, titleTextResID, subtitleTextResID);
        this.continueID = continueID;
        this.hintTextResID = hintTextResID;
    }

    public NumberInputQuestion(String ID, QuestionSection section, String glossaryTerm, String DBColumnID, int titleTextResID, int subtitleTextResID, String continueID, int hintTextResID) {
        super(ID, section, glossaryTerm, DBColumnID, titleTextResID, subtitleTextResID);
        this.continueID = continueID;
        this.hintTextResID = hintTextResID;
    }

    public NumberInputQuestion(String ID, QuestionSection section, String glossaryTerm, int titleTextResID, int subtitleTextResID, double lowerBound, double upperBound, String belowLower, String aboveUpper, String inBounds, int hintTextResID) {

        super(ID, section, glossaryTerm, titleTextResID, subtitleTextResID);
        this.lowerBound = lowerBound;
        this.upperBound = upperBound;
        this.belowLower = belowLower;
        this.aboveUpper = aboveUpper;
        this.inBounds = inBounds;
        this.hintTextResID = hintTextResID;
    }

    public NumberInputQuestion(String ID, QuestionSection section, String glossaryTerm, String DBColumnID, int titleTextResID, int subtitleTextResID, double lowerBound, double upperBound, String belowLower, String aboveUpper, String inBounds, int hintTextResID) {
        super(ID, section, glossaryTerm, DBColumnID, titleTextResID, subtitleTextResID);
        this.lowerBound = lowerBound;
        this.upperBound = upperBound;
        this.belowLower = belowLower;
        this.aboveUpper = aboveUpper;
        this.inBounds = inBounds;
        this.hintTextResID = hintTextResID;
    }

    public NumberInputQuestion(String ID, int titleTextResID, int subtitleTextResID, int hintTextResID, String continueID, QuestionSection section) {
        super(ID, titleTextResID, subtitleTextResID, section);
        this.continueID = continueID;
        this.hintTextResID = hintTextResID;
    }

    public NumberInputQuestion(String ID, String DBColumnID, int titleTextResID, int subtitleTextResID, int hintTextResID, String continueID,QuestionSection section) {
        super(ID, DBColumnID, titleTextResID, subtitleTextResID, section);
        this.continueID = continueID;
        this.hintTextResID = hintTextResID;
    }

    public NumberInputQuestion(String ID, int titleTextResID, int subtitleTextResID, int hintTextResID, double lowerBound, double upperBound, String belowLower, String aboveUpper, String inBounds, QuestionSection section) {
        super(ID, titleTextResID, subtitleTextResID, section);
        this.lowerBound = lowerBound;
        this.upperBound = upperBound;
        this.belowLower = belowLower;
        this.aboveUpper = aboveUpper;
        this.inBounds = inBounds;
        this.hintTextResID = hintTextResID;
    }

    public NumberInputQuestion(String ID, String DBColumnID, int titleTextResID, int subtitleTextResID,int hintTextResID, double lowerBound, double upperBound, String belowLower, String aboveUpper, String inBounds, QuestionSection section) {
        super(ID, DBColumnID, titleTextResID, subtitleTextResID, section);
        this.lowerBound = lowerBound;
        this.upperBound = upperBound;
        this.belowLower = belowLower;
        this.aboveUpper = aboveUpper;
        this.inBounds = inBounds;
        this.hintTextResID = hintTextResID;
    }

    public NumberInputQuestion(String ID, String DBColumnID, int titleTextResID, int subtitleTextResID, int hintTextResID, QuestionSection section) {
        super(ID, DBColumnID, titleTextResID, subtitleTextResID,section);
        this.hintTextResID = hintTextResID;
    }


    @Override
    public void setupView() {
        super.setupView();
        fillNavigationContent(R.layout.submit_layout);
        fillCardContent(R.layout.number_input_question_layout_full);
        linkSubmitButtonView();
        linkEditTextView();
        linkHintTextView();
        setListeners();
        updateHintText();
    }

    @Override
    public String getDBValue() {
        return getValue()+"";
    }

    /**
     * Important navigation function. THis determines how the survey branching happens.
     * @return String representing next question or blank for nothing
     */
    @Override
    public String nextNavigation() {
        if(continueID!=null) {
            if (!continueID.isEmpty()) {
                return continueID;
            }
        }
        if(isBelowLower()){
            return belowLower;
        }
        if(isAboveUpper()){
            return aboveUpper;
        }
        return inBounds;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public void linkSubmitButtonView(){
        submitButton = getHostActivity().findViewById(R.id.SubmitButton);
    }

    public void linkEditTextView(){
        editText = getHostActivity().findViewById(R.id.EditText);
    }

    public void linkHintTextView(){
        hintTextView = getHostActivity().findViewById(R.id.Hint);
    }

    public void setListeners(){
        submitButton.setOnClickListener(submitButtonListener);
    }

    public void setSubmitButtonListener(View.OnClickListener submitButtonListener) {
        this.submitButtonListener = submitButtonListener;
        setListeners();
    }

    public int getHintTextResID() {
        return hintTextResID;
    }

    public void setHintTextResID(int hintTextResID) {
        this.hintTextResID = hintTextResID;
    }

    public void updateHintText(){
        hintTextView.setText(getHintTextResID());
    }

    public double getLowerBound() {
        return lowerBound;
    }

    public void setLowerBound(double lowerBound) {
        this.lowerBound = lowerBound;
    }

    public double getUpperBound() {
        return upperBound;
    }

    public void setUpperBound(double upperBound) {
        this.upperBound = upperBound;
    }

    public boolean isBelowLower(){
        return value<lowerBound;
    }

    public boolean isAboveUpper(){
        return value>upperBound;
    }

    @Override
    public void reset() {
        super.reset();
        value = -1;
        typedText = "";
        if (editText != null) {
            editText.setText("");
        }
    }

    /**
     * renders previous answer in textbox when question is revisited
     */
    @Override
    public void renderPrevious() {
        editText.setText(typedText);
    }
}
