package thundercats.com.aritediagnosisapp;

import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

/**
 * Text input question - used for patient name
 */

public class TextQuestion extends BaseQuestion {
    private String value,nextID;
    private Button submitButton;
    private EditText editText;
    private View.OnClickListener submitButtonListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (editText.getText().toString().isEmpty()) {
                return;
            }
            try {
                setValue(editText.getText().toString());
                afterClick();
            } catch (Exception e) {
                Log.d("GERO", "No string value given");
            }
        }
    };

    public TextQuestion(String ID, QuestionSection section, String glossaryTerm, int titleTextResID, int subtitleTextResID, String nextID) {
        super(ID, section, glossaryTerm, titleTextResID, subtitleTextResID);
        this.nextID = nextID;
    }

    public TextQuestion(String ID, QuestionSection section, String glossaryTerm, String DBColumnID, int titleTextResID, int subtitleTextResID, String nextID) {
        super(ID, section, glossaryTerm, DBColumnID, titleTextResID, subtitleTextResID);
        this.nextID = nextID;
    }

    public TextQuestion(String ID, int titleTextResID, int subtitleTextResID, String nextID, QuestionSection section) {
        super(ID, titleTextResID, subtitleTextResID, section);
        this.nextID = nextID;
    }

    public TextQuestion(String ID, String DBColumnID, int titleTextResID, int subtitleTextResID, String nextID,QuestionSection section) {
        super(ID, DBColumnID, titleTextResID, subtitleTextResID, section);
        this.nextID = nextID;
    }

    @Override
    public void setupView() {
        super.setupView();
        fillNavigationContent(R.layout.submit_layout);
        fillCardContent(R.layout.text_input_layout_full);
        linkSubmitButtonView();
        linkEditTextView();
        setListeners();
    }

    @Override
    public String getDBValue() {
        return getValue();
    }

    @Override
    public String nextNavigation() {
        return nextID;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public void linkSubmitButtonView(){
        submitButton = getHostActivity().findViewById(R.id.SubmitButton);
    }

    public void linkEditTextView(){
        editText = getHostActivity().findViewById(R.id.EditText);
    }

    public void setListeners(){
        submitButton.setOnClickListener(submitButtonListener);
    }

    public void setSubmitButtonListener(View.OnClickListener submitButtonListener) {
        this.submitButtonListener = submitButtonListener;
        setListeners();
    }

    @Override
    public void reset() {
        super.reset();
        value = "";
        if (editText != null) {
            editText.setText("");
        }
    }

    @Override
    public void renderPrevious() {
        editText.setText(value);
    }
}
