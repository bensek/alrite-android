package thundercats.com.aritediagnosisapp;

import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;

/**
 * Radio button question type. Used for gender question currently. Options should be in a string-array in the strings.xml file
 */

public class RadioQuestion extends BaseQuestion{
    private int value;
    private int optionsStringArrayRes;
    private String[] options,nextQuestionIDs;
    private RadioGroup radioGroup;
    private Button submitButton;
    private View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            int radioButtonID = radioGroup.getCheckedRadioButtonId();
            View radioButton = radioGroup.findViewById(radioButtonID);
            int idx = radioGroup.indexOfChild(radioButton);
            if (idx > -1 && idx < options.length) {
                setValue(idx);
                afterClick();
            }
        }
    };


    public RadioQuestion(String ID, String DBColumnID, String glossaryTerm, int titleTextResID, int subtitleTextResID, int optionsStringArrayRes, String nextQuestionID, QuestionSection section) {
        super(ID, section, glossaryTerm, DBColumnID, titleTextResID, subtitleTextResID);
        this.nextQuestionIDs = new String[]{nextQuestionID};
        this.optionsStringArrayRes = optionsStringArrayRes;
    }


    public RadioQuestion(String ID, QuestionSection section, String glossaryTerm, String DBColumnID, int titleTextResID, int subtitleTextResID, int optionsStringArrayRes) {
        super(ID, section, glossaryTerm, DBColumnID, titleTextResID, subtitleTextResID);
        this.optionsStringArrayRes = optionsStringArrayRes;
    }

    public RadioQuestion(String ID, String DBColumnID, int titleTextResID, int subtitleTextResID, int optionsStringArrayRes, QuestionSection section) {
        super(ID, DBColumnID, titleTextResID, subtitleTextResID,section);
        this.optionsStringArrayRes = optionsStringArrayRes;
    }

    public RadioQuestion(String ID, String DBColumnID, int titleTextResID, int subtitleTextResID, int optionsStringArrayRes, String nextQuestionID,QuestionSection section) {
        super(ID, DBColumnID, titleTextResID, subtitleTextResID,section);
        this.nextQuestionIDs = new String[]{nextQuestionID};
        this.optionsStringArrayRes = optionsStringArrayRes;
    }

    public RadioQuestion(String ID, String DBColumnID, int titleTextResID, int subtitleTextResID, int optionsStringArrayRes, String[] nextQuestionIDs,QuestionSection section) {
        super(ID, DBColumnID, titleTextResID, subtitleTextResID,section);
        this.optionsStringArrayRes = optionsStringArrayRes;
        this.nextQuestionIDs = nextQuestionIDs;
    }

    /**
     * Sets up question view and links buttons with listeners
     */
    @Override
    public void setupView() {
        super.setupView();
        fillCardContent(R.layout.radio_question_layout_full);
        fillNavigationContent(R.layout.submit_layout);
        optionsArrayResToStringArray();
        linkRadioGroupView();
        addOptions();
        linkSubmitButtonView();
        setSubmitButtonListener();
    }

    /**
     * converts string-array in strings.xml to string array
     */
    public void optionsArrayResToStringArray(){
        options = getHostActivity().getResources().getStringArray(optionsStringArrayRes);
    }

    public String[] getNextQuestionIDs() {
        return nextQuestionIDs;
    }

    public void setNextQuestionIDs(String[] nextQuestionIDs) {
        this.nextQuestionIDs = nextQuestionIDs;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public int getOptionsStringArrayRes() {
        return optionsStringArrayRes;
    }

    public void setOptionsStringArrayRes(int optionsStringArrayRes) {
        this.optionsStringArrayRes = optionsStringArrayRes;
    }

    public String[] getOptions() {
        return options;
    }

    public void setOptions(String[] options) {
        this.options = options;
    }

    public View.OnClickListener getListener() {
        return listener;
    }

    public void setListener(View.OnClickListener listener) {
        this.listener = listener;
    }

    public RadioGroup getRadioGroup() {
        return radioGroup;
    }

    public void setRadioGroup(RadioGroup radioGroup) {
        this.radioGroup = radioGroup;
    }

    public void linkSubmitButtonView() {
        submitButton = getHostActivity().findViewById(R.id.SubmitButton);
    }

    public void linkRadioGroupView(){
        radioGroup = getHostActivity().findViewById(R.id.RadioGroup);
    }

    /**
     * Adds radio buttons to view given populated options array
     */
    public void addOptions(){
        for(int ii = 0; ii< options.length; ii++){
            RadioButton r = new RadioButton(getHostActivity());
            r.setId(ii);
            r.setText(options[ii]);
            r.setTextAppearance(getHostActivity(),R.style.radioButtonStyle);
            radioGroup.addView(r);
        }
    }

    /**
     * navigation based on individual responses or to same location.
     * For individual options to link to different questions pass in string array where index corresponds to option index.
     * option 1 -> question A
     * option 2 -> question B
     * option 3 -> question C
     * ['A','B','C']
     * <p>
     * for all options -> same next question, pass array of length 1 with next question ID
     *
     * @return next question ID
     */
    @Override
    public String  nextNavigation() {
        if(value>-1){
            if(nextQuestionIDs!=null) {
                if (value < nextQuestionIDs.length) {
                    return nextQuestionIDs[getValue()];
                } else if(nextQuestionIDs.length==1){
                    return nextQuestionIDs[0];
                }
            }
        }
        return "home";
    }

    public void setSubmitButtonListener(){
        submitButton.setOnClickListener(listener);
    }

    @Override
    public String getDBValue() {
        return value+"";
    }

    @Override
    public void reset() {
        super.reset();
        value = -1;
        radioGroup.clearCheck();
    }

    /**
     * Shows previously answered option as selected
     */
    @Override
    public void renderPrevious() {
        radioGroup.check(value);
    }
}
