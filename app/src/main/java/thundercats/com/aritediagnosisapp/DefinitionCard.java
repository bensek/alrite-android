package thundercats.com.aritediagnosisapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;


public class DefinitionCard extends BaseDynamicCard {
    private TextView definitionView;
    private String definition;

    public DefinitionCard(String definition) {
        this.definition = definition;
        layoutRes = R.layout.definition_card_layout;
    }

    /**
     * Inflates defintition card and returns new view
     *
     * @param layoutInflater layout inflator
     * @param context        activity context
     * @return card view
     */
    @Override
    public View inflateCard(LayoutInflater layoutInflater, Context context) {
        View view = super.inflateCard(layoutInflater, context);
        View innerView = layoutInflater.inflate(layoutRes, null);
        definitionView = innerView.findViewById(R.id.Subtitle);
        definitionView.setText(definition);
        container.addView(innerView);
        return view;
    }
}
