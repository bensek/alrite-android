package thundercats.com.aritediagnosisapp;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.kolastudios.KSUtils;
import com.pixplicity.easyprefs.library.Prefs;

import java.io.IOException;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import thundercats.com.aritediagnosisapp.activities.MainActivity;
import thundercats.com.aritediagnosisapp.models.Assessment;
import thundercats.com.aritediagnosisapp.utils.APIInterface;
import thundercats.com.aritediagnosisapp.utils.AccountManager;
import thundercats.com.aritediagnosisapp.utils.RetrofitClient;
import thundercats.com.aritediagnosisapp.utils.Utils;

/**
 * Class for diagnosis popup item
 */

public class DynamicDiagnosis {
    public ArrayList<BaseDynamicCard> cards;
    public ArrayList<Integer> mainSteps;
    public NumberedStepsCard mainCard;
    public View navigationLayout;
    public boolean showRespScore, showFinal;
    public int score1, score2;
    private int titleRes;
    private Severity severity;
    private Dialog dialog;


    public DynamicDiagnosis(int titleRes, Severity severity, boolean showrespScore) {
        this.titleRes = titleRes;
        this.severity = severity;
        this.showRespScore = showrespScore;
        cards = new ArrayList<>();
        mainSteps = new ArrayList<>();
        NumberedStepsCard c = new NumberedStepsCard(new String[]{});
        cards.add(c);
        mainCard = c;
        this.showFinal = false;
    }

    /**
     * Function to create and show the popup over current activity.
     *
     * @param context
     */
    public void showPopup(Activity context) {
        GlobalClass globalClass = (GlobalClass) context.getApplication();
        String[] stepsText = new String[mainSteps.size()];
        for (int ii = 0; ii < stepsText.length; ii++) {
            stepsText[ii] = context.getResources().getString(mainSteps.get(ii));
        }
        mainCard.setSteps(stepsText);

        dialog = new Dialog(context);
        LayoutInflater inflater = context.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dynamic_card_popup_layout, null);

        String diag = globalClass.getResources().getString(R.string.diagnosis) + ": ";
        diag += globalClass.getResources().getString(titleRes);
        ((TextView) dialogView.findViewById(R.id.Title)).setText(diag);
        dialogView.findViewById(R.id.TopBar).setBackgroundColor(context.getResources().getColor(severity.getColor()));

        LinearLayout dynNav = dialogView.findViewById(R.id.DynamicNavigation);
        dynNav.removeAllViews();
        if (navigationLayout.getParent() != null) {
            ((ViewGroup) navigationLayout.getParent()).removeView(navigationLayout);
        }
        dynNav.addView(navigationLayout);
        (dialogView.findViewById(R.id.respScoreInfo)).setVisibility(View.GONE);
        (dialogView.findViewById(R.id.respScoreInfo2)).setVisibility(View.GONE);
        /*if (showRespScore) {
            String respScore = "Respiratory Score = " + score1 + " out of 9";
            String treatment = "Scroll down for full treatment and follow up information";
            ((TextView) dialogView.findViewById(R.id.respScoreInfo)).setText(respScore);
            ((TextView) dialogView.findViewById(R.id.respScoreInfo2)).setText(treatment);
            (dialogView.findViewById(R.id.respScoreInfo)).setVisibility(View.VISIBLE);
            (dialogView.findViewById(R.id.respScoreInfo2)).setVisibility(View.VISIBLE);
        }

        if (showFinal) {
            String respScore = "Respiratory Score went from " + score1 + " to " + score2 + " out of 9";
            String treatment = "Scroll down for full treatment and follow up information";
            ((TextView) dialogView.findViewById(R.id.respScoreInfo)).setText(respScore);
            ((TextView) dialogView.findViewById(R.id.respScoreInfo2)).setText(treatment);
            (dialogView.findViewById(R.id.respScoreInfo)).setVisibility(View.VISIBLE);
            (dialogView.findViewById(R.id.respScoreInfo2)).setVisibility(View.VISIBLE);
        }*/

        inflateAll(inflater, (LinearLayout) dialogView.findViewById(R.id.MainContainer));

        ScrollView scrollView = dialogView.findViewById(R.id.ScrollView);
        scrollView.smoothScrollTo(0, 0);

        dialog.setCancelable(true);
        dialog.setContentView(dialogView);
        dialog.show();
    }

    /**
     * Inflates all cards linked to diagnosis popup
     * @param inflater
     * @param container
     */
    public void inflateAll(LayoutInflater inflater, LinearLayout container) {
        for (BaseDynamicCard bc : cards) {
            container.addView(bc.inflateCard(inflater, container.getContext()));
        }
    }

    /**
     * Converts array of step ids to steps
     * @param ids array of step resource ids
     * @param globalClass global application class instance
     */
    public void stepsFromIDs(int[] ids, GlobalClass globalClass) {
        mainSteps.clear();
        for (int i : ids) {
            mainSteps.add(i);
        }
    }

    public View getNavigationLayout() {
        return navigationLayout;
    }

    public void setNavigationLayout(View navigationLayout) {
        this.navigationLayout = navigationLayout;
    }

    /**
     * Sets the diagnosis navigation type to a single dismiss button - no navigation possible.
     * @param context activity
     */
    public void setDismissableLayout(Activity context) {
        LayoutInflater inflater = context.getLayoutInflater();
        View view = inflater.inflate(R.layout.exit_only_navigation, null);
        Button dismiss = view.findViewById(R.id.ContinueButton);
        dismiss.setText(R.string.dismiss);
        dismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();
            }
        });
        navigationLayout = view;
    }

    /**
     * Sets the diagnosis navigation type to a single save and exit button - no continuation possible.
     * @param context activity
     */
    public void setExitOnlyLayout(final Activity context) {
        LayoutInflater inflater = context.getLayoutInflater();
        View view = inflater.inflate(R.layout.exit_only_navigation, null);
        Button dismiss = view.findViewById(R.id.ContinueButton);
        dismiss.setText(R.string.save_and_exit_text);
        dismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                if(Prefs.getString(AccountManager.TOKEN, "").equals("") || !Utils.isNetworkConnected(context)){
                    int patient_id = Prefs.getInt(AccountManager.CURRENT_PATIENT, 0);
                    String title = context.getResources().getString(titleRes);
                    Assessment assessment = new Assessment();

                    assessment.patient_id = patient_id;
                    assessment.title = title;
                    assessment.local_date = Utils.currentDate();
                    assessment.save();

                    ((GlobalClass) context.getApplication()).currentPatient.saveAndRemove();
                    Intent intent = new Intent(context, MainActivity.class);
                    context.startActivity(intent);
                }

                if(!Prefs.getString(AccountManager.TOKEN, "").equals("") && Utils.isNetworkConnected(context)){
                    createAssessment(context);
                }
            }
        });
        navigationLayout = view;
    }

    private void createAssessment(Activity activity){
        ProgressDialog dialog = new ProgressDialog(activity);
        dialog.setMessage("Please wait...");
        dialog.setTitle("Saving Assessment");
        dialog.setCancelable(true);

        dialog.show();

        int patient_id = Prefs.getInt(AccountManager.CURRENT_PATIENT, 0);
        String title = activity.getResources().getString(titleRes);

        Call<Assessment> call = RetrofitClient.getInstance().create(APIInterface.class).createAssessment(patient_id, title);
        call.enqueue(new Callback<Assessment>() {
            @Override
            public void onResponse(Call<Assessment> call, Response<Assessment> response) {
                if(response.isSuccessful()){
                    dialog.dismiss();
                    Toast.makeText(activity, "Assessment Saved", Toast.LENGTH_LONG).show();
                    ((GlobalClass) activity.getApplication()).currentPatient.saveAndRemove();
                    Intent intent = new Intent(activity, MainActivity.class);
                    activity.startActivity(intent);
                }else{
                    dialog.dismiss();
                    Utils.errorDialog(activity, "An error occurred. Please try again later.");
                    try {
                        KSUtils.logE("Save Assessment OnError -> "+ response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<Assessment> call, Throwable t) {
                dialog.dismiss();
                if(t instanceof IOException){
                    Utils.errorDialog(activity, "Please check your internet settings to ensure the device has network connection.");

                }else{
                    Utils.errorDialog(activity, "An error occurred. Please try again later.");

                }

                KSUtils.logE("Save Assessment OnFailure -> "+ t.getLocalizedMessage());
            }
        });

    }

    /**
     * Sets the diagnosis navigation type to a single continue button - no exit possible.
     * @param context activity
     * @param continueID next item to navigate to
     */
    public void setContinueLayout(final Activity context, final String continueID) {
        LayoutInflater inflater = context.getLayoutInflater();
        View view = inflater.inflate(R.layout.exit_only_navigation, null);
        Button dismiss = view.findViewById(R.id.ContinueButton);
        dismiss.setText(R.string.continue_text);
        dismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                ((GlobalClass) context.getApplication()).currentPatient.pneumoniaSection.getCurrentQuestion().toNext(continueID);
            }
        });
        navigationLayout = view;
    }

    /**
     * Sets the diagnosis navigation type to two options, save and exit, continue.
     * @param context activity
     * @param continueID next item to navigate to if continue is clicked
     */
    public void setSaveOrContinueLayout(final Activity context, final String continueID) {
        LayoutInflater inflater = context.getLayoutInflater();
        View view = inflater.inflate(R.layout.exitable_navigation, null);
        Button save = view.findViewById(R.id.SaveAndExit);
        save.setText(R.string.save_and_exit_text);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                ((GlobalClass) context.getApplication()).currentPatient.saveAndRemove();
                Intent intent = new Intent(context, MainActivity.class);
                context.startActivity(intent);
            }
        });

        Button cont = view.findViewById(R.id.ContinueButton);
        cont.setText(R.string.continue_text);
        cont.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                ((GlobalClass) context.getApplication()).currentPatient.pneumoniaSection.getCurrentQuestion().toNext(continueID);
            }
        });
        navigationLayout = view;
    }

    /**
     * Add step to diagnosis check for duplicates
     * @param id string resource id
     */
    public void safeAddStep(int id) {
        if (!mainSteps.contains(id)) {
            mainSteps.add(id);
        }
    }

    public void safeReplaceStep(int id, int replacementid, int index) {
        if (index>mainSteps.size() || index<0) {
            mainSteps.add(id);
        } else {
            mainSteps.add(index, id);
            for (int ii = mainSteps.size() - 1; ii >= 0; ii--) {
                if (mainSteps.get(ii) == (replacementid)) {
                    mainSteps.remove(ii);
                }
            }
        }
    }

    public void removeDuplicates(int id, int startIndex) {
        for (int ii = startIndex; ii <mainSteps.size(); ii++) {
            if (mainSteps.get(ii) == id) {
                mainSteps.remove(ii);
            }
        }
    }

    public void safeInsertStep(int id, int index) {
        if (index>mainSteps.size() || index<0) {
            mainSteps.add(id);
        } else {
            mainSteps.add(index, id);
        }
    }

    /**
     * Remove step from diagnosis
     * @param id resource id
     */
    public void removeSafeStep(int id) {
        if (mainSteps != null) {
            for (int ii = mainSteps.size() - 1; ii >= 0; ii--) {
                if (mainSteps.get(ii) == id) {
                    mainSteps.remove(ii);
                }
            }
        }
    }

    public int getTitleRes() {
        return titleRes;
    }

    public void setTitleRes(int titleRes) {
        this.titleRes = titleRes;
    }
    public void setShowFinal(boolean showFinal1) {this.showFinal = showFinal1;}


    /**
     * Diagnosis severity
     */
    public enum Severity {
        High(R.color.severeDiagnosisColor),
        Moderate(R.color.moderateDiagnosisColor),
        Mild(R.color.mildDiagnosisColor),
        None(R.color.primaryButtonColor);

        private final int color;

        Severity(int color) {
            this.color = color;
        }

        public int getColor() {
            return color;
        }
    }
}
