package thundercats.com.aritediagnosisapp;

import android.app.Dialog;
import android.os.CountDownTimer;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.concurrent.TimeUnit;

/**
 * Respiratory rate calculator question
 * Improved algorithm for RR calculation.
 * If at least 5 measured breaths are within 13% margin of error, respiratory rate can be extrapolated to one minute.
 * Else measure for a minute.
 *
 * Measures duration in between taps and uses median breath duration to calculate breath rate.
 */

public class RespiratoryRate extends BaseQuestion{
    private ConstraintLayout circle;
    private Button resetButton, manualInput;
    private TextView elapsedTime;
    private ArrayList<Long> durations;
    private long lastBreath;
    private double value;
    private int numBreaths, margin, score, birthday;
    private String ifFastBreathing,ifNormalBreathing;
    private boolean fastBreathing;
    private CountDownTimer timer;
    private View.OnClickListener resetButtonListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            resetRespRate();
        }
    };
    private View.OnClickListener circleListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            view.startAnimation(AnimationUtils.loadAnimation(getHostActivity(), R.anim.circle_animation));
            ((TextView) view.findViewById(R.id.TapOnInhale)).setText(R.string.tap_on_inhale);
            breathTaken();
            if (validateDataCollection()) {
                value = getBreathRate(numBreaths);
                completeMeasuring();
            }
        }
    };

    public RespiratoryRate(String ID, String DBColumnID, String ifFastBreathing,String ifNormalBreathing,QuestionSection section) {
        super(ID, DBColumnID, R.string.respiratory_rate_title, R.string.respiratory_rate_subtitle, section);
        this.ifFastBreathing = ifFastBreathing;
        this.ifNormalBreathing = ifNormalBreathing;
        numBreaths = 5;
        margin=13;
        score = 0;
        durations = new ArrayList<>();
        newTimer();
        lastBreath=-1;
    }

    /**
     * fully custom question so base question setup method must be overwritten.
     * Toolbar setup, layout setup and button listener linking.
     */
    @Override
    public void setupView() {
        getHostActivity().setContentView(R.layout.respiratory_rate_layout_full);
        setMyToolbar((Toolbar) getHostActivity().findViewById(R.id.my_toolbar));
        ((AppCompatActivity)getHostActivity()).setSupportActionBar(getMyToolbar());
        ((AppCompatActivity) getHostActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((AppCompatActivity) getHostActivity()).getSupportActionBar().setDisplayShowHomeEnabled(true);
        setTitleTextView((TextView)getHostActivity().findViewById(R.id.Title));
        setSubtitleTextView((TextView)getHostActivity().findViewById(R.id.Subtitle));
        this.setScreenTimeStamp(System.currentTimeMillis());
        birthday = ((GlobalClass) getHostActivity().getApplication()).currentPatient.getBirthday();
        updateTitleText();
        updateSubtitleText();
        linkCircleView();
        linkResetButtonView();
        linkManualButtonView();
        linkElapsedTimeView();
        updateElapsedTimeView(0);
        linkResetButtonListener();
        linkManualButtonListener();
        linkCircleListener();
        if(isCompleted()){
            showDialog();
        }
        resetRespRate();
    }

    /**
     * Creates new timer to break off after one minute in worst case where taps are too inconsistent.
     */
    public void newTimer(){
        timer = new CountDownTimer(60*1000,1000) {
            @Override
            public void onTick(long l) {
                updateElapsedTimeView((60*1000)-l);
            }

            @Override
            public void onFinish() {
                if(durations.size()<5){
                    resetRespRate();
                } else {
                    value = getBreathRate(durations.size());
                    completeMeasuring();
                }
            }
        };
    }

    /**
     * method decides whether child is experiencing fast breathing based on RR and age.
     */
    public void evalFastBreathing(){
        if(isCompleted()) {
            //Date birthday = ((GlobalClass) getHostActivity().getApplication()).currentPatient.getBirthday();

            if (birthday>-1) {
                if (birthday <2) {
                    if(value>=50 && value<60){
                        fastBreathing = true;
                        score = 2;
                        return;
                    }

                    if (value >=60) {
                        fastBreathing = true;
                        score = 3;
                        return;
                    }
                }
                else {
                    if (value >= 40 && value <45) {
                        fastBreathing = true;
                        score = 2;
                        return;
                    }
                    if (value >= 45) {
                        fastBreathing = true;
                        score = 3;
                        return;
                    }
                }

                fastBreathing = false;
            } else {
                //TODO
            }
        } else {
            //TODO
        }
    }

    /**
     * shows popup with alert explaining RR and if child is experiencing fast breathing
     */
    public void showDialog(){
        final Dialog dialog = new Dialog(getHostActivity());
        dialog.requestWindowFeature(getHostActivity().getWindow().FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.respiratory_rate_result_dialog_layout);

        TextView respRateNum = dialog.findViewById(R.id.RespRateNum);
        TextView condition = dialog.findViewById(R.id.FastBreathing);
        TextView breathInfo = dialog.findViewById(R.id.breathingInfo);

        NumberFormat formatter = new DecimalFormat("#0");
        respRateNum.setText(formatter.format(value));

        if(fastBreathing){
            condition.setText(R.string.fast_breathing);
            condition.setTextColor(getHostActivity().getResources().getColor(R.color.red));
        } else {
            condition.setText(R.string.normal_breathing);
            condition.setTextColor(getHostActivity().getResources().getColor(R.color.primaryButtonColor));
        }
        if (birthday<2) {
            breathInfo.setText(R.string.breathing_info_under1);
        } else {
            breathInfo.setText(R.string.breathing_info_over1);
        }

        Button dialogButtonReset = dialog.findViewById(R.id.ResetButton);
        // if button is clicked, close the custom dialog
        dialogButtonReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                resetRespRate();
            }
        });

        Button dialogButtonContinue = dialog.findViewById(R.id.ContinueButton);
        // if button is clicked, close the custom dialog
        dialogButtonContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                afterClick();
            }
        });

        dialog.show();
    }

    public void linkManualButtonView() {
        manualInput = getHostActivity().findViewById(R.id.ManualInput);
    }

    /**
     * Sets the listener for when user clicks on manual input option
     */
    public void linkManualButtonListener() {
        manualInput.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Dialog dialog = new Dialog(getHostActivity());
                dialog.requestWindowFeature(getHostActivity().getWindow().FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.manual_input_resp_rate);

                final EditText editText = dialog.findViewById(R.id.EditText);
                Button submitButton = dialog.findViewById(R.id.SubmitButton);

                submitButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (editText.getText().toString().isEmpty()) {
                            return;
                        }
                        String typedText = editText.getText().toString();
                        value = Double.parseDouble(typedText);
                        dialog.dismiss();
                        setCompleted(true);
                        evalFastBreathing();
                        showDialog();
                    }
                });
                dialog.show();
            }
        });
    }

    /**
     * handles logic for when a breath is measured
     */
    public void breathTaken() {
        long currTime = System.currentTimeMillis();
        if (lastBreath != -1) {
            long dur = currTime - lastBreath;
            durations.add(dur);
        } else{
            startTimer();
        }

        lastBreath = currTime;
    }

    /**
     * calculates number of consistent breaths in trailing window.
     *
     * @return number of consistent breaths
     */
    public int getValidProgress() {
        for (int ii = 1; ii <= numBreaths; ii++) {
            if (ii > durations.size()) {
                return ii - 1;
            }
            long median = getMedian(ii);
            if (median == -1) {
                return ii - 1;
            }
            long up = upperBound(median);
            long low = lowerBound(median);

            for (int jj = durations.size() - 1; jj > ((durations.size() - 1) - ii); jj--) {
                if (!inBounds(durations.get(jj), up, low)) {
                    return ii - 1;
                }
            }
        }
        return numBreaths;
    }

    /**
     * Method to determine if number of consistent breaths matches or exceeds threshold
     * @return
     */
    public boolean validateDataCollection() {
        return getValidProgress() >= numBreaths;
    }

    public double getBreathRate(int num) {
        return 60 / (getMedian(num) / 1000.0);
    }

    public long getMedian(int length) {
        if (length > durations.size()) {
            return -1;
        }
        if (length == 0) {
            return -1;
        }

        ArrayList<Long> sub = new ArrayList<Long>(durations.subList(durations.size() - (length), durations.size()));
        Collections.sort(sub);
        int half = (length / 2);
        if (length % 2 == 0) {
            return (sub.get(half - 1) + sub.get(half)) / 2;
        }
        return sub.get(half);
    }

    /**
     * Calculates the lower bound of what is considered a consistent breath given the margin
     * @param med median
     * @return lower bound value
     */
    public long lowerBound(long med) {
        return (long) (med * (1.0 - (margin / 100.0)));
    }

    /**
     * Calculates the upper bound of what is considered a consistent breath given the margin
     * @param med median
     * @return upper bound value
     */
    public long upperBound(long med) {
        return (long) (med * (1.0 + (margin / 100.0)));
    }

    /**
     * determines if value is in bounds
     * @param val value
     * @param up upper bound
     * @param low lower bound
     * @return is in bounds
     */
    public boolean inBounds(long val, long up, long low) {
        return (val < up) && (val > low);
    }

    @Override
    public String getDBValue() {
        return value + "";
    }

    @Override
    public String nextNavigation() {
        return fastBreathing?ifFastBreathing:ifNormalBreathing;
    }

    /**
     * links 'tap here' button
     */
    public void linkCircleView(){
        setCircle((ConstraintLayout) getHostActivity().findViewById(R.id.Circle));
    }

    public void linkResetButtonView(){
        setResetButton((Button) getHostActivity().findViewById(R.id.ResetButton));
    }

    public void linkElapsedTimeView(){
        setElapsedTimeView((TextView) getHostActivity().findViewById(R.id.ElapsedTime));
    }

    public void updateElapsedTimeView(long millis){
        String b = getHostActivity().getResources().getString(R.string.elapsed_time);
        elapsedTime.setText(b+" "+millisecondsToString(millis));
    }

    public ConstraintLayout getCircle() {
        return circle;
    }

    public void setCircle(ConstraintLayout circle) {
        this.circle = circle;
    }

    public Button getResetButton() {
        return resetButton;
    }

    public void setResetButton(Button resetButton) {
        this.resetButton = resetButton;
    }

    public TextView getElapsedTimeView() {
        return elapsedTime;
    }

    public void setElapsedTimeView(TextView elapsedTime) {
        this.elapsedTime = elapsedTime;
    }

    public void linkResetButtonListener(){
        resetButton.setOnClickListener(resetButtonListener);
    }

    public void linkCircleListener(){
        circle.setOnClickListener(circleListener);
    }

    public void resetRespRate(){
        ((TextView) circle.findViewById(R.id.TapOnInhale)).setText(R.string.start_text);
        durations.clear();
        lastBreath = -1;
        resetTimer();
        updateElapsedTimeView(0);
    }

    public void completeMeasuring(){
        setCompleted(true);
        resetTimer();
        evalFastBreathing();
        showDialog();
    }

    public boolean isFastBreathing() {
        return fastBreathing;
    }

    public void setFastBreathing(boolean fastBreathing) {
        this.fastBreathing = fastBreathing;
    }

    public void resetTimer(){
        timer.cancel();
        newTimer();
    }

    public void startTimer(){
        timer.start();
    }

    public String millisecondsToString(long millis){
        String  ms = (TimeUnit.MILLISECONDS.toMinutes(millis) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)))+
                ":"+ (TimeUnit.MILLISECONDS.toSeconds(millis) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
        return ms;
    }

    @Override
    public void reset() {
        super.reset();
        resetRespRate();
    }

    @Override
    public void renderPrevious() {

    }

    public double getValue() {
        return value;
    }

    public String getIfFastBreathing() {
        return ifFastBreathing;
    }

    public void setIfFastBreathing(String ifFastBreathing) {
        this.ifFastBreathing = ifFastBreathing;
    }

    public String getIfNormalBreathing() {
        return ifNormalBreathing;
    }

    public void setIfNormalBreathing(String ifNormalBreathing) {
        this.ifNormalBreathing = ifNormalBreathing;
    }
    public int getScore() {
        return score;
    }

}
