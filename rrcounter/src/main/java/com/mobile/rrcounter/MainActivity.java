package com.mobile.rrcounter;

import android.os.Bundle;
import android.os.CountDownTimer;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.Collections;
import java.util.concurrent.TimeUnit;

public class MainActivity extends AppCompatActivity {

    private ConstraintLayout circle;
    private Button resetButton, manualInput;
    private TextView elapsedTime;
    private ArrayList<Long> durations;
    private long lastBreath;
    private double value;
    private int numBreaths, margin, score, birthday;
    private String ifFastBreathing,ifNormalBreathing;
    private boolean fastBreathing;
    private CountDownTimer timer;
    private boolean completed;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.my_toolbar);
        setSupportActionBar(toolbar);

        numBreaths = 5;
        margin=13;
        score = 0;
        durations = new ArrayList<>();
        newTimer();
        lastBreath=-1;

        elapsedTime = findViewById(R.id.ElapsedTime);
        circle = findViewById(R.id.Circle);
        circle.setOnClickListener(view -> {
            view.startAnimation(AnimationUtils.loadAnimation(this, R.anim.circle_animation));
            ((TextView) view.findViewById(R.id.TapOnInhale)).setText(R.string.tap_on_inhale);
            breathTaken();
            if (validateDataCollection()) {
                value = getBreathRate(numBreaths);
                completeMeasuring();
            }
        });
    }

    /**
     * handles logic for when a breath is measured
     */
    public void breathTaken() {
        long currTime = System.currentTimeMillis();
        if (lastBreath != -1) {
            long dur = currTime - lastBreath;
            durations.add(dur);
        } else{
            startTimer();
        }

        lastBreath = currTime;
    }

    /**
     * Method to determine if number of consistent breaths matches or exceeds threshold
     * @return
     */
    public boolean validateDataCollection() {
        return getValidProgress() >= numBreaths;
    }

    public double getBreathRate(int num) {
        return 60 / (getMedian(num) / 1000.0);
    }

    public long getMedian(int length) {
        if (length > durations.size()) {
            return -1;
        }
        if (length == 0) {
            return -1;
        }

        ArrayList<Long> sub = new ArrayList<Long>(durations.subList(durations.size() - (length), durations.size()));
        Collections.sort(sub);
        int half = (length / 2);
        if (length % 2 == 0) {
            return (sub.get(half - 1) + sub.get(half)) / 2;
        }
        return sub.get(half);
    }

    public void resetTimer(){
        timer.cancel();
        newTimer();
    }

    public void startTimer(){
        timer.start();
    }

    public void newTimer(){
        timer = new CountDownTimer(60*1000,1000) {
            @Override
            public void onTick(long l) {
                updateElapsedTimeView((60*1000)-l);
            }

            @Override
            public void onFinish() {
                if(durations.size()<5){
                    resetRespRate();
                } else {
                    value = getBreathRate(durations.size());
                    completeMeasuring();
                }
            }
        };
    }


    public void completeMeasuring(){
        setCompleted(true);
        resetTimer();
        evalFastBreathing();
//        showDialog();
    }

    /**
     * calculates number of consistent breaths in trailing window.
     *
     * @return number of consistent breaths
     */
    public int getValidProgress() {
        for (int ii = 1; ii <= numBreaths; ii++) {
            if (ii > durations.size()) {
                return ii - 1;
            }
            long median = getMedian(ii);
            if (median == -1) {
                return ii - 1;
            }
            long up = upperBound(median);
            long low = lowerBound(median);

            for (int jj = durations.size() - 1; jj > ((durations.size() - 1) - ii); jj--) {
                if (!inBounds(durations.get(jj), up, low)) {
                    return ii - 1;
                }
            }
        }
        return numBreaths;
    }

    /**
     * Calculates the lower bound of what is considered a consistent breath given the margin
     * @param med median
     * @return lower bound value
     */
    public long lowerBound(long med) {
        return (long) (med * (1.0 - (margin / 100.0)));
    }

    /**
     * Calculates the upper bound of what is considered a consistent breath given the margin
     * @param med median
     * @return upper bound value
     */
    public long upperBound(long med) {
        return (long) (med * (1.0 + (margin / 100.0)));
    }

    /**
     * determines if value is in bounds
     * @param val value
     * @param up upper bound
     * @param low lower bound
     * @return is in bounds
     */
    public boolean inBounds(long val, long up, long low) {
        return (val < up) && (val > low);
    }

    public void updateElapsedTimeView(long millis){
        String b = this.getResources().getString(R.string.elapsed_time);
        elapsedTime.setText(b+" "+millisecondsToString(millis));
    }

    public String millisecondsToString(long millis){
        String  ms = (TimeUnit.MILLISECONDS.toMinutes(millis) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)))+
                ":"+ (TimeUnit.MILLISECONDS.toSeconds(millis) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
        return ms;
    }

    public void resetRespRate(){
        ((TextView) circle.findViewById(R.id.TapOnInhale)).setText(R.string.start_text);
        durations.clear();
        lastBreath = -1;
        resetTimer();
        updateElapsedTimeView(0);
    }

    /**
     * method decides whether child is experiencing fast breathing based on RR and age.
     */
    public void evalFastBreathing(){
        if(isCompleted()) {
            //Date birthday = ((GlobalClass) getHostActivity().getApplication()).currentPatient.getBirthday();

            birthday = 1;
            if (birthday>-1) {
                if (birthday <2) {
                    if(value>=50 && value<60){
                        fastBreathing = true;
                        score = 2;
                        return;
                    }

                    if (value >=60) {
                        fastBreathing = true;
                        score = 3;
                        return;
                    }
                }
                else {
                    if (value >= 40 && value <45) {
                        fastBreathing = true;
                        score = 2;
                        return;
                    }
                    if (value >= 45) {
                        fastBreathing = true;
                        score = 3;
                        return;
                    }
                }

                fastBreathing = false;
            } else {
                //TODO
            }
        } else {
            //TODO
        }
    }

    public boolean isCompleted() {
        return completed;
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;

    }

//    public void showDialog(){
//        final Dialog dialog = new Dialog(this);
//        dialog.requestWindowFeature(this.getWindow().FEATURE_NO_TITLE);
//        dialog.setContentView(R.layout.respiratory_rate_result_dialog_layout);
//
//        TextView respRateNum = dialog.findViewById(R.id.RespRateNum);
//        TextView condition = dialog.findViewById(R.id.FastBreathing);
//        TextView breathInfo = dialog.findViewById(R.id.breathingInfo);
//
//        NumberFormat formatter = new DecimalFormat("#0");
//        respRateNum.setText(formatter.format(value));
//
//        if(fastBreathing){
//            condition.setText(R.string.fast_breathing);
//            condition.setTextColor(getHostActivity().getResources().getColor(R.color.red));
//        } else {
//            condition.setText(R.string.normal_breathing);
//            condition.setTextColor(getHostActivity().getResources().getColor(R.color.primaryButtonColor));
//        }
//        if (birthday<2) {
//            breathInfo.setText(R.string.breathing_info_under1);
//        } else {
//            breathInfo.setText(R.string.breathing_info_over1);
//        }
//
//        Button dialogButtonReset = dialog.findViewById(R.id.ResetButton);
//        // if button is clicked, close the custom dialog
//        dialogButtonReset.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                dialog.dismiss();
//                resetRespRate();
//            }
//        });
//
//        Button dialogButtonContinue = dialog.findViewById(R.id.ContinueButton);
//        // if button is clicked, close the custom dialog
//        dialogButtonContinue.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                dialog.dismiss();
//                afterClick();
//            }
//        });
//
//        dialog.show();
//    }


}